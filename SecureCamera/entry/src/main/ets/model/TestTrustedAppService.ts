/*
 * Copyright (c) 2023-2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// @ts-check
import Logger from '../model/Logger';
import trustedAppService from '@hms.security.trustedAppService';
import { BusinessError } from '@ohos.base';

const TAG: string = 'test----TrustedAppService';

class TestTrustedAppService {

  async cteateAttestKeyTest(): Promise<void> {
      Logger.error(TAG, 'create key init');
      let device_id = 12356;
      let proprities: Array<trustedAppService.AttestParam> = [
        {
          tag: trustedAppService.AttestTag.ATTEST_TAG_ALGORITHM,
          value: trustedAppService.AttestKeyAlg.ATTEST_ALG_ECC
        },
        {
          tag: trustedAppService.AttestTag.ATTEST_TAG_KEY_SIZE,
          value: trustedAppService.AttestKeySize.ATTEST_ECC_KEY_SIZE_256
        }
      ];
      let options: trustedAppService.AttestOptions = {
        properties: proprities,
      };
      Logger.error(TAG, 'create key call start');
      await trustedAppService.createAttestKey(options)
        .then(
              ():void => {
                Logger.error(TAG, '=================================  TrustedAppService create =========end =========');
              }
        ).catch(
          (error):void => {
            let err = error as BusinessError;
            Logger.error(TAG, 'TrustedAppService create failed failed'  +  JSON.stringify(err));
          }
        );

}
  async destroyAttestKeyTest(): Promise<void> {
      Logger.error(TAG, 'destory dddd key hhh start');
    await trustedAppService.destroyAttestKey().then(
      ():void => {
        Logger.error(TAG, 'destory dddd key hhh success');
      }
    ).catch(
      (error):void => {
        let err = error as BusinessError;
        Logger.error(TAG, 'destory dddd key hhh failed ' +  JSON.stringify(err));
      }
    );
  }

  async finalizeAttestContextTest(): Promise<void> {
      Logger.error(TAG, 'finalize context start');
      let proprities: Array<trustedAppService.AttestParam> = [
        {
          tag: trustedAppService.AttestTag.ATTEST_TAG_DEVICE_TYPE,
          value: trustedAppService.AttestType.ATTEST_TYPE_CAMERA
        }
      ];
      let options: trustedAppService.AttestOptions = {
        properties: proprities,
      };

      Logger.error(TAG, 'finalize context call before');
      await trustedAppService.finalizeAttestContext(options).then(
        ():void => {
          Logger.error(TAG, 'finalize context call success');
        }
      ).catch(
        (error):void => {
          let err = error as BusinessError;
          Logger.error(TAG, 'finilize context call failed'  +  JSON.stringify(err));
        }
      );


  }

  async initializeAttestContextTest(device_id: bigint): Promise<void> {
      Logger.error(TAG, 'initializeAttestContextTest start');
      let proprities2: Array<trustedAppService.AttestParam> = [
        {
          tag: trustedAppService.AttestTag.ATTEST_TAG_DEVICE_TYPE,
          value: trustedAppService.AttestType.ATTEST_TYPE_CAMERA
        },
        {
          tag: trustedAppService.AttestTag.ATTEST_TAG_DEVICE_ID,
          value: BigInt(device_id)
        },
      ];
      let options2: trustedAppService.AttestOptions = {
        properties: proprities2,
      };
      let user_data = "test_user_data12321132132132";
      Logger.error(TAG, 'initializeAttestContextTest call  start user AttestOptions');
      await trustedAppService.initializeAttestContext(user_data, options2).then(
        (returnResult : trustedAppService.AttestReturnResult) : void => {
          Logger.error(TAG, 'begin dump result info =========================');
          for (const item of returnResult.certChains) {
            Logger.error(TAG, 'item: ' + item);
          };
        }
      ).catch(
        (error):void => {
          let err = error as BusinessError;
          Logger.error(TAG, "initial result: " +  JSON.stringify(err));
        }
      );
    }





}

export default new TestTrustedAppService();