/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "camera_manager.h"
#include <fstream>

#define LOG_TAG "DEMO:"
#define LOG_DOMAIN 0x3200
// preconfig mode
#define NO_PRECONFIG_MODE 1
#define TYPE_PRECONFIG_MODE 2
#define TYPE_RATIO_PRECONFIG_MODE 3
static void *requestImageCallback = nullptr;

namespace OHOS_CAMERA_SAMPLE {
NDKCamera *NDKCamera::ndkCamera_ = nullptr;
std::mutex NDKCamera::mtx_;
const std::unordered_map<uint32_t, Camera_SceneMode> g_int32ToCameraSceneMode = {
    {1, Camera_SceneMode::NORMAL_PHOTO}, {2, Camera_SceneMode::NORMAL_VIDEO}, {12, Camera_SceneMode::SECURE_PHOTO}};
const std::unordered_map<uint32_t, Camera_PreconfigType> g_int32ToCameraPreconfigType = {
    {0, Camera_PreconfigType::PRECONFIG_720P},
    {1, Camera_PreconfigType::PRECONFIG_1080P},
    {2, Camera_PreconfigType::PRECONFIG_4K},
    {3, Camera_PreconfigType::PRECONFIG_HIGH_QUALITY}};
const std::unordered_map<uint32_t, Camera_PreconfigRatio> g_int32ToCameraPreconfigRatio = {
    {0, Camera_PreconfigRatio::PRECONFIG_RATIO_1_1},
    {1, Camera_PreconfigRatio::PRECONFIG_RATIO_4_3},
    {2, Camera_PreconfigRatio::PRECONFIG_RATIO_16_9}};

NDKCamera::NDKCamera(char *str, uint32_t focusMode, uint32_t cameraDeviceIndex, uint32_t sceneMode,
                     uint32_t preconfigMode, uint32_t preconfigType, uint32_t preconfigRatio, uint32_t photoOutputType,
                     bool isMovingPhoto)
    : previewSurfaceId_(str), cameras_(nullptr), focusMode_(focusMode), cameraDeviceIndex_(cameraDeviceIndex),
      cameraOutputCapability_(nullptr), cameraInput_(nullptr), captureSession_(nullptr), size_(0),
      isCameraMuted_(nullptr), profile_(nullptr), photoSurfaceId_(nullptr), previewOutput_(nullptr),
      photoOutput_(nullptr), metaDataObjectType_(nullptr), metadataOutput_(nullptr), isExposureModeSupported_(false),
      isFocusModeSupported_(false), exposureMode_(EXPOSURE_MODE_LOCKED), minExposureBias_(0), maxExposureBias_(0),
      step_(0), ret_(CAMERA_OK), sceneMode_(NORMAL_PHOTO), sceneModes_(nullptr), preconfigType_(PRECONFIG_720P),
      preconfigRatio_(PRECONFIG_RATIO_1_1), activePreviewProfile_(nullptr), activePhotoProfile_(nullptr),
      activeVideoProfile_(nullptr), sceneModeSize_(0), preconfigMode_(preconfigMode), isSuccess_(false),
      preconfigged_(false), colorSpaces_(nullptr), colorSpaceSize_(0), frameRateRanges_(nullptr),
      frameRateRangeSize_(0), photoOutputType_(photoOutputType), isMovingPhoto_(isMovingPhoto) {
    valid_ = false;
    ReleaseCamera();
    Camera_ErrorCode ret = OH_Camera_GetCameraManager(&cameraManager_);
    if (cameraManager_ == nullptr || ret != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "Get CameraManager failed.");
    }

    ret = OH_CameraManager_CreateCaptureSession(cameraManager_, &captureSession_);
    if (captureSession_ == nullptr || ret != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "Create captureSession failed.");
    }

    auto itr1 = g_int32ToCameraSceneMode.find(sceneMode);
    if (itr1 != g_int32ToCameraSceneMode.end()) {
        sceneMode_ = itr1->second;
    }
    auto itr2 = g_int32ToCameraPreconfigType.find(preconfigType);
    if (itr2 != g_int32ToCameraPreconfigType.end()) {
        preconfigType_ = itr2->second;
    }
    auto itr3 = g_int32ToCameraPreconfigRatio.find(preconfigRatio);
    if (itr3 != g_int32ToCameraPreconfigRatio.end()) {
        preconfigRatio_ = itr3->second;
    }
    // expect CAMERA_OK
    ret = OH_CaptureSession_SetSessionMode(captureSession_, sceneMode_);
    DRAWING_LOGD("OH_CaptureSession_SetSessionMode sceneMode_: %{public}d!", sceneMode_);
    DRAWING_LOGD("OH_CaptureSession_SetSessionMode return with ret code: %{public}d!", ret);

    CaptureSessionRegisterCallback();
    GetSupportedCameras();
    GetSupportedSceneModes();
    GetSupportedOutputCapabilityWithSceneMode(sceneMode_);
    GetSupportedOutputCapability();
    IsTorchSupported(&isSuccess_);
    IsTorchSupportedByTorchMode(&isSuccess_);
    SetTorchMode();
    CreatePreviewOutput();
    CreateCameraInput();
    CameraInputOpen();
    CameraManagerRegisterCallback();
    SessionFlowFn();
    valid_ = true;
}

NDKCamera::~NDKCamera() {
    valid_ = false;
    OH_LOG_ERROR(LOG_APP, "~NDKCamera");
    Camera_ErrorCode ret = CAMERA_OK;

    if (cameraManager_) {
        OH_LOG_ERROR(LOG_APP, "Release OH_CameraManager_DeleteSupportedCameraOutputCapability. enter");
        ret = OH_CameraManager_DeleteSupportedCameraOutputCapability(cameraManager_, cameraOutputCapability_);
        if (ret != CAMERA_OK) {
            OH_LOG_ERROR(LOG_APP, "Delete CameraOutputCapability failed.");
        } else {
            OH_LOG_ERROR(LOG_APP, "Release OH_CameraManager_DeleteSupportedCameraOutputCapability. ok");
        }

        OH_LOG_ERROR(LOG_APP, "Release OH_CameraManager_DeleteSupportedCameras. enter");
        ret = OH_CameraManager_DeleteSupportedCameras(cameraManager_, cameras_, size_);
        if (ret != CAMERA_OK) {
            OH_LOG_ERROR(LOG_APP, "Delete Cameras failed.");
        } else {
            OH_LOG_ERROR(LOG_APP, "Release OH_CameraManager_DeleteSupportedCameras. ok");
        }

        ret = OH_Camera_DeleteCameraManager(cameraManager_);
        if (ret != CAMERA_OK) {
            OH_LOG_ERROR(LOG_APP, "Delete CameraManager failed.");
        } else {
            OH_LOG_ERROR(LOG_APP, "Release OH_Camera_DeleteCameraMananger. ok");
        }
        cameraManager_ = nullptr;
    }
    OH_LOG_ERROR(LOG_APP, "~NDKCamera exit");
}

Camera_ErrorCode NDKCamera::ReleaseCamera(void) {
    OH_LOG_ERROR(LOG_APP, " enter ReleaseCamera");
    if (previewOutput_ && captureSession_) {
        PreviewOutputStop();
        OH_CaptureSession_RemovePreviewOutput(captureSession_, previewOutput_);
        PreviewOutputRelease();
    }
    if (photoOutput_) {
        PhotoOutputRelease();
    }
    if (captureSession_) {
        SessionRealese();
    }
    if (cameraInput_) {
        CameraInputClose();
    }
    OH_LOG_ERROR(LOG_APP, " exit ReleaseCamera");
    return ret_;
}

Camera_ErrorCode NDKCamera::RequestImageCallback(void *cb) {
    if (cb == nullptr) {
        OH_LOG_ERROR(LOG_APP, " RequestImageCallback invalid error");
        return CAMERA_INVALID_ARGUMENT;
    }
    requestImageCallback = cb;

    return CAMERA_OK;
}

Camera_ErrorCode NDKCamera::ReleaseSession(void) {
    OH_LOG_ERROR(LOG_APP, " enter ReleaseSession");
    PreviewOutputStop();
    PhotoOutputRelease();
    SessionRealese();
    OH_LOG_ERROR(LOG_APP, " exit ReleaseSession");
    return ret_;
}

Camera_ErrorCode NDKCamera::SessionRealese(void) {
    OH_LOG_ERROR(LOG_APP, " enter SessionRealese");
    Camera_ErrorCode ret = OH_CaptureSession_Release(captureSession_);
    captureSession_ = nullptr;
    OH_LOG_ERROR(LOG_APP, " exit SessionRealese");
    return ret;
}

Camera_ErrorCode NDKCamera::HasFlashFn(uint32_t mode) {
    Camera_FlashMode flashMode = static_cast<Camera_FlashMode>(mode);
    // Check for flashing lights
    bool hasFlash = false;
    Camera_ErrorCode ret = OH_CaptureSession_HasFlash(captureSession_, &hasFlash);
    if (captureSession_ == nullptr || ret != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_HasFlash failed.");
    }
    if (hasFlash) {
        OH_LOG_INFO(LOG_APP, "hasFlash success-----");
    } else {
        OH_LOG_ERROR(LOG_APP, "hasFlash fail-----");
    }

    // Check if the flash mode is supported
    bool isSupported = false;
    ret = OH_CaptureSession_IsFlashModeSupported(captureSession_, flashMode, &isSupported);
    if (ret != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_IsFlashModeSupported failed.");
    }
    if (isSupported) {
        OH_LOG_INFO(LOG_APP, "isFlashModeSupported success-----");
    } else {
        OH_LOG_ERROR(LOG_APP, "isFlashModeSupported fail-----");
    }

    // Set flash mode
    ret = OH_CaptureSession_SetFlashMode(captureSession_, flashMode);
    if (ret == CAMERA_OK) {
        OH_LOG_INFO(LOG_APP, "OH_CaptureSession_SetFlashMode success.");
    } else {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_SetFlashMode failed. %{public}d ", ret);
    }

    // Obtain the flash mode of the current device
    ret = OH_CaptureSession_GetFlashMode(captureSession_, &flashMode);
    if (ret == CAMERA_OK) {
        OH_LOG_INFO(LOG_APP, "OH_CaptureSession_GetFlashMode success. flashMode：%{public}d ", flashMode);
    } else {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_GetFlashMode failed. %d ", ret);
    }
    return ret;
}

Camera_ErrorCode NDKCamera::IsVideoStabilizationModeSupportedFn(uint32_t mode) {
    Camera_VideoStabilizationMode videoMode = static_cast<Camera_VideoStabilizationMode>(mode);
    // Check if the specified video anti shake mode is supported
    bool isSupported = false;
    Camera_ErrorCode ret =
        OH_CaptureSession_IsVideoStabilizationModeSupported(captureSession_, videoMode, &isSupported);
    if (ret != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_IsVideoStabilizationModeSupported failed.");
    }
    if (isSupported) {
        OH_LOG_INFO(LOG_APP, "OH_CaptureSession_IsVideoStabilizationModeSupported success-----");
    } else {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_IsVideoStabilizationModeSupported fail-----");
    }

    // Set video stabilization
    ret = OH_CaptureSession_SetVideoStabilizationMode(captureSession_, videoMode);
    if (ret == CAMERA_OK) {
        OH_LOG_INFO(LOG_APP, "OH_CaptureSession_SetVideoStabilizationMode success.");
    } else {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_SetVideoStabilizationMode failed. %{public}d ", ret);
    }

    ret = OH_CaptureSession_GetVideoStabilizationMode(captureSession_, &videoMode);
    if (ret == CAMERA_OK) {
        OH_LOG_INFO(LOG_APP, "OH_CaptureSession_GetVideoStabilizationMode success. videoMode：%{public}f ", videoMode);
    } else {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_GetVideoStabilizationMode failed. %{public}d ", ret);
    }
    return ret;
}

Camera_ErrorCode NDKCamera::setZoomRatioFn(uint32_t zoomRatio) {
    float zoom = float(zoomRatio);
    // Obtain supported zoom range
    float minZoom;
    float maxZoom;
    Camera_ErrorCode ret = OH_CaptureSession_GetZoomRatioRange(captureSession_, &minZoom, &maxZoom);
    if (captureSession_ == nullptr || ret != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_GetZoomRatioRange failed.");
    } else {
        OH_LOG_INFO(LOG_APP, "OH_CaptureSession_GetZoomRatioRange success. minZoom: %{public}f, maxZoom:%{public}f",
                    minZoom, maxZoom);
    }

    // Set Zoom
    ret = OH_CaptureSession_SetZoomRatio(captureSession_, zoom);
    if (ret == CAMERA_OK) {
        OH_LOG_INFO(LOG_APP, "OH_CaptureSession_SetZoomRatio success.");
    } else {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_SetZoomRatio failed. %{public}d ", ret);
    }

    // Obtain the zoom value of the current device
    ret = OH_CaptureSession_GetZoomRatio(captureSession_, &zoom);
    if (ret == CAMERA_OK) {
        OH_LOG_INFO(LOG_APP, "OH_CaptureSession_GetZoomRatio success. zoom：%{public}f ", zoom);
    } else {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_GetZoomRatio failed. %{public}d ", ret);
    }
    return ret;
}

Camera_ErrorCode NDKCamera::SessionBegin(void) {
    Camera_ErrorCode ret = OH_CaptureSession_BeginConfig(captureSession_);
    if (ret == CAMERA_OK) {
        OH_LOG_INFO(LOG_APP, "OH_CaptureSession_BeginConfig success.");
    } else {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_BeginConfig failed. %d ", ret);
    }
    return ret;
}

Camera_ErrorCode NDKCamera::SessionCommitConfig(void) {
    Camera_ErrorCode ret = OH_CaptureSession_CommitConfig(captureSession_);
    if (ret == CAMERA_OK) {
        OH_LOG_INFO(LOG_APP, "OH_CaptureSession_CommitConfig success.");
    } else {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_CommitConfig failed. %d ", ret);
    }
    return ret;
}

Camera_ErrorCode NDKCamera::SessionStart(void) {
    Camera_ErrorCode ret = OH_CaptureSession_Start(captureSession_);
    if (ret == CAMERA_OK) {
        OH_LOG_INFO(LOG_APP, "OH_CaptureSession_Start success.");
    } else {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_Start failed. %d ", ret);
    }
    return ret;
}

Camera_ErrorCode NDKCamera::SessionStop(void) {
    Camera_ErrorCode ret = OH_CaptureSession_Stop(captureSession_);
    if (ret == CAMERA_OK) {
        OH_LOG_INFO(LOG_APP, "OH_CaptureSession_Stop success.");
    } else {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_Stop failed. %d ", ret);
    }
    return ret;
}

Camera_ErrorCode NDKCamera::SessionFlowFn(void) {
    OH_LOG_INFO(LOG_APP, "Start SessionFlowFn IN.");
    // Start configuring session
    OH_LOG_INFO(LOG_APP, "session beginConfig.");
    Camera_ErrorCode ret = OH_CaptureSession_BeginConfig(captureSession_);

    isSuccess_ = false;
    CanAddInput(&isSuccess_);
    if (!isSuccess_) {
        DRAWING_LOGD("NDKCamera::SessionFlowFn can not add input!");
        return Camera_ErrorCode::CAMERA_OPERATION_NOT_ALLOWED;
    }

    // Add CameraInput to the session
    OH_LOG_INFO(LOG_APP, "session addInput.");
    ret = OH_CaptureSession_AddInput(captureSession_, cameraInput_);

    isSuccess_ = false;
    CanAddPreviewOutput(&isSuccess_);
    if (!isSuccess_) {
        DRAWING_LOGD("NDKCamera::SessionFlowFn can not add preview output!");
        return Camera_ErrorCode::CAMERA_OPERATION_NOT_ALLOWED;
    }

    // Add previewOutput to the session
    OH_LOG_INFO(LOG_APP, "session add Preview Output.");
    ret = OH_CaptureSession_AddPreviewOutput(captureSession_, previewOutput_);
    GetActivePreviewOutputProfile();

    // Adding PhotoOutput to the Session
    OH_LOG_INFO(LOG_APP, "session add Photo Output.");

    // Submit configuration information
    OH_LOG_INFO(LOG_APP, "session commitConfig");
    ret = OH_CaptureSession_CommitConfig(captureSession_);

    // Start Session Work
    OH_LOG_INFO(LOG_APP, "session start");
    ret = OH_CaptureSession_Start(captureSession_);
    OH_LOG_INFO(LOG_APP, "session success");

    PreviewOutputGetSupportedFrameRates();
    PreviewOutputGetActiveFrameRate();
    PreviewOutputSetFrameRate();
    PreviewOutputGetActiveFrameRate();
    GetSupportedColorSpaces();
    GetActiveColorSpace();
    SetActiveColorSpace();
    GetActiveColorSpace();
    GetExposureValue();
    GetFocalLength();
    SetSmoothZoom();

    // Start focusing
    OH_LOG_INFO(LOG_APP, "IsFocusMode start");
    ret = IsFocusMode(focusMode_);
    OH_LOG_INFO(LOG_APP, "IsFocusMode success");
    return ret;
}

Camera_ErrorCode NDKCamera::CreateCameraInput(void) {
    OH_LOG_ERROR(LOG_APP, "enter CreateCameraInput.");
    ret_ = OH_CameraManager_CreateCameraInput(cameraManager_, &cameras_[cameraDeviceIndex_], &cameraInput_);
    if (cameraInput_ == nullptr || ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "CreateCameraInput failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    OH_LOG_ERROR(LOG_APP, "exit CreateCameraInput.");
    CameraInputRegisterCallback();
    return ret_;
}

Camera_ErrorCode NDKCamera::CameraInputOpen(void) {
    OH_LOG_ERROR(LOG_APP, "enter CameraInputOpen.");
    ret_ = OH_CameraInput_Open(cameraInput_);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "CameraInput_Open failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    OH_LOG_ERROR(LOG_APP, "exit CameraInputOpen.");
    return ret_;
}

Camera_ErrorCode NDKCamera::CameraInputClose(void) {
    OH_LOG_ERROR(LOG_APP, "enter CameraInput_Close.");
    ret_ = OH_CameraInput_Close(cameraInput_);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "CameraInput_Close failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    OH_LOG_ERROR(LOG_APP, "exit CameraInput_Close.");
    return ret_;
}

Camera_ErrorCode NDKCamera::CameraInputRelease(void) {
    OH_LOG_ERROR(LOG_APP, "enter CameraInputRelease.");
    ret_ = OH_CameraInput_Release(cameraInput_);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "CameraInput_Release failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    OH_LOG_ERROR(LOG_APP, "exit CameraInputRelease.");
    return ret_;
}

Camera_ErrorCode NDKCamera::GetSupportedCameras(void) {
    ret_ = OH_CameraManager_GetSupportedCameras(cameraManager_, &cameras_, &size_);
    if (cameras_ == nullptr || &size_ == nullptr || ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "Get supported cameras failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    return ret_;
}

Camera_ErrorCode NDKCamera::GetSupportedOutputCapability(void) {
    ret_ = OH_CameraManager_GetSupportedCameraOutputCapability(cameraManager_, &cameras_[cameraDeviceIndex_],
                                                               &cameraOutputCapability_);
    if (cameraOutputCapability_ == nullptr || ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "GetSupportedCameraOutputCapability failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    return ret_;
}

Camera_ErrorCode NDKCamera::CreatePreviewOutput(void) {
    DRAWING_LOGD("NDKCamera::CreatePreviewOutput start!");
    isSuccess_ = false;
    if (preconfigMode_ == TYPE_RATIO_PRECONFIG_MODE) {
        ret_ = CanPreconfigWithRatio(&isSuccess_);
        if (ret_ != CAMERA_OK) {
            DRAWING_LOGD("NDKCamera::CreatePreviewOutput CanPreconfigWithRatio failed!");
            return CAMERA_INVALID_ARGUMENT;
        }
    } else if (preconfigMode_ == TYPE_PRECONFIG_MODE) {
        ret_ = CanPreconfig(&isSuccess_);
        if (ret_ != CAMERA_OK) {
            DRAWING_LOGD("NDKCamera::CreatePreviewOutput CanPreconfig failed!");
            return CAMERA_INVALID_ARGUMENT;
        }
    }

    if (preconfigMode_ == NO_PRECONFIG_MODE || !isSuccess_) {
        DRAWING_LOGD("NDKCamera::CreatePreviewOutput into normal branch!");
        profile_ = cameraOutputCapability_->previewProfiles[0];
        if (profile_ == nullptr) {
            OH_LOG_ERROR(LOG_APP, "Get previewProfiles failed.");
            return CAMERA_INVALID_ARGUMENT;
        }
        ret_ = OH_CameraManager_CreatePreviewOutput(cameraManager_, profile_, previewSurfaceId_, &previewOutput_);
        if (previewSurfaceId_ == nullptr || previewOutput_ == nullptr || ret_ != CAMERA_OK) {
            OH_LOG_ERROR(LOG_APP, "CreatePreviewOutput failed.");
            return CAMERA_INVALID_ARGUMENT;
        }
        return ret_;
        PreviewOutputRegisterCallback();
    } else {
        DRAWING_LOGD("NDKCamera::CreatePreviewOutput into preconfig branch!");
        if (preconfigMode_ == TYPE_RATIO_PRECONFIG_MODE) {
            ret_ = PreconfigWithRatio();
            if (ret_ != CAMERA_OK) {
                DRAWING_LOGD("NDKCamera::CreatePreviewOutput PreconfigWithRatio failed!");
                return CAMERA_INVALID_ARGUMENT;
            }
        } else {
            ret_ = Preconfig();
            if (ret_ != CAMERA_OK) {
                DRAWING_LOGD("NDKCamera::CreatePreviewOutput Preconfig failed!");
                return CAMERA_INVALID_ARGUMENT;
            }
        }
        preconfigged_ = true;
        ret_ = OH_CameraManager_CreatePreviewOutputUsedInPreconfig(cameraManager_, previewSurfaceId_, &previewOutput_);
        if (previewOutput_ == nullptr || ret_ != CAMERA_OK) {
            DRAWING_LOGD("OH_CameraManager_CreatePreviewOutputUsedInPreconfig failed!");
            OH_LOG_ERROR(LOG_APP, "CreatePreviewOutput failed.");
            return CAMERA_INVALID_ARGUMENT;
        }
        DRAWING_LOGD("OH_CameraManager_CreatePreviewOutputUsedInPreconfig return with ret code: %{public}d!", ret_);
        return ret_;
        PreviewOutputRegisterCallback();
    }
}

Camera_ErrorCode NDKCamera::CreatePhotoOutput(char *photoSurfaceId) {
    DRAWING_LOGD("NDKCamera::CreatePhotoOutput start!");
    if (photoOutputType_ == 1) {
        isMovingPhoto_ = false;
        if (preconfigged_) {
            DRAWING_LOGD("NDKCamera::CreatePhotoOutput into preconfigged branch!");
            ret_ = OH_CameraManager_CreatePhotoOutputUsedInPreconfig(cameraManager_, photoSurfaceId, &photoOutput_);
            if (photoOutput_ == nullptr || ret_ != CAMERA_OK) {
                DRAWING_LOGD("OH_CameraManager_CreatePhotoOutputUsedInPreconfig failed!");
                OH_LOG_ERROR(LOG_APP, "CreatePhotoOutput failed.");
                return CAMERA_INVALID_ARGUMENT;
            }
            DRAWING_LOGD("OH_CameraManager_CreatePhotoOutputUsedInPreconfig return with ret code: %{public}d!", ret_);
            PhotoOutputRegisterCallback();
            return ret_;
        } else {
            DRAWING_LOGD("NDKCamera::CreatePhotoOutput into normal branch!");
            profile_ = cameraOutputCapability_->photoProfiles[0];
            if (profile_ == nullptr) {
                OH_LOG_ERROR(LOG_APP, "Get photoProfiles failed.");
                return CAMERA_INVALID_ARGUMENT;
            }

            if (photoSurfaceId == nullptr) {
                OH_LOG_ERROR(LOG_APP, "CreatePhotoOutput failed.");
                return CAMERA_INVALID_ARGUMENT;
            }

            ret_ = OH_CameraManager_CreatePhotoOutput(cameraManager_, profile_, photoSurfaceId, &photoOutput_);
            PhotoOutputRegisterCallback();
            return ret_;
        }
    } else {
        DRAWING_LOGD("NDKCamera::CreatePhotoOutput into createWithoutSurfaceID branch!");
        profile_ = cameraOutputCapability_->photoProfiles[0];
        profile_->size.width = 1920;
        profile_->size.height = 1080;
        //     cameraProfile_->size.width = 3840;
        //     cameraProfile_->size.height = 2160;
        profile_->format = CAMERA_FORMAT_JPEG;
        ret_ = OH_CameraManager_CreatePhotoOutputWithoutSurface(cameraManager_, profile_, &photoOutput_);
        if (photoOutput_ == nullptr || ret_ != CAMERA_OK) {
            DRAWING_LOGD("RM007 Log OH_CameraManager_CreatePhotoOutputWithoutSurface failed!");
            OH_LOG_ERROR(LOG_APP, "CreatePhotoOutput failed.");
            return CAMERA_INVALID_ARGUMENT;
        }

        if (photoOutputType_ == 2) {
            isMovingPhoto_ = false;
        }
        DRAWING_LOGD("RM007 Log OH_CameraManager_CreatePhotoOutputWithoutSurface return with ret code: %{public}d!",
                     ret_);
        if ((photoOutputType_ == 2 || photoOutputType_ == 4) && !isMovingPhoto_) {
            DRAWING_LOGD("RM007 Log NDKCamera::CreatePhotoOutput register photo available callback!");
            PhotoOutputRegisterPhotoAvailableCallback();
        }
        if (photoOutputType_ == 3 || photoOutputType_ == 4) {
            DRAWING_LOGD("RM007 Log NDKCamera::CreatePhotoOutput register photo asset available callback!");
            PhotoOutputRegisterPhotoAssetAvailableCallback();
        }
        //         if (photoOutputType_ == 4) {
        //             PhotoOutputUnRegisterPhotoAvailableCallback();
        //             PhotoOutputUnRegisterPhotoAssetAvailableCallback();
        //         }
        return ret_;
    }
}

Camera_ErrorCode NDKCamera::CreateVideoOutput(char *videoId) {
    DRAWING_LOGD("NDKCamera::CreateVideoOutput start!");
    if (preconfigged_) {
        DRAWING_LOGD("NDKCamera::CreateVideoOutput into preconfigged_ branch!");
        ret_ = OH_CameraManager_CreateVideoOutputUsedInPreconfig(cameraManager_, videoId, &videoOutput_);
        if (videoOutput_ == nullptr || ret_ != CAMERA_OK) {
            DRAWING_LOGD("OH_CameraManager_CreateVideoOutputUsedInPreconfig failed!");
            OH_LOG_ERROR(LOG_APP, "CreateVideoOutput failed.");
            return CAMERA_INVALID_ARGUMENT;
        }
        DRAWING_LOGD("OH_CameraManager_CreateVideoOutputUsedInPreconfig return with ret code: %{public}d!", ret_);
        return ret_;
    } else {
        DRAWING_LOGD("NDKCamera::CreateVideoOutput into normal branch!");
        videoProfile_ = cameraOutputCapability_->videoProfiles[0];

        if (videoProfile_ == nullptr) {
            OH_LOG_ERROR(LOG_APP, "Get videoProfiles failed.");
            return CAMERA_INVALID_ARGUMENT;
        }
        ret_ = OH_CameraManager_CreateVideoOutput(cameraManager_, videoProfile_, videoId, &videoOutput_);
        if (videoId == nullptr || videoOutput_ == nullptr || ret_ != CAMERA_OK) {
            OH_LOG_ERROR(LOG_APP, "CreateVideoOutput failed.");
            return CAMERA_INVALID_ARGUMENT;
        }

        return ret_;
    }
}

Camera_ErrorCode NDKCamera::AddVideoOutput(void) {
    Camera_ErrorCode ret = OH_CaptureSession_AddVideoOutput(captureSession_, videoOutput_);
    if (ret == CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_AddVideoOutput success.");
    } else {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_AddVideoOutput failed. %d ", ret);
    }
    return ret;
}

Camera_ErrorCode NDKCamera::AddPhotoOutput() {
    Camera_ErrorCode ret = OH_CaptureSession_AddPhotoOutput(captureSession_, photoOutput_);
    if (ret == CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_AddPhotoOutput success.");
    } else {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_AddPhotoOutput failed. %d ", ret);
    }
    return ret;
}

Camera_ErrorCode NDKCamera::CreateMetadataOutput(void) {
    metaDataObjectType_ = cameraOutputCapability_->supportedMetadataObjectTypes[0];
    if (metaDataObjectType_ == nullptr) {
        OH_LOG_ERROR(LOG_APP, "Get metaDataObjectType failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    ret_ = OH_CameraManager_CreateMetadataOutput(cameraManager_, metaDataObjectType_, &metadataOutput_);
    if (metadataOutput_ == nullptr || ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "CreateMetadataOutput failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    MetadataOutputRegisterCallback();
    return ret_;
}

Camera_ErrorCode NDKCamera::IsCameraMuted(void) {
    ret_ = OH_CameraManager_IsCameraMuted(cameraManager_, isCameraMuted_);
    if (isCameraMuted_ == nullptr || ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "IsCameraMuted failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    return ret_;
}

Camera_ErrorCode NDKCamera::PreviewOutputStop(void) {
    OH_LOG_ERROR(LOG_APP, "enter PreviewOutputStop.");
    ret_ = OH_PreviewOutput_Stop(previewOutput_);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "PreviewOutputStop failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    return ret_;
}

Camera_ErrorCode NDKCamera::PreviewOutputRelease(void) {
    OH_LOG_ERROR(LOG_APP, "enter PreviewOutputRelease.");
    if (previewOutput_ != nullptr) {
        ret_ = OH_PreviewOutput_Release(previewOutput_);
        if (ret_ != CAMERA_OK) {
            OH_LOG_ERROR(LOG_APP, "PreviewOutputRelease failed.");
            return CAMERA_INVALID_ARGUMENT;
        }
    }
    return CAMERA_OK;
}

Camera_ErrorCode NDKCamera::PhotoOutputRelease(void) {
    OH_LOG_ERROR(LOG_APP, "enter PhotoOutputRelease.");
    ret_ = OH_PhotoOutput_Release(photoOutput_);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "PhotoOutputRelease failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    return ret_;
}

Camera_ErrorCode NDKCamera::StartVideo(char *videoId, char *photoId) {
    OH_LOG_INFO(LOG_APP, "StartVideo begin.");
    Camera_ErrorCode ret = SessionStop();
    if (ret == CAMERA_OK) {
        OH_LOG_INFO(LOG_APP, "SessionStop success.");
    } else {
        OH_LOG_ERROR(LOG_APP, "SessionStop failed. %d ", ret);
    }
    ret = SessionBegin();
    if (ret == CAMERA_OK) {
        OH_LOG_INFO(LOG_APP, "SessionBegin success.");
    } else {
        OH_LOG_ERROR(LOG_APP, "SessionBegin failed. %d ", ret);
    }
    OH_CaptureSession_RemovePhotoOutput(captureSession_, photoOutput_);
    CreatePhotoOutput(photoId);
    // expect CAMERA_SERVICE_FATAL_ERROR
    ret = OH_CaptureSession_SetSessionMode(captureSession_, Camera_SceneMode::NORMAL_PHOTO);
    DRAWING_LOGD("OH_CaptureSession_SetSessionMode NORMAL_PHOTO return with ret code: %{public}d!", ret);

    isSuccess_ = false;
    CanAddPhotoOutput(&isSuccess_);
    if (!isSuccess_) {
        DRAWING_LOGD("NDKCamera::StartVideo can not add photo output!");
        return Camera_ErrorCode::CAMERA_OPERATION_NOT_ALLOWED;
    }
    AddPhotoOutput();
    GetActivePhotoOutputProfile();
    CreateVideoOutput(videoId);
    // expect CAMERA_SERVICE_FATAL_ERROR
    ret = OH_CaptureSession_SetSessionMode(captureSession_, Camera_SceneMode::NORMAL_VIDEO);
    DRAWING_LOGD("OH_CaptureSession_SetSessionMode NORMAL_VIDEO return with ret code: %{public}d!", ret);
    isSuccess_ = false;
    CanAddVideoOutput(&isSuccess_);
    if (!isSuccess_) {
        DRAWING_LOGD("NDKCamera::StartVideo can not add video output!");
        return Camera_ErrorCode::CAMERA_OPERATION_NOT_ALLOWED;
    }
    AddVideoOutput();
    GetActiveVideoOutputProfile();
    SessionCommitConfig();
    VideoOutputGetSupportedFrameRates();
    VideoOutputGetActiveFrameRate();
    VideoOutputSetFrameRate();
    VideoOutputGetActiveFrameRate();
    SessionStart();
    VideoOutputRegisterCallback();
    return ret;
}

Camera_ErrorCode NDKCamera::VideoOutputStart(void) {
    OH_LOG_INFO(LOG_APP, "VideoOutputStart begin.");
    Camera_ErrorCode ret = OH_VideoOutput_Start(videoOutput_);
    if (ret == CAMERA_OK) {
        OH_LOG_INFO(LOG_APP, "OH_VideoOutput_Start success.");
    } else {
        OH_LOG_ERROR(LOG_APP, "OH_VideoOutput_Start failed. %d ", ret);
    }
    return ret;
}

Camera_ErrorCode NDKCamera::StartPhoto(char *mSurfaceId) {
    Camera_ErrorCode ret = CAMERA_OK;
    if (takePictureTimes == 0) {
        ret = SessionStop();
        if (ret == CAMERA_OK) {
            OH_LOG_INFO(LOG_APP, "SessionStop success.");
        } else {
            OH_LOG_ERROR(LOG_APP, "SessionStop failed. %d ", ret);
        }
        ret = SessionBegin();
        if (ret == CAMERA_OK) {
            OH_LOG_INFO(LOG_APP, "SessionBegin success.");
        } else {
            OH_LOG_ERROR(LOG_APP, "SessionBegin failed. %d ", ret);
        }


        OH_LOG_INFO(LOG_APP, "startPhoto begin.");
        ret = CreatePhotoOutput(mSurfaceId);
        OH_LOG_INFO(LOG_APP, "startPhoto CreatePhotoOutput ret = %{public}d.", ret);

        isSuccess_ = false;
        CanAddPhotoOutput(&isSuccess_);
        if (!isSuccess_) {
            DRAWING_LOGD("NDKCamera::StartPhoto can not add photo output!");
            return Camera_ErrorCode::CAMERA_OPERATION_NOT_ALLOWED;
        }
        ret = OH_CaptureSession_AddPhotoOutput(captureSession_, photoOutput_);
        OH_LOG_INFO(LOG_APP, "startPhoto AddPhotoOutput ret = %{public}d.", ret);
        GetActivePhotoOutputProfile();
        // expect CAMERA_SERVICE_FATAL_ERROR
        ret = OH_CaptureSession_SetSessionMode(captureSession_, Camera_SceneMode::NORMAL_PHOTO);
        DRAWING_LOGD("OH_CaptureSession_SetSessionMode NORMAL_PHOTO return with ret code: %{public}d!", ret);
        isSuccess_ = false;
        ret = OH_PhotoOutput_IsMovingPhotoSupported(photoOutput_, &isSuccess_);
        DRAWING_LOGD(
            "RM007 Log NDKCamera::StartVideo OH_PhotoOutput_IsMovingPhotoSupported return with ret code: %{public}d!",
            ret);
        if (!isSuccess_) {
            DRAWING_LOGD("RM007 Log NDKCamera::StartVideo OH_PhotoOutput_IsMovingPhotoSupported return false!");
        } else {
            DRAWING_LOGD("RM007 Log NDKCamera::StartVideo OH_PhotoOutput_EnableMovingPhoto isMovingPhoto_ : %{public}d",
                         isMovingPhoto_);
            ret = OH_PhotoOutput_EnableMovingPhoto(photoOutput_, isMovingPhoto_);
            DRAWING_LOGD("RM007 Log NDKCamera::StartVideo OH_PhotoOutput_IsMovingPhotoSupported return true!");
            DRAWING_LOGD(
                "RM007 Log NDKCamera::StartVideo OH_PhotoOutput_EnableMovingPhoto return with ret code: %{public}d!",
                ret);
        }
        ret = SessionCommitConfig();

        OH_LOG_INFO(LOG_APP, "startPhoto SessionCommitConfig ret = %{public}d.", ret);
        ret = SessionStart();
        OH_LOG_INFO(LOG_APP, "startPhoto SessionStart ret = %{public}d.", ret);
    }
    ret = TakePicture();
    OH_LOG_INFO(LOG_APP, "startPhoto OH_PhotoOutput_Capture ret = %{public}d.", ret);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "startPhoto failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    takePictureTimes++;
    return ret_;
}

// exposure mode
Camera_ErrorCode NDKCamera::IsExposureModeSupportedFn(uint32_t mode) {
    OH_LOG_INFO(LOG_APP, "IsExposureModeSupportedFn start.");
    exposureMode_ = static_cast<Camera_ExposureMode>(mode);
    ret_ = OH_CaptureSession_IsExposureModeSupported(captureSession_, exposureMode_, &isExposureModeSupported_);
    if (&isExposureModeSupported_ == nullptr || ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "IsExposureModeSupported failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    ret_ = OH_CaptureSession_SetExposureMode(captureSession_, exposureMode_);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "SetExposureMode failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    ret_ = OH_CaptureSession_GetExposureMode(captureSession_, &exposureMode_);
    if (&exposureMode_ == nullptr || ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "GetExposureMode failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    OH_LOG_INFO(LOG_APP, "IsExposureModeSupportedFn end.");
    return ret_;
}

Camera_ErrorCode NDKCamera::IsMeteringPoint(int x, int y) {
    OH_LOG_INFO(LOG_APP, "IsMeteringPoint start.");
    ret_ = OH_CaptureSession_GetExposureMode(captureSession_, &exposureMode_);
    if (&exposureMode_ == nullptr || ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "GetExposureMode failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    Camera_Point exposurePoint;
    exposurePoint.x = x;
    exposurePoint.y = y;
    ret_ = OH_CaptureSession_SetMeteringPoint(captureSession_, exposurePoint);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "SetMeteringPoint failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    ret_ = OH_CaptureSession_GetMeteringPoint(captureSession_, &exposurePoint);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "GetMeteringPoint failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    OH_LOG_INFO(LOG_APP, "IsMeteringPoint end.");
    return ret_;
}

Camera_ErrorCode NDKCamera::IsExposureBiasRange(int exposureBias) {
    OH_LOG_INFO(LOG_APP, "IsExposureBiasRange end.");
    float exposureBiasValue = (float)exposureBias;
    ret_ = OH_CaptureSession_GetExposureBiasRange(captureSession_, &minExposureBias_, &maxExposureBias_, &step_);
    if (&minExposureBias_ == nullptr || &maxExposureBias_ == nullptr || &step_ == nullptr || ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "GetExposureBiasRange failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    ret_ = OH_CaptureSession_SetExposureBias(captureSession_, exposureBiasValue);
    OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_SetExposureBias end.");
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "SetExposureBias failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    ret_ = OH_CaptureSession_GetExposureBias(captureSession_, &exposureBiasValue);
    if (&exposureBiasValue == nullptr || ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "GetExposureBias failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    OH_LOG_INFO(LOG_APP, "IsExposureBiasRange end.");
    return ret_;
}

// focus mode
Camera_ErrorCode NDKCamera::IsFocusModeSupported(uint32_t mode) {
    Camera_FocusMode focusMode = static_cast<Camera_FocusMode>(mode);
    ret_ = OH_CaptureSession_IsFocusModeSupported(captureSession_, focusMode, &isFocusModeSupported_);
    if (&isFocusModeSupported_ == nullptr || ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "IsFocusModeSupported failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    return ret_;
}

Camera_ErrorCode NDKCamera::IsFocusMode(uint32_t mode) {
    OH_LOG_INFO(LOG_APP, "IsFocusMode start.");
    Camera_FocusMode focusMode = static_cast<Camera_FocusMode>(mode);
    ret_ = OH_CaptureSession_IsFocusModeSupported(captureSession_, focusMode, &isFocusModeSupported_);
    if (&isFocusModeSupported_ == nullptr || ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "IsFocusModeSupported failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    ret_ = OH_CaptureSession_SetFocusMode(captureSession_, focusMode);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "SetFocusMode failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    ret_ = OH_CaptureSession_GetFocusMode(captureSession_, &focusMode);
    if (&focusMode == nullptr || ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "GetFocusMode failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    OH_LOG_INFO(LOG_APP, "IsFocusMode end.");
    return ret_;
}

Camera_ErrorCode NDKCamera::IsFocusPoint(float x, float y) {
    OH_LOG_INFO(LOG_APP, "IsFocusPoint start.");
    Camera_Point focusPoint;
    focusPoint.x = x;
    focusPoint.y = y;
    ret_ = OH_CaptureSession_SetFocusPoint(captureSession_, focusPoint);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "SetFocusPoint failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    ret_ = OH_CaptureSession_GetFocusPoint(captureSession_, &focusPoint);
    if (&focusPoint == nullptr || ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "GetFocusPoint failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    OH_LOG_INFO(LOG_APP, "IsFocusPoint end.");
    return ret_;
}

int32_t NDKCamera::GetVideoFrameWidth(void) {
    videoProfile_ = cameraOutputCapability_->videoProfiles[0];
    if (videoProfile_ == nullptr) {
        OH_LOG_ERROR(LOG_APP, "Get videoProfiles failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    return videoProfile_->size.width;
}

int32_t NDKCamera::GetVideoFrameHeight(void) {
    videoProfile_ = cameraOutputCapability_->videoProfiles[0];
    if (videoProfile_ == nullptr) {
        OH_LOG_ERROR(LOG_APP, "Get videoProfiles failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    return videoProfile_->size.height;
}

int32_t NDKCamera::GetVideoFrameRate(void) {
    videoProfile_ = cameraOutputCapability_->videoProfiles[0];
    if (videoProfile_ == nullptr) {
        OH_LOG_ERROR(LOG_APP, "Get videoProfiles failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    return videoProfile_->range.min;
}

Camera_ErrorCode NDKCamera::VideoOutputStop(void) {
    OH_LOG_ERROR(LOG_APP, "enter VideoOutputStop.");
    ret_ = OH_VideoOutput_Stop(videoOutput_);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "VideoOutputStop failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    return ret_;
}

Camera_ErrorCode NDKCamera::VideoOutputRelease(void) {
    OH_LOG_ERROR(LOG_APP, "enter VideoOutputRelease.");
    ret_ = OH_VideoOutput_Release(videoOutput_);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "VideoOutputRelease failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    return ret_;
}

Camera_ErrorCode NDKCamera::TakePicture(void) {
    Camera_ErrorCode ret = CAMERA_OK;
    ret = OH_PhotoOutput_Capture(photoOutput_);
    OH_LOG_ERROR(LOG_APP, "takePicture OH_PhotoOutput_Capture ret = %{public}d.", ret);
    if (ret != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "startPhoto failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    return ret;
}

Camera_ErrorCode NDKCamera::TakePictureWithPhotoSettings(Camera_PhotoCaptureSetting photoSetting) {
    Camera_ErrorCode ret = CAMERA_OK;
    ret = OH_PhotoOutput_Capture_WithCaptureSetting(photoOutput_, photoSetting);

    OH_LOG_INFO(LOG_APP,
                "TakePictureWithPhotoSettings get quality %{public}d, rotation %{public}d, mirror %{public}d, "
                "latitude, %{public}d, longitude %{public}d, altitude %{public}d",
                photoSetting.quality, photoSetting.rotation, photoSetting.mirror, photoSetting.location->latitude,
                photoSetting.location->longitude, photoSetting.location->altitude);

    OH_LOG_ERROR(LOG_APP, "takePicture TakePictureWithPhotoSettings ret = %{public}d.", ret);
    if (ret != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "startPhoto failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    return ret;
}

// CameraManager Callback
void CameraManagerStatusCallback(Camera_Manager *cameraManager, Camera_StatusInfo *status) {
    OH_LOG_INFO(LOG_APP, "CameraManagerStatusCallback");
}

void CameraManagerTorchStatusCallback(Camera_Manager *cameraManager, Camera_TorchStatusInfo *status) {
    DRAWING_LOGD("CameraManagerTorchStatusCallback start!");
    OH_LOG_INFO(LOG_APP, "CameraManagerTorchStatusCallback");
}

CameraManager_Callbacks *NDKCamera::GetCameraManagerListener(void) {
    static CameraManager_Callbacks cameraManagerListener = {.onCameraStatus = CameraManagerStatusCallback};
    return &cameraManagerListener;
}

Camera_ErrorCode NDKCamera::CameraManagerRegisterCallback(void) {
    ret_ = OH_CameraManager_RegisterCallback(cameraManager_, GetCameraManagerListener());
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_CameraManager_RegisterCallback failed.");
    }
    ret_ = OH_CameraManager_RegisterTorchStatusCallback(cameraManager_, CameraManagerTorchStatusCallback);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_CameraManager_RegisterTorchStatusCallback failed.");
    }
    return ret_;
}

// CameraInput Callback
void OnCameraInputError(const Camera_Input *cameraInput, Camera_ErrorCode errorCode) {
    OH_LOG_INFO(LOG_APP, "OnCameraInput errorCode = %{public}d", errorCode);
}

CameraInput_Callbacks *NDKCamera::GetCameraInputListener(void) {
    static CameraInput_Callbacks cameraInputCallbacks = {.onError = OnCameraInputError};
    return &cameraInputCallbacks;
}

Camera_ErrorCode NDKCamera::CameraInputRegisterCallback(void) {
    ret_ = OH_CameraInput_RegisterCallback(cameraInput_, GetCameraInputListener());
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_CameraInput_RegisterCallback failed.");
    }
    return ret_;
}

// PreviewOutput Callback
void PreviewOutputOnFrameStart(Camera_PreviewOutput *previewOutput) {
    OH_LOG_INFO(LOG_APP, "PreviewOutputOnFrameStart");
}

void PreviewOutputOnFrameEnd(Camera_PreviewOutput *previewOutput, int32_t frameCount) {
    OH_LOG_INFO(LOG_APP, "PreviewOutput frameCount = %{public}d", frameCount);
}

void PreviewOutputOnError(Camera_PreviewOutput *previewOutput, Camera_ErrorCode errorCode) {
    OH_LOG_INFO(LOG_APP, "PreviewOutput errorCode = %{public}d", errorCode);
}

PreviewOutput_Callbacks *NDKCamera::GetPreviewOutputListener(void) {
    static PreviewOutput_Callbacks previewOutputListener = {.onFrameStart = PreviewOutputOnFrameStart,
                                                            .onFrameEnd = PreviewOutputOnFrameEnd,
                                                            .onError = PreviewOutputOnError};
    return &previewOutputListener;
}

Camera_ErrorCode NDKCamera::PreviewOutputRegisterCallback(void) {
    ret_ = OH_PreviewOutput_RegisterCallback(previewOutput_, GetPreviewOutputListener());
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_PreviewOutput_RegisterCallback failed.");
    }
    return ret_;
}

// PhotoOutput Callback
void PhotoOutputOnFrameStart(Camera_PhotoOutput *photoOutput) {
    DRAWING_LOGD("PhotoOutputOnFrameStart start!");
    OH_LOG_INFO(LOG_APP, "PhotoOutputOnFrameStart");
}

void PhotoOutputOnFrameShutter(Camera_PhotoOutput *photoOutput, Camera_FrameShutterInfo *info) {
    DRAWING_LOGD("PhotoOutputOnFrameShutter start!");
    OH_LOG_INFO(LOG_APP, "PhotoOutputOnFrameShutter");
}

void PhotoOutputOnFrameEnd(Camera_PhotoOutput *photoOutput, int32_t frameCount) {
    DRAWING_LOGD("PhotoOutputOnFrameEnd start!");
    OH_LOG_INFO(LOG_APP, "PhotoOutput frameCount = %{public}d", frameCount);
}

void PhotoOutputOnError(Camera_PhotoOutput *photoOutput, Camera_ErrorCode errorCode) {
    DRAWING_LOGD("PhotoOutputOnError start!");
    OH_LOG_INFO(LOG_APP, "PhotoOutput errorCode = %{public}d", errorCode);
}

void PhotoOutputCaptureEnd(Camera_PhotoOutput *photoOutput, int32_t frameCount) {
    DRAWING_LOGD("PhotoOutputCaptureEnd start!");
    OH_LOG_INFO(LOG_APP, "PhotoOutputCaptureEnd");
}

void PhotoOutputCaptureStartWithInfo(Camera_PhotoOutput *photoOutput, Camera_CaptureStartInfo *Info) {
    DRAWING_LOGD("PhotoOutputCaptureStartWithInfo start!");
    OH_LOG_INFO(LOG_APP, "PhotoOutputCaptureStartWithInfo");
}

void PhotoOutputOnFrameShutterEnd(Camera_PhotoOutput *photoOutput, Camera_FrameShutterInfo *Info) {
    DRAWING_LOGD("PhotoOutputOnFrameShutterEnd start!");
    OH_LOG_INFO(LOG_APP, "PhotoOutputOnFrameShutterEnd");
}

void PhotoOutputCaptureReady(Camera_PhotoOutput *photoOutput) {
    DRAWING_LOGD("PhotoOutputCaptureReady start!");
    OH_LOG_INFO(LOG_APP, "PhotoOutputCaptureReady");
}

void PhotoOutputEstimatedCaptureDuration(Camera_PhotoOutput *photoOutput, int64_t duration) {
    DRAWING_LOGD("PhotoOutputEstimatedCaptureDuration start!");
    OH_LOG_INFO(LOG_APP, "PhotoOutputEstimatedCaptureDuration");
}

PhotoOutput_Callbacks *NDKCamera::GetPhotoOutputListener(void) {
    static PhotoOutput_Callbacks photoOutputListener = {.onFrameStart = PhotoOutputOnFrameStart,
                                                        .onFrameShutter = PhotoOutputOnFrameShutter,
                                                        .onFrameEnd = PhotoOutputOnFrameEnd,
                                                        .onError = PhotoOutputOnError};
    return &photoOutputListener;
}

Camera_ErrorCode NDKCamera::PhotoOutputRegisterCallback(void) {
    ret_ = OH_PhotoOutput_RegisterCallback(photoOutput_, GetPhotoOutputListener());
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_PhotoOutput_RegisterCallback failed.");
    }
    ret_ = OH_PhotoOutput_RegisterCaptureStartWithInfoCallback(photoOutput_, PhotoOutputCaptureStartWithInfo);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_PhotoOutput_RegisterCaptureStartWithInfoCallback failed.");
    }
    ret_ = OH_PhotoOutput_RegisterCaptureEndCallback(photoOutput_, PhotoOutputCaptureEnd);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_PhotoOutput_RegisterCaptureEndCallback failed.");
    }
    ret_ = OH_PhotoOutput_RegisterFrameShutterEndCallback(photoOutput_, PhotoOutputOnFrameShutterEnd);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_PhotoOutput_RegisterFrameShutterEndCallback failed.");
    }
    ret_ = OH_PhotoOutput_RegisterCaptureReadyCallback(photoOutput_, PhotoOutputCaptureReady);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_PhotoOutput_RegisterCaptureReadyCallback failed.");
    }
    ret_ = OH_PhotoOutput_RegisterEstimatedCaptureDurationCallback(photoOutput_, PhotoOutputEstimatedCaptureDuration);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_PhotoOutput_RegisterEstimatedCaptureDurationCallback failed.");
    }
    return ret_;
}

// VideoOutput Callback
void VideoOutputOnFrameStart(Camera_VideoOutput *videoOutput) { OH_LOG_INFO(LOG_APP, "VideoOutputOnFrameStart"); }

void VideoOutputOnFrameEnd(Camera_VideoOutput *videoOutput, int32_t frameCount) {
    OH_LOG_INFO(LOG_APP, "VideoOutput frameCount = %{public}d", frameCount);
}

void VideoOutputOnError(Camera_VideoOutput *videoOutput, Camera_ErrorCode errorCode) {
    OH_LOG_INFO(LOG_APP, "VideoOutput errorCode = %{public}d", errorCode);
}

VideoOutput_Callbacks *NDKCamera::GetVideoOutputListener(void) {
    static VideoOutput_Callbacks videoOutputListener = {
        .onFrameStart = VideoOutputOnFrameStart, .onFrameEnd = VideoOutputOnFrameEnd, .onError = VideoOutputOnError};
    return &videoOutputListener;
}

Camera_ErrorCode NDKCamera::VideoOutputRegisterCallback(void) {
    ret_ = OH_VideoOutput_RegisterCallback(videoOutput_, GetVideoOutputListener());
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_VideoOutput_RegisterCallback failed.");
    }
    return ret_;
}

// Metadata Callback
void OnMetadataObjectAvailable(Camera_MetadataOutput *metadataOutput, Camera_MetadataObject *metadataObject,
                               uint32_t size) {
    OH_LOG_INFO(LOG_APP, "size = %{public}d", size);
}

void OnMetadataOutputError(Camera_MetadataOutput *metadataOutput, Camera_ErrorCode errorCode) {
    OH_LOG_INFO(LOG_APP, "OnMetadataOutput errorCode = %{public}d", errorCode);
}

MetadataOutput_Callbacks *NDKCamera::GetMetadataOutputListener(void) {
    static MetadataOutput_Callbacks metadataOutputListener = {.onMetadataObjectAvailable = OnMetadataObjectAvailable,
                                                              .onError = OnMetadataOutputError};
    return &metadataOutputListener;
}

Camera_ErrorCode NDKCamera::MetadataOutputRegisterCallback(void) {
    ret_ = OH_MetadataOutput_RegisterCallback(metadataOutput_, GetMetadataOutputListener());
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_MetadataOutput_RegisterCallback failed.");
    }
    return ret_;
}

// Session Callback
void CaptureSessionOnFocusStateChange(Camera_CaptureSession *session, Camera_FocusState focusState) {
    OH_LOG_INFO(LOG_APP, "CaptureSession_Callbacks CaptureSessionOnFocusStateChange");
    OH_LOG_INFO(LOG_APP, "CaptureSessionOnFocusStateChange");
}

void CaptureSessionOnError(Camera_CaptureSession *session, Camera_ErrorCode errorCode) {
    OH_LOG_INFO(LOG_APP, "CaptureSession_Callbacks CaptureSessionOnError");
    OH_LOG_INFO(LOG_APP, "CaptureSession errorCode = %{public}d", errorCode);
}

void CaptureSessionOnSmoothZoomInfo(Camera_CaptureSession *session, Camera_SmoothZoomInfo *smoothZoomInfo) {
    DRAWING_LOGD("CaptureSessionOnSmoothZoomInfo start!");
    OH_LOG_INFO(LOG_APP, "CaptureSessionOnSmoothZoomInfo");
}

CaptureSession_Callbacks *NDKCamera::GetCaptureSessionRegister(void) {
    static CaptureSession_Callbacks captureSessionCallbacks = {.onFocusStateChange = CaptureSessionOnFocusStateChange,
                                                               .onError = CaptureSessionOnError};
    return &captureSessionCallbacks;
}

Camera_ErrorCode NDKCamera::CaptureSessionRegisterCallback(void) {
    ret_ = OH_CaptureSession_RegisterCallback(captureSession_, GetCaptureSessionRegister());
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_RegisterCallback failed.");
    }
    ret_ = OH_CaptureSession_RegisterSmoothZoomInfoCallback(captureSession_, CaptureSessionOnSmoothZoomInfo);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_RegisterSmoothZoomInfoCallback failed.");
    }
    return ret_;
}

Camera_ErrorCode NDKCamera::GetSupportedSceneModes(void) {
    DRAWING_LOGD("NDKCamera::GetSupportedSceneModes start!");
    DRAWING_LOGD("NDKCamera::GetSupportedSceneModes cameraDeviceIndex_: %{public}d", cameraDeviceIndex_);
    ret_ = OH_CameraManager_GetSupportedSceneModes(&cameras_[cameraDeviceIndex_], &sceneModes_, &sceneModeSize_);
    if (sceneModes_ == nullptr || &sceneModeSize_ == nullptr || ret_ != CAMERA_OK) {
        DRAWING_LOGD("NDKCamera::GetSupportedSceneModes failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    DRAWING_LOGD("NDKCamera::GetSupportedSceneModes sceneModeSize_: %{public}d", sceneModeSize_);
    for (uint32_t i = 0; i < sceneModeSize_; i++) {
        DRAWING_LOGD("NDKCamera::GetSupportedSceneModes sceneMode_: %{public}d", sceneModes_[i]);
    }
    DRAWING_LOGD("NDKCamera::GetSupportedSceneModes return with ret code: %{public}d!", ret_);
    return ret_;
}
Camera_ErrorCode NDKCamera::GetSupportedOutputCapabilityWithSceneMode(Camera_SceneMode sceneMode) {
    DRAWING_LOGD("NDKCamera::GetSupportedOutputCapabilityWithSceneMode start!");
    DRAWING_LOGD("NDKCamera::GetSupportedOutputCapabilityWithSceneMode sceneMode: %{public}d", sceneMode);
    DRAWING_LOGD("NDKCamera::GetSupportedOutputCapabilityWithSceneMode cameraDeviceIndex_: %{public}d",
                 cameraDeviceIndex_);
    ret_ = OH_CameraManager_GetSupportedCameraOutputCapabilityWithSceneMode(
        cameraManager_, &cameras_[cameraDeviceIndex_], sceneMode, &cameraOutputCapability_);
    if (cameraOutputCapability_ == nullptr || ret_ != CAMERA_OK) {
        DRAWING_LOGD("NDKCamera::GetSupportedOutputCapabilityWithSceneMode failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    DRAWING_LOGD("NDKCamera::GetSupportedOutputCapabilityWithSceneMode sceneMode: %{public}d "
                 "previewProfilesSize: %{public}d!",
                 sceneMode, cameraOutputCapability_->previewProfilesSize);
    DRAWING_LOGD("NDKCamera::GetSupportedOutputCapabilityWithSceneMode sceneMode: %{public}d "
                 "previewProfiles[0] format: %{public}d!",
                 sceneMode, cameraOutputCapability_->previewProfiles[0]->format);
    DRAWING_LOGD("NDKCamera::GetSupportedOutputCapabilityWithSceneMode sceneMode: %{public}d "
                 "previewProfiles[0] width: %{public}d!",
                 sceneMode, cameraOutputCapability_->previewProfiles[0]->size.width);
    DRAWING_LOGD("NDKCamera::GetSupportedOutputCapabilityWithSceneMode sceneMode: %{public}d "
                 "previewProfiles[0] height: %{public}d!",
                 sceneMode, cameraOutputCapability_->previewProfiles[0]->size.height);
    DRAWING_LOGD("NDKCamera::GetSupportedOutputCapabilityWithSceneMode sceneMode: %{public}d "
                 "photoProfilesSize: %{public}d!",
                 sceneMode, cameraOutputCapability_->photoProfilesSize);
    DRAWING_LOGD("NDKCamera::GetSupportedOutputCapabilityWithSceneMode sceneMode: %{public}d "
                 "photoProfiles[0] format: %{public}d!",
                 sceneMode, cameraOutputCapability_->photoProfiles[0]->format);
    DRAWING_LOGD("NDKCamera::GetSupportedOutputCapabilityWithSceneMode sceneMode: %{public}d "
                 "photoProfiles[0] width: %{public}d!",
                 sceneMode, cameraOutputCapability_->photoProfiles[0]->size.width);
    DRAWING_LOGD("NDKCamera::GetSupportedOutputCapabilityWithSceneMode sceneMode: %{public}d "
                 "photoProfiles[0] height: %{public}d!",
                 sceneMode, cameraOutputCapability_->photoProfiles[0]->size.height);
    if (sceneMode == Camera_SceneMode::NORMAL_VIDEO) {
        DRAWING_LOGD("NDKCamera::GetSupportedOutputCapabilityWithSceneMode sceneMode: %{public}d "
                     "videoProfilesSize: %{public}d!",
                     sceneMode, cameraOutputCapability_->videoProfilesSize);
        DRAWING_LOGD("NDKCamera::GetSupportedOutputCapabilityWithSceneMode sceneMode: %{public}d "
                     "videoProfiles[0] format: %{public}d!",
                     sceneMode, cameraOutputCapability_->videoProfiles[0]->format);
        DRAWING_LOGD("NDKCamera::GetSupportedOutputCapabilityWithSceneMode sceneMode: %{public}d "
                     "videoProfiles[0] width: %{public}d!",
                     sceneMode, cameraOutputCapability_->videoProfiles[0]->size.width);
        DRAWING_LOGD("NDKCamera::GetSupportedOutputCapabilityWithSceneMode sceneMode: %{public}d "
                     "videoProfiles[0] height: %{public}d!",
                     sceneMode, cameraOutputCapability_->videoProfiles[0]->size.height);
        DRAWING_LOGD("NDKCamera::GetSupportedOutputCapabilityWithSceneMode sceneMode: %{public}d "
                     "videoProfiles[0] range.min: %{public}d!",
                     sceneMode, cameraOutputCapability_->videoProfiles[0]->range.min);
        DRAWING_LOGD("NDKCamera::GetSupportedOutputCapabilityWithSceneMode sceneMode: %{public}d "
                     "videoProfiles[0] range.max: %{public}d!",
                     sceneMode, cameraOutputCapability_->videoProfiles[0]->range.max);
        DRAWING_LOGD("NDKCamera::GetSupportedOutputCapabilityWithSceneMode sceneMode: %{public}d "
                     "metadataProfilesSize: %{public}d!",
                     sceneMode, cameraOutputCapability_->metadataProfilesSize);
    }
    DRAWING_LOGD("NDKCamera::GetSupportedOutputCapabilityWithSceneMode sceneMode: %{public}d "
                 "return with ret code: %{public}d!",
                 sceneMode, ret_);
    return ret_;
}
Camera_ErrorCode NDKCamera::CanAddInput(bool *isSuccess) {
    DRAWING_LOGD("NDKCamera::CanAddInput start!");
    ret_ = OH_CaptureSession_CanAddInput(captureSession_, cameraInput_, isSuccess);
    if (ret_ != CAMERA_OK) {
        DRAWING_LOGD("NDKCamera::CanAddInput failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    DRAWING_LOGD("NDKCamera::CanAddInput isSuccess: %{public}d!", *isSuccess);
    DRAWING_LOGD("NDKCamera::CanAddInput return with ret code: %{public}d!", ret_);
    return ret_;
}
Camera_ErrorCode NDKCamera::CanAddPreviewOutput(bool *isSuccess) {
    DRAWING_LOGD("NDKCamera::CanAddPreviewOutput start!");
    ret_ = OH_CaptureSession_CanAddPreviewOutput(captureSession_, previewOutput_, isSuccess);
    if (ret_ != CAMERA_OK) {
        DRAWING_LOGD("NDKCamera::CanAddPreviewOutput failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    DRAWING_LOGD("NDKCamera::CanAddPreviewOutput isSuccess: %{public}d!", *isSuccess);
    DRAWING_LOGD("NDKCamera::CanAddPreviewOutput return with ret code: %{public}d!", ret_);
    return ret_;
}
Camera_ErrorCode NDKCamera::CanAddPhotoOutput(bool *isSuccess) {
    DRAWING_LOGD("NDKCamera::CanAddPhotoOutput start!");
    ret_ = OH_CaptureSession_CanAddPhotoOutput(captureSession_, photoOutput_, isSuccess);
    if (ret_ != CAMERA_OK) {
        DRAWING_LOGD("NDKCamera::CanAddPhotoOutput failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    DRAWING_LOGD("NDKCamera::CanAddPhotoOutput isSuccess: %{public}d!", *isSuccess);
    DRAWING_LOGD("NDKCamera::CanAddPhotoOutput return with ret code: %{public}d!", ret_);
    return ret_;
}
Camera_ErrorCode NDKCamera::CanAddVideoOutput(bool *isSuccess) {
    DRAWING_LOGD("NDKCamera::CanAddVideoOutput start!");
    ret_ = OH_CaptureSession_CanAddVideoOutput(captureSession_, videoOutput_, isSuccess);
    if (ret_ != CAMERA_OK) {
        DRAWING_LOGD("NDKCamera::CanAddVideoOutput failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    DRAWING_LOGD("NDKCamera::CanAddVideoOutput isSuccess: %{public}d!", *isSuccess);
    DRAWING_LOGD("NDKCamera::CanAddVideoOutput return with ret code: %{public}d!", ret_);
    return ret_;
}
Camera_ErrorCode NDKCamera::CanPreconfig(bool *isSuccess) {
    DRAWING_LOGD("NDKCamera::CanPreconfig start!");
    ret_ = OH_CaptureSession_CanPreconfig(captureSession_, preconfigType_, isSuccess);
    if (ret_ != CAMERA_OK) {
        DRAWING_LOGD("NDKCamera::CanPreconfig failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    DRAWING_LOGD("NDKCamera::CanPreconfig isSuccess: %{public}d!", *isSuccess);
    DRAWING_LOGD("NDKCamera::CanPreconfig return with ret code: %{public}d!", ret_);
    return ret_;
}
Camera_ErrorCode NDKCamera::Preconfig(void) {
    DRAWING_LOGD("NDKCamera::Preconfig start!");
    ret_ = OH_CaptureSession_Preconfig(captureSession_, preconfigType_);
    if (ret_ != CAMERA_OK) {
        DRAWING_LOGD("NDKCamera::Preconfig failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    DRAWING_LOGD("NDKCamera::Preconfig return with ret code: %{public}d!", ret_);
    return ret_;
}
Camera_ErrorCode NDKCamera::CanPreconfigWithRatio(bool *isSuccess) {
    DRAWING_LOGD("NDKCamera::CanPreconfigWithRatio start!");
    ret_ = OH_CaptureSession_CanPreconfigWithRatio(captureSession_, preconfigType_, preconfigRatio_, isSuccess);
    if (ret_ != CAMERA_OK) {
        DRAWING_LOGD("NDKCamera::CanPreconfigWithRatio failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    DRAWING_LOGD("NDKCamera::CanPreconfigWithRatio isSuccess: %{public}d!", *isSuccess);
    DRAWING_LOGD("NDKCamera::CanPreconfigWithRatio return with ret code: %{public}d!", ret_);
    return ret_;
}
Camera_ErrorCode NDKCamera::PreconfigWithRatio(void) {
    DRAWING_LOGD("NDKCamera::PreconfigWithRatio start!");
    ret_ = OH_CaptureSession_PreconfigWithRatio(captureSession_, preconfigType_, preconfigRatio_);
    if (ret_ != CAMERA_OK) {
        DRAWING_LOGD("NDKCamera::PreconfigWithRatio failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    DRAWING_LOGD("NDKCamera::PreconfigWithRatio return with ret code: %{public}d!", ret_);
    return ret_;
}
Camera_ErrorCode NDKCamera::GetActivePreviewOutputProfile(void) {
    DRAWING_LOGD("NDKCamera::GetActivePreviewOutputProfile start!");
    ret_ = OH_PreviewOutput_GetActiveProfile(previewOutput_, &activePreviewProfile_);
    if (activePreviewProfile_ == nullptr || ret_ != CAMERA_OK) {
        DRAWING_LOGD("NDKCamera::GetActivePreviewOutputProfile failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    DRAWING_LOGD("NDKCamera::GetActivePreviewOutputProfile activePreviewProfile_ format: %{public}d!",
                 activePreviewProfile_->format);
    DRAWING_LOGD("NDKCamera::GetActivePreviewOutputProfile activePreviewProfile_ width: %{public}d!",
                 activePreviewProfile_->size.width);
    DRAWING_LOGD("NDKCamera::GetActivePreviewOutputProfile activePreviewProfile_ height: %{public}d!",
                 activePreviewProfile_->size.height);
    DRAWING_LOGD("NDKCamera::GetActivePreviewOutputProfile return with ret code: %{public}d!", ret_);
    return ret_;
}
Camera_ErrorCode NDKCamera::GetActivePhotoOutputProfile(void) {
    DRAWING_LOGD("NDKCamera::GetActivePhotoOutputProfile start!");
    ret_ = OH_PhotoOutput_GetActiveProfile(photoOutput_, &activePhotoProfile_);
    if (activePhotoProfile_ == nullptr || ret_ != CAMERA_OK) {
        DRAWING_LOGD("NDKCamera::GetActivePhotoOutputProfile failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    DRAWING_LOGD("NDKCamera::GetActivePhotoOutputProfile activePhotoProfile_ format: %{public}d!",
                 activePhotoProfile_->format);
    DRAWING_LOGD("NDKCamera::GetActivePhotoOutputProfile activePhotoProfile_ width: %{public}d!",
                 activePhotoProfile_->size.width);
    DRAWING_LOGD("NDKCamera::GetActivePhotoOutputProfile activePhotoProfile_ height: %{public}d!",
                 activePhotoProfile_->size.height);
    DRAWING_LOGD("NDKCamera::GetActivePhotoOutputProfile return with ret code: %{public}d!", ret_);
    return ret_;
}
Camera_ErrorCode NDKCamera::GetActiveVideoOutputProfile(void) {
    DRAWING_LOGD("NDKCamera::GetActiveVideoOutputProfile start!");
    ret_ = OH_VideoOutput_GetActiveProfile(videoOutput_, &activeVideoProfile_);
    if (activeVideoProfile_ == nullptr || ret_ != CAMERA_OK) {
        DRAWING_LOGD("NDKCamera::GetActiveVideoOutputProfile failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    DRAWING_LOGD("NDKCamera::GetActiveVideoOutputProfile activeVideoProfile_ format: %{public}d!",
                 activeVideoProfile_->format);
    DRAWING_LOGD("NDKCamera::GetActiveVideoOutputProfile activeVideoProfile_ width: %{public}d!",
                 activeVideoProfile_->size.width);
    DRAWING_LOGD("NDKCamera::GetActiveVideoOutputProfile activeVideoProfile_ height: %{public}d!",
                 activeVideoProfile_->size.height);
    DRAWING_LOGD("NDKCamera::GetActiveVideoOutputProfile activeVideoProfile_ range.min: %{public}d!",
                 activeVideoProfile_->range.min);
    DRAWING_LOGD("NDKCamera::GetActiveVideoOutputProfile activeVideoProfile_ range.max: %{public}d!",
                 activeVideoProfile_->range.max);
    DRAWING_LOGD("NDKCamera::GetActiveVideoOutputProfile return with ret code: %{public}d!", ret_);
    return ret_;
}

Camera_ErrorCode NDKCamera::IsTorchSupported(bool *isSuccess) {
    DRAWING_LOGD("NDKCamera::IsTorchSupported start!");
    *isSuccess = false;
    ret_ = OH_CameraManager_IsTorchSupported(cameraManager_, isSuccess);
    if (ret_ != CAMERA_OK) {
        DRAWING_LOGD("NDKCamera::IsTorchSupported failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    DRAWING_LOGD("NDKCamera::IsTorchSupported isSuccess: %{public}d!", *isSuccess);
    DRAWING_LOGD("NDKCamera::IsTorchSupported return with ret code: %{public}d!", ret_);
    return ret_;
}
Camera_ErrorCode NDKCamera::IsTorchSupportedByTorchMode(bool *isSuccess) {
    DRAWING_LOGD("NDKCamera::IsTorchSupportedByTorchMode start!");
    *isSuccess = false;
    ret_ = OH_CameraManager_IsTorchSupportedByTorchMode(cameraManager_, Camera_TorchMode::OFF, isSuccess);
    if (ret_ != CAMERA_OK) {
        DRAWING_LOGD("NDKCamera::IsTorchSupportedByTorchMode failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    DRAWING_LOGD("NDKCamera::IsTorchSupportedByTorchMode mode: %{public}d isSuccess: %{public}d!",
                 Camera_TorchMode::OFF, *isSuccess);
    ret_ = OH_CameraManager_IsTorchSupportedByTorchMode(cameraManager_, Camera_TorchMode::ON, isSuccess);
    if (ret_ != CAMERA_OK) {
        DRAWING_LOGD("NDKCamera::IsTorchSupportedByTorchMode failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    DRAWING_LOGD("NDKCamera::IsTorchSupportedByTorchMode mode: %{public}d isSuccess: %{public}d!", Camera_TorchMode::ON,
                 *isSuccess);
    ret_ = OH_CameraManager_IsTorchSupportedByTorchMode(cameraManager_, Camera_TorchMode::AUTO, isSuccess);
    if (ret_ != CAMERA_OK) {
        DRAWING_LOGD("NDKCamera::IsTorchSupportedByTorchMode failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    DRAWING_LOGD("NDKCamera::IsTorchSupportedByTorchMode mode: %{public}d isSuccess: %{public}d!",
                 Camera_TorchMode::AUTO, *isSuccess);
    DRAWING_LOGD("NDKCamera::IsTorchSupportedByTorchMode return with ret code: %{public}d!", ret_);
    return ret_;
}
Camera_ErrorCode NDKCamera::SetTorchMode(void) {
    DRAWING_LOGD("NDKCamera::SetTorchMode start!");
    bool isSuccess = false;
    OH_CameraManager_IsTorchSupportedByTorchMode(cameraManager_, Camera_TorchMode::OFF, &isSuccess);
    if (!isSuccess) {
        DRAWING_LOGI("[wqg log] NDKCamera::SetTorchMode this torchMode is not supported.");
        return CAMERA_OK;
    }
    ret_ = OH_CameraManager_SetTorchMode(cameraManager_, Camera_TorchMode::OFF);
    DRAWING_LOGD("NDKCamera::SetTorchMode return with ret code: %{public}d!", ret_);
    if (ret_ != CAMERA_OK) {
        DRAWING_LOGD("NDKCamera::SetTorchMode failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    return ret_;
}
Camera_ErrorCode NDKCamera::SetTorchMode(int32_t torchMode) {
    DRAWING_LOGD("NDKCamera::SetTorchMode start! torchMode is %{public}d!", torchMode);
    bool isSuccess = false;
    OH_CameraManager_IsTorchSupportedByTorchMode(cameraManager_, static_cast<Camera_TorchMode>(torchMode), &isSuccess);
    if (!isSuccess) {
        DRAWING_LOGI("[wqg log] NDKCamera::SetTorchMode this torchMode is not supported.");
        return CAMERA_OK;
    }
    ret_ = OH_CameraManager_SetTorchMode(cameraManager_, static_cast<Camera_TorchMode>(torchMode));
    DRAWING_LOGD("NDKCamera::SetTorchMode return with ret code: %{public}d!", ret_);
    if (ret_ != CAMERA_OK) {
        DRAWING_LOGD("NDKCamera::SetTorchMode failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    return ret_;
}
Camera_ErrorCode NDKCamera::GetExposureValue(void) {
    DRAWING_LOGD("NDKCamera::GetExposureValue start!");
    ret_ = OH_CaptureSession_GetExposureValue(captureSession_, &exposureValue_);
    if (ret_ != CAMERA_OK) {
        DRAWING_LOGD("NDKCamera::GetExposureValue failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    DRAWING_LOGD("NDKCamera::GetExposureValue return with ret code: %{public}d!", ret_);
    DRAWING_LOGD("NDKCamera::GetExposureValue exposureValue_: %{public}f!", exposureValue_);
    return ret_;
}
Camera_ErrorCode NDKCamera::GetFocalLength(void) {
    DRAWING_LOGD("NDKCamera::GetFocalLength start!");
    ret_ = OH_CaptureSession_GetFocalLength(captureSession_, &focalLength_);
    if (ret_ != CAMERA_OK) {
        DRAWING_LOGD("NDKCamera::GetFocalLength failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    DRAWING_LOGD("NDKCamera::GetFocalLength return with ret code: %{public}d!", ret_);
    DRAWING_LOGD("NDKCamera::GetFocalLength focalLength_: %{public}f!", focalLength_);
    return ret_;
}
Camera_ErrorCode NDKCamera::SetSmoothZoom(void) {
    DRAWING_LOGD("NDKCamera::SetSmoothZoom start!");
    ret_ = OH_CaptureSession_SetSmoothZoom(captureSession_, 1, Camera_SmoothZoomMode::NORMAL);
    if (ret_ != CAMERA_OK) {
        DRAWING_LOGD("NDKCamera::SetSmoothZoom failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    DRAWING_LOGD("NDKCamera::SetSmoothZoom return with ret code: %{public}d!", ret_);
    return ret_;
}
Camera_ErrorCode NDKCamera::GetSupportedColorSpaces(void) {
    DRAWING_LOGD("NDKCamera::GetSupportedColorSpaces start!");
    ret_ = OH_CaptureSession_GetSupportedColorSpaces(captureSession_, &colorSpaces_, &colorSpaceSize_);
    if (ret_ != CAMERA_OK) {
        DRAWING_LOGD("NDKCamera::GetSupportedColorSpaces failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    if (colorSpaces_ == nullptr || &colorSpaceSize_ == nullptr) {
        DRAWING_LOGI("[wqg log] NDKCamera::GetSupportedColorSpaces colorSpaceInfo is empty.");
        return CAMERA_OK;
    }
    DRAWING_LOGD("NDKCamera::GetSupportedColorSpaces colorSpaceSize_: %{public}d", colorSpaceSize_);
    for (uint32_t i = 0; i < colorSpaceSize_; i++) {
        DRAWING_LOGD("NDKCamera::GetSupportedColorSpaces colorSpace_: %{public}d", colorSpaces_[i]);
    }
    DRAWING_LOGD("NDKCamera::GetSupportedColorSpaces return with ret code: %{public}d!", ret_);
    return ret_;
}
Camera_ErrorCode NDKCamera::GetActiveColorSpace(void) {
    DRAWING_LOGD("NDKCamera::GetActiveColorSpace start!");
    ret_ = OH_CaptureSession_GetActiveColorSpace(captureSession_, &colorSpace_);
    if (&colorSpace_ == nullptr || ret_ != CAMERA_OK) {
        DRAWING_LOGD("NDKCamera::GetActiveColorSpace failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    DRAWING_LOGD("NDKCamera::GetActiveColorSpace colorSpace_: %{public}d!", colorSpace_);
    DRAWING_LOGD("NDKCamera::GetActiveColorSpace return with ret code: %{public}d!", ret_);
    return ret_;
}
Camera_ErrorCode NDKCamera::SetActiveColorSpace(void) {
    DRAWING_LOGD("NDKCamera::SetActiveColorSpace start!");
    if (colorSpaces_ != nullptr && colorSpaceSize_ != 0) {
        ret_ = OH_CaptureSession_SetActiveColorSpace(captureSession_, colorSpaces_[0]);
        if (ret_ != CAMERA_OK) {
            DRAWING_LOGD("NDKCamera::SetActiveColorSpace failed.");
            return CAMERA_INVALID_ARGUMENT;
        }
        DRAWING_LOGD("NDKCamera::SetActiveColorSpace return with ret code: %{public}d!", ret_);
        return ret_;
    }
    return CAMERA_OK;
}
Camera_ErrorCode NDKCamera::PreviewOutputGetSupportedFrameRates(void) {
    DRAWING_LOGD("NDKCamera::PreviewOutputGetSupportedFrameRates start!");
    ret_ = OH_PreviewOutput_GetSupportedFrameRates(previewOutput_, &frameRateRanges_, &frameRateRangeSize_);

    if (frameRateRanges_ == nullptr || ret_ != CAMERA_OK) {
        DRAWING_LOGD("NDKCamera::PreviewOutputGetSupportedFrameRates failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    DRAWING_LOGD("NDKCamera::PreviewOutputGetSupportedFrameRates frameRateRangeSize_: %{public}d", frameRateRangeSize_);
    for (uint32_t i = 0; i < frameRateRangeSize_; i++) {
        DRAWING_LOGD("NDKCamera::PreviewOutputGetSupportedFrameRates frameRateRange_ min: %{public}d",
                     frameRateRanges_[i].min);
        DRAWING_LOGD("NDKCamera::PreviewOutputGetSupportedFrameRates frameRateRange_ max: %{public}d",
                     frameRateRanges_[i].max);
    }
    DRAWING_LOGD("NDKCamera::PreviewOutputGetSupportedFrameRates return with ret code: %{public}d!", ret_);
    return ret_;
}
Camera_ErrorCode NDKCamera::PreviewOutputSetFrameRate(void) {
    DRAWING_LOGD("NDKCamera::PreviewOutputSetFrameRate start!");
    if (frameRateRanges_ != nullptr && frameRateRangeSize_ != 0) {
        ret_ = OH_PreviewOutput_SetFrameRate(previewOutput_, frameRateRanges_[0].min, frameRateRanges_[0].max);
        DRAWING_LOGD("NDKCamera::PreviewOutputSetFrameRate return with ret code: %{public}d!", ret_);
        if (ret_ != CAMERA_OK) {
            DRAWING_LOGD("NDKCamera::PreviewOutputSetFrameRate failed.");
            return CAMERA_INVALID_ARGUMENT;
        }
        return ret_;
    }
    return CAMERA_OK;
}
Camera_ErrorCode NDKCamera::PreviewOutputGetActiveFrameRate(void) {
    DRAWING_LOGD("NDKCamera::PreviewOutputGetActiveFrameRate start!");
    ret_ = OH_PreviewOutput_GetActiveFrameRate(previewOutput_, &frameRateRange_);

    if (ret_ != CAMERA_OK) {
        DRAWING_LOGD("NDKCamera::PreviewOutputGetActiveFrameRate failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    DRAWING_LOGD("NDKCamera::PreviewOutputGetActiveFrameRate frameRateRange_ min: %{public}d", frameRateRange_.min);
    DRAWING_LOGD("NDKCamera::PreviewOutputGetActiveFrameRate frameRateRange_ max: %{public}d", frameRateRange_.max);
    DRAWING_LOGD("NDKCamera::PreviewOutputGetActiveFrameRate return with ret code: %{public}d!", ret_);
    return ret_;
}
Camera_ErrorCode NDKCamera::VideoOutputGetSupportedFrameRates(void) {
    DRAWING_LOGD("NDKCamera::VideoOutputGetSupportedFrameRates start!");
    ret_ = OH_VideoOutput_GetSupportedFrameRates(videoOutput_, &frameRateRanges_, &frameRateRangeSize_);

    if (frameRateRanges_ == nullptr || ret_ != CAMERA_OK) {
        DRAWING_LOGD("NDKCamera::VideoOutputGetSupportedFrameRates failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    DRAWING_LOGD("NDKCamera::VideoOutputGetSupportedFrameRates frameRateRangeSize_: %{public}d", frameRateRangeSize_);
    for (uint32_t i = 0; i < frameRateRangeSize_; i++) {
        DRAWING_LOGD("NDKCamera::VideoOutputGetSupportedFrameRates frameRateRange_ min: %{public}d",
                     frameRateRanges_[i].min);
        DRAWING_LOGD("NDKCamera::VideoOutputGetSupportedFrameRates frameRateRange_ max: %{public}d",
                     frameRateRanges_[i].max);
    }
    DRAWING_LOGD("NDKCamera::VideoOutputGetSupportedFrameRates return with ret code: %{public}d!", ret_);
    return ret_;
}
Camera_ErrorCode NDKCamera::VideoOutputSetFrameRate(void) {
    DRAWING_LOGD("NDKCamera::VideoOutputSetFrameRate start!");
    if (frameRateRanges_ != nullptr && frameRateRangeSize_ != 0) {
        ret_ = OH_VideoOutput_SetFrameRate(videoOutput_, frameRateRanges_[0].min, frameRateRanges_[0].max);
        if (ret_ != CAMERA_OK) {
            DRAWING_LOGD("NDKCamera::VideoOutputSetFrameRate failed.");
            return CAMERA_INVALID_ARGUMENT;
        }
        DRAWING_LOGD("NDKCamera::VideoOutputSetFrameRate return with ret code: %{public}d!", ret_);
        return ret_;
    }
    return CAMERA_OK;
}
Camera_ErrorCode NDKCamera::VideoOutputGetActiveFrameRate(void) {
    DRAWING_LOGD("NDKCamera::VideoOutputGetActiveFrameRate start!");
    ret_ = OH_VideoOutput_GetActiveFrameRate(videoOutput_, &frameRateRange_);

    if (ret_ != CAMERA_OK) {
        DRAWING_LOGD("NDKCamera::VideoOutputGetActiveFrameRate failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    DRAWING_LOGD("NDKCamera::VideoOutputGetActiveFrameRate frameRateRange_ min: %{public}d", frameRateRange_.min);
    DRAWING_LOGD("NDKCamera::VideoOutputGetActiveFrameRate frameRateRange_ max: %{public}d", frameRateRange_.max);
    DRAWING_LOGD("NDKCamera::VideoOutputGetActiveFrameRate return with ret code: %{public}d!", ret_);
    return ret_;
}

void onPhotoAvailable(Camera_PhotoOutput *photoOutput, OH_PhotoNative *photo) {
    DRAWING_LOGD("RM007 Log onPhotoAvailable start!");
    NDKCamera::PhotoNativeGetMainImage(photo);
    NDKCamera::PhotoNativeRelease(photo);
    DRAWING_LOGD("RM007 Log onPhotoAvailable finish!");
}
Camera_ErrorCode NDKCamera::PhotoOutputRegisterPhotoAvailableCallback(void) {
    DRAWING_LOGD("RM007 Log NDKCamera::PhotoOutputRegisterPhotoAvailableCallback start!");
    ret_ = OH_PhotoOutput_RegisterPhotoAvailableCallback(photoOutput_, onPhotoAvailable);
    if (ret_ != CAMERA_OK) {
        DRAWING_LOGD("RM007 Log NDKCamera::PhotoOutputRegisterPhotoAvailableCallback failed.");
    }
    DRAWING_LOGD("RM007 Log NDKCamera::PhotoOutputRegisterPhotoAvailableCallback return with ret code: %{public}d!",
                 ret_);
    return ret_;
}
Camera_ErrorCode NDKCamera::PhotoOutputUnRegisterPhotoAvailableCallback(void) {
    DRAWING_LOGD("RM007 Log NDKCamera::PhotoOutputUnRegisterPhotoAvailableCallback start!");
    ret_ = OH_PhotoOutput_UnregisterPhotoAvailableCallback(photoOutput_, onPhotoAvailable);
    if (ret_ != CAMERA_OK) {
        DRAWING_LOGD("RM007 Log NDKCamera::PhotoOutputUnRegisterPhotoAvailableCallback failed.");
    }
    DRAWING_LOGD("RM007 Log NDKCamera::PhotoOutputUnRegisterPhotoAvailableCallback return with ret code: %{public}d!",
                 ret_);
    return ret_;
}
void onPhotoAssetAvailable(Camera_PhotoOutput *photoOutput, OH_MediaAsset *mediaAsset) {
    DRAWING_LOGD("RM007 Log onPhotoAssetAvailable start!");
    // NDKCamera::MediaAssetGetUri(mediaAsset);
    DRAWING_LOGD("RM007 Log onPhotoAssetAvailable finish!");
    return;
}
Camera_ErrorCode NDKCamera::PhotoOutputRegisterPhotoAssetAvailableCallback(void) {
    DRAWING_LOGD("RM007 Log NDKCamera::PhotoOutputRegisterPhotoAssetAvailableCallback start!");
    ret_ = OH_PhotoOutput_RegisterPhotoAssetAvailableCallback(photoOutput_, onPhotoAssetAvailable);
    if (ret_ != CAMERA_OK) {
        DRAWING_LOGD("RM007 Log NDKCamera::PhotoOutputRegisterPhotoAssetAvailableCallback failed.");
    }
    DRAWING_LOGD(
        "RM007 Log NDKCamera::PhotoOutputRegisterPhotoAssetAvailableCallback return with ret code: %{public}d!", ret_);
    return ret_;
}
Camera_ErrorCode NDKCamera::PhotoOutputUnRegisterPhotoAssetAvailableCallback(void) {
    DRAWING_LOGD("RM007 Log NDKCamera::PhotoOutputUnRegisterPhotoAssetAvailableCallback start!");
    ret_ = OH_PhotoOutput_UnregisterPhotoAssetAvailableCallback(photoOutput_, onPhotoAssetAvailable);
    if (ret_ != CAMERA_OK) {
        DRAWING_LOGD("RM007 Log NDKCamera::PhotoOutputUnRegisterPhotoAssetAvailableCallback failed.");
    }
    DRAWING_LOGD(
        "RM007 Log NDKCamera::PhotoOutputUnRegisterPhotoAssetAvailableCallback return with ret code: %{public}d!",
        ret_);
    return ret_;
}
Camera_ErrorCode NDKCamera::PhotoNativeGetMainImage(OH_PhotoNative *photoNative) {
    DRAWING_LOGD("RM007 Log NDKCamera::PhotoNativeGetMainImage start!");
    Camera_ErrorCode result = CAMERA_SERVICE_FATAL_ERROR;
    OH_ImageNative *imageNative = nullptr;
    OH_NativeBuffer *nativeBuffer = nullptr;
    size_t size;
    auto cb = (void (*)(uint8_t *, size_t))(requestImageCallback);
    result = OH_PhotoNative_GetMainImage(photoNative, &imageNative);
    OH_ImageNative_GetByteBuffer(imageNative, 4, &nativeBuffer);
    OH_ImageNative_GetBufferSize(imageNative, 4, &size);
    void *virAddr = nullptr;
    OH_NativeBuffer_Map(nativeBuffer, &virAddr);
    std::string fileName = "/data/storage/el2/base/files/test.jpg";
    std::ofstream outFile(fileName, std::ofstream::out);
    OH_LOG_ERROR(LOG_APP, "liyan uri1 = %{public}p", nativeBuffer);
    OH_LOG_ERROR(LOG_APP, "liyan uri2 = %{public}d", size);
    outFile.is_open();
    outFile.write(reinterpret_cast<const char *>(virAddr), size);
    outFile.close();
    cb((uint8_t *)virAddr, size);
   
    //OH_NativeBuffer_Unmap(nativeBuffer);

    if (imageNative == nullptr || result != CAMERA_OK) {
        DRAWING_LOGD("RM007 Log NDKCamera::PhotoNativeGetMainImage failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    //     Image_Size size;
    //     if (OH_ImageNative_GetImageSize(imageNative, &size) != IMAGE_SUCCESS) {
    //         DRAWING_LOGD("RM007 Log NDKCamera::OH_ImageNative_GetImageSize return with ret code: %{public}d!",
    //         result); return CAMERA_INVALID_ARGUMENT;
    //     }
    //
    //     DRAWING_LOGD("RM007 Log NDKCamera::PhotoNativeGetMainImage return with ret code: %{public}d!", result);
    //     DRAWING_LOGD("RM007 Log NDKCamera::PhotoNativeGetMainImage return with size.width: %{public}d!", size.width);
    //     DRAWING_LOGD("RM007 Log NDKCamera::PhotoNativeGetMainImage return with size.height: %{public}d!",
    //     size.height);
    DRAWING_LOGD("RM007 Log NDKCamera::PhotoNativeGetMainImage return with ret code: %{public}d!", result);
    return result;
}
Camera_ErrorCode NDKCamera::PhotoNativeRelease(OH_PhotoNative *photoNative) {
    DRAWING_LOGD("RM007 Log NDKCamera::PhotoNativeRelease start!");
    Camera_ErrorCode result = CAMERA_OK;
    if (photoNative != nullptr) {
        result = OH_PhotoNative_Release(photoNative);
        if (result != CAMERA_OK) {
            DRAWING_LOGD("RM007 Log NDKCamera::PhotoNativeRelease failed.");
        }
    }
    DRAWING_LOGD("RM007 Log NDKCamera::PhotoNativeRelease return with ret code: %{public}d!", result);
    return result;
}

// MediaLibrary_ErrorCode NDKCamera::MediaAssetGetUri(OH_MediaAsset* mediaAsset)
// {
//     DRAWING_LOGD("NDKCamera::MediaAssetGetUri start!");
//     const char* uri = nullptr;
//     result = OH_MediaAsset_GetUri(mediaAsset, &uri);
//     if (uri == nullptr || result != MEDIA_LIBRARY_OK) {
//         DRAWING_LOGD("NDKCamera::MediaAssetGetUri failed.");
//     }
//     DRAWING_LOGD("NDKCamera::MediaAssetGetUri uri: %s", uri);
//     DRAWING_LOGD("NDKCamera::MediaAssetGetUri return with ret code: %{public}d!", result);
//     return result;
// }

Camera_ErrorCode NDKCamera::UnregisterCallback(void) {
    DRAWING_LOGD("NDKCamera::UnregisterCallback start!");
    ret_ = OH_CaptureSession_UnregisterCallback(captureSession_, GetCaptureSessionRegister());
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_UnregisterCallback failed.");
    }
    ret_ = OH_CaptureSession_UnregisterSmoothZoomInfoCallback(captureSession_, CaptureSessionOnSmoothZoomInfo);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_UnregisterSmoothZoomInfoCallback failed.");
    }
    ret_ = OH_CameraManager_UnregisterTorchStatusCallback(cameraManager_, CameraManagerTorchStatusCallback);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_CameraManager_UnregisterTorchStatusCallback failed.");
    }
    ret_ = OH_PhotoOutput_UnregisterCaptureStartWithInfoCallback(photoOutput_, PhotoOutputCaptureStartWithInfo);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_PhotoOutput_UnregisterCaptureStartWithInfoCallback failed.");
    }
    ret_ = OH_PhotoOutput_UnregisterCaptureEndCallback(photoOutput_, PhotoOutputCaptureEnd);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_PhotoOutput_UnregisterCaptureEndCallback failed.");
    }
    ret_ = OH_PhotoOutput_UnregisterFrameShutterEndCallback(photoOutput_, PhotoOutputOnFrameShutterEnd);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_PhotoOutput_UnregisterFrameShutterEndCallback failed.");
    }
    ret_ = OH_PhotoOutput_UnregisterCaptureReadyCallback(photoOutput_, PhotoOutputCaptureReady);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_PhotoOutput_UnregisterCaptureReadyCallback failed.");
    }
    ret_ = OH_PhotoOutput_UnregisterEstimatedCaptureDurationCallback(photoOutput_, PhotoOutputEstimatedCaptureDuration);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_PhotoOutput_UnregisterEstimatedCaptureDurationCallback failed.");
    }
    ret_ = OH_PhotoOutput_UnregisterPhotoAvailableCallback(photoOutput_, onPhotoAvailable);
    if (ret_ != CAMERA_OK) {
        DRAWING_LOGD("NDKCamera::PhotoOutputUnRegisterPhotoAvailableCallback failed.");
    }
    ret_ = OH_PhotoOutput_UnregisterPhotoAssetAvailableCallback(photoOutput_, onPhotoAssetAvailable);
    if (ret_ != CAMERA_OK) {
        DRAWING_LOGD("NDKCamera::PhotoOutputUnRegisterPhotoAssetAvailableCallback failed.");
    }
    return ret_;
}

} // namespace OHOS_CAMERA_SAMPLE