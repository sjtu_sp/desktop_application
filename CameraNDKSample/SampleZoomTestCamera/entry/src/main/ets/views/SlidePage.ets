/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Zoom component
// For changes in components - can only be integers 1 to 6
import cameraDemo from 'libentry.so'
import Logger from '../model/Logger';

const TAG: string = 'SlidePage';

export class SliderValue {
  min: number = 1;
  max: number = 6;
  step: number = 0.1;
  outSetValueOne: number = 1;
}

@Component
export struct SlidePage {
  // Slide slider
  @State sliderValue: SliderValue = new SliderValue();

  slideChange(value: number) {
    cameraDemo.setZoomRatio(value);
  }

  build() {
    if (this.sliderValue) {
      Column() {
        Text(this.sliderValue.outSetValueOne + 'x')
          .fontColor('#182431')
          .width('100px')
          .height('50px')
          .borderRadius(25)
          .backgroundColor(Color.White)
          .fontSize(14)
          .textAlign(TextAlign.Center)
          .zIndex(9)

        Row() {
          Text(this.sliderValue.min + 'x').fontColor(Color.White);
          Text(this.sliderValue.max + 'x').fontColor(Color.White);
        }.justifyContent(FlexAlign.SpaceBetween).width('95%')

        Row() {
          Slider({
            value: this.sliderValue.outSetValueOne,
            min: this.sliderValue.min,
            max: this.sliderValue.max,
            step: this.sliderValue.step,
            style: SliderStyle.OutSet
          }).showSteps(false)
            .trackColor('rgba(255,255,255,0.6)')
            .selectedColor($r('app.color.theme_color'))
            .onChange((value: number) => {
              let val = Number(value.toFixed(2));
              this.slideChange(val);
              this.sliderValue.outSetValueOne = val;
            })
        }.width('100%')
      }
      .height('60')
      .width('32.5%')
      .position({ x: '35%', y: '3%' })
    }
  }
}