/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import DateTimeUtil from '../model/DateTimeUtil';
import Logger from '../model/Logger';
import cameraDemo from 'libentry.so';
import photoAccessHelper from '@ohos.file.photoAccessHelper';
import image from '@ohos.multimedia.image';
import media from '@ohos.multimedia.media';
import MediaUtils from '../model/MediaUtils';
import deviceInfo from '@ohos.deviceInfo';
import fileio from '@ohos.fileio';
import { Constants, SettingDataObj } from '../common/Constants'
import common from '@ohos.app.ability.common'
import camera from '@ohos.multimedia.camera';

const NORMAL_PHOTO: number = 1;
const NORMAL_VIDEO: number = 2;
const SECURE_PHOTO: number = 12;
const cameraSize = {
  width: 1280,
  height: 720
};

class CameraService {
  private previewId: string | undefined = undefined;
  private focusMode: number | undefined = undefined;
  private cameraDeviceIndex: number | undefined = undefined;
  private sceneMode: number | undefined = undefined;
  private preconfigType: camera.PreconfigType = camera.PreconfigType.PRECONFIG_1080P;
  private preconfigRatio: camera.PreconfigRatio = camera.PreconfigRatio.PRECONFIG_RATIO_16_9;

  private mediaUtil = MediaUtils.getInstance();
  private tag: string = '0912 camera service:';
  private fd: number = -1;
  private videoId: string = '';
  private photoId: string = '';
  private mReceiver?: image.ImageReceiver;
  private videoRecorder?: media.AVRecorder;
  private fileAsset?: photoAccessHelper.PhotoAsset;
  private videoThumbnail: image.PixelMap | undefined = undefined;
  private imgThumbnailCallback: (imgThumbnail: string) => void | undefined = undefined;
  private videoThumbnailCallback: (videoThumbnail: image.PixelMap) => void | undefined = undefined;
  private photoRotationMap = {
    rotation0: 0,
    rotation90: 90,
    rotation180: 180,
    rotation270: 270,
  };

  async initCamera(previewId: string, focusMode: number, cameraDeviceIndex: number, sceneMode: number): Promise<void> {
    Logger.info(this.tag, 'initCamera start');
    this.previewId = previewId;
    this.focusMode = focusMode;
    this.cameraDeviceIndex = cameraDeviceIndex;
    this.sceneMode = sceneMode;

    Logger.info(this.tag, 'initCamera get photo surface id start');
    await this.getPhotoSurfaceID();
    Logger.info(this.tag, 'initCamera get photo surface id end');
    if (this.sceneMode == NORMAL_VIDEO) {
      Logger.info(this.tag, 'initCamera get video surface id start');
      await this.getVideoSurfaceID();
      Logger.info(this.tag, 'initCamera get video surface id start');
    }
    Logger.info(this.tag, 'initCamera cameraDemo.initCamera start');
    cameraDemo.initCamera(this.focusMode, this.cameraDeviceIndex, this.sceneMode,
      this.previewId, this.photoId, this.videoId);
    Logger.info(this.tag, 'initCamera cameraDemo.initCamera end');
    Logger.info(this.tag, 'initCamera end');
  }

  async getPhotoSurfaceID() {
    if (this.mReceiver) {
      Logger.info(this.tag, 'imageReceiver has been created');
    } else {
      this.createImageReceiver();
    }
    if (this.mReceiver) {
      this.photoId = await this.mReceiver.getReceivingSurfaceId();
    }
    if (this.photoId) {
      Logger.info(this.tag, `createImageReceiver photoId: ${this.photoId} `);
    } else {
      Logger.info(this.tag, `Get photoId failed `);
    }
  }

  createImageReceiver() {
    try {
      this.mReceiver = image.createImageReceiver(cameraSize.width, cameraSize.height, 2000, 8);
      Logger.info(this.tag, `createImageReceiver value: ${this.mReceiver} `);
      this.mReceiver.on('imageArrival', () => {
        Logger.info(this.tag, 'imageArrival start');
        if (this.mReceiver) {
          this.mReceiver.readNextImage((err, image) => {
            Logger.info(this.tag, 'readNextImage start');
            if (err || image === undefined) {
              Logger.error(this.tag, 'readNextImage failed ');
              return;
            }
            image.getComponent(4, (errMsg, img) => {
              Logger.info(this.tag, 'getComponent start');
              if (errMsg || img === undefined) {
                Logger.info(this.tag, 'getComponent failed ');
                return;
              }
              let buffer = new ArrayBuffer(2048);
              if (img.byteBuffer) {
                buffer = img.byteBuffer;
              } else {
                Logger.error(this.tag, 'img.byteBuffer is undefined');
              }
              this.savePicture(buffer, image);
            })
          })
        }
      })
    } catch {
      Logger.info(this.tag, 'savePicture err');
    }
  }

  async savePicture(buffer: ArrayBuffer, img: image.Image) {
    try {
      Logger.info(this.tag, 'savePicture start');
      let imgFileAsset: photoAccessHelper.PhotoAsset = await this.mediaUtil.createAndGetUri(Constants.IMAGE);
      let imgPhotoUri = imgFileAsset.uri;
      Logger.info(this.tag, `photoUri = ${imgPhotoUri}`);
      let imgFd = 0;
      imgFd = await this.mediaUtil.getFdPath(imgFileAsset);
      Logger.info(this.tag, `fd = ${imgFd}`);
      await fileio.write(imgFd, buffer);
      await imgFileAsset.close(imgFd);
      await img.release();
      Logger.info(this.tag, 'save image End');
      this.imgThumbnailCallback(imgPhotoUri);
    } catch (err) {
      Logger.info(this.tag, 'savePicture err' + JSON.stringify(err.message));
    }
  }

  async getVideoSurfaceID() {
    let videoConfig = this.getVideoConfig(this.preconfigType, this.preconfigRatio);

    Logger.info(this.tag, `getVideoSurfaceID`);
    this.videoRecorder = await media.createAVRecorder();
    Logger.info(this.tag, `getVideoSurfaceID videoRecorder: ${this.videoRecorder}`);

    this.fileAsset = await this.mediaUtil.createAndGetUri(Constants.VIDEO);
    Logger.info(this.tag, `getVideoSurfaceID fileAsset: ${this.fileAsset}`);
    Logger.info(this.tag, `getVideoSurfaceID uri: ${this.fileAsset?.uri}`);

    this.fd = await this.mediaUtil.getFdPath(this.fileAsset);
    Logger.info(this.tag, `getVideoSurfaceID fd: ${this.fd}`);

    // videoConfig.url = `fd://${this.fd}`;
    videoConfig.url = `fd://${this.fd.toString()}`;
    Logger.info(this.tag, `getVideoSurfaceID videoConfig.url : ${videoConfig.url}`);

    if (deviceInfo.deviceType == 'default') {
      Logger.info(this.tag, `deviceType = default`);
      videoConfig.videoSourceType = media.VideoSourceType.VIDEO_SOURCE_TYPE_SURFACE_ES;
    }
    if (deviceInfo.deviceType == 'phone') {
      Logger.info(this.tag, `deviceType = phone`)
      // videoConfig.videoSourceType = media.VideoSourceType.VIDEO_SOURCE_TYPE_SURFACE_YUV;
      // videoConfig.profile.videoCodec = media.CodecMimeType.VIDEO_MPEG4;
      if (this.cameraDeviceIndex == 1) {
        videoConfig.rotation = this.photoRotationMap.rotation270;
      } else {
        videoConfig.rotation = this.photoRotationMap.rotation90;
      }
    }
    if (deviceInfo.deviceType == 'tablet') {
      Logger.info(this.tag, `deviceType = tablet`);
    }

    await this.videoRecorder.prepare(videoConfig);
    this.videoId = await this.videoRecorder.getInputSurface();
    Logger.info(this.tag, `getVideoSurfaceID videoId: ${this.videoId}`);
  }

  async getVideoSurfaceID2() {
    Logger.info(this.tag, `getVideoSurfaceID2`);
    let videoConfig = this.getVideoConfig(this.preconfigType, this.preconfigRatio);

    this.fileAsset = await this.mediaUtil.createAndGetUri(Constants.VIDEO);
    Logger.info(this.tag, `getVideoSurfaceID fileAsset: ${this.fileAsset}`);

    this.fd = await this.mediaUtil.getFdPath(this.fileAsset);
    Logger.info(this.tag, `getVideoSurfaceID fd: ${this.fd}`);

    videoConfig.url = `fd://${this.fd}`;
    Logger.info(this.tag, `getVideoSurfaceID videoConfig.url : ${videoConfig.url}`);

    if (deviceInfo.deviceType == 'default') {
      Logger.info(this.tag, `deviceType = default`);
      videoConfig.videoSourceType = media.VideoSourceType.VIDEO_SOURCE_TYPE_SURFACE_ES;
    }
    if (deviceInfo.deviceType == 'phone') {
      Logger.info(this.tag, `deviceType = phone`)
      if (this.cameraDeviceIndex == 1) {
        videoConfig.rotation = this.photoRotationMap.rotation270;
      } else {
        videoConfig.rotation = this.photoRotationMap.rotation90;
      }
    }
    if (deviceInfo.deviceType == 'tablet') {
      Logger.info(this.tag, `deviceType = tablet`);
    }

    await this.videoRecorder.prepare(videoConfig);
    await this.videoRecorder.getInputSurface();
  }

  videoRecorderStart() {
    if (this.videoRecorder) {
      this.videoRecorder.start();
    }
  }

  async videoRecorderStop() {
    if (this.videoRecorder) {
      Logger.info(this.tag, `stopVideo videoRecorder.stop.`);
      await this.videoRecorder.stop();
    }
  }

  async videoRecorderRelease() {
    if (this.videoRecorder) {
      await this.videoRecorder.release();
    }
  }

  async fileAssetClose(): Promise<photoAccessHelper.PhotoAsset> {
    Logger.info(this.tag, `fileAssetClose start`);
    if (this.fileAsset) {
      let url = `fd://${this.fd}`;
      Logger.info(this.tag, `fileAssetClose fd.url : ${url}`);
      Logger.info(this.tag, `fileAssetClose uri: ${this.fileAsset?.uri}`);
      await this.fileAsset.close(this.fd);

      return this.fileAsset;
    }
  }

  registerImgThumbnailCallback(callback: (imgThumbnail: string) => void): void {
    this.imgThumbnailCallback = callback;
  }

  registerVideoThumbnailCallback(callback: (videoThumbnail: image.PixelMap) => void): void {
    this.videoThumbnailCallback = callback;
  }

  async callbackVideoThumbnail(): Promise<void> {
    Logger.info(this.tag, `callbackVideoThumbnail start`);
    try {
      this.videoThumbnail = await this.fileAsset.getThumbnail();
      if (this.videoThumbnail == undefined || this.videoThumbnail == null) {
        Logger.info(this.tag, `callbackVideoThumbnail settingDialog not exist!`);
      }
    } catch (err) {
      Logger.info(this.tag, 'callbackVideoThumbnail err----------:' + JSON.stringify(err.message));
    }
    Logger.info(this.tag, 'callbackVideoThumbnailthis.videoThumbnail :' + JSON.stringify(this.videoThumbnail));
    this.videoThumbnailCallback(this.videoThumbnail);
    Logger.info(this.tag, `callbackVideoThumbnail end`);
  }

  getVideoConfig(preconfigType: camera.PreconfigType, preconfigRatio: camera.PreconfigRatio): media.AVRecorderConfig {
    switch (preconfigType) {
      case camera.PreconfigType.PRECONFIG_720P:
        switch (preconfigRatio) {
          case camera.PreconfigRatio.PRECONFIG_RATIO_1_1:
            return this.getConfig(720, 720);
          case camera.PreconfigRatio.PRECONFIG_RATIO_4_3:
            return this.getConfig(960, 720);
          case camera.PreconfigRatio.PRECONFIG_RATIO_16_9:
            return this.getConfig(1280, 720);
          default:
            break;
        }
        break
      case camera.PreconfigType.PRECONFIG_1080P:
        switch (preconfigRatio) {
          case camera.PreconfigRatio.PRECONFIG_RATIO_1_1:
            return this.getConfig(1080, 1080);
          case camera.PreconfigRatio.PRECONFIG_RATIO_4_3:
            return this.getConfig(1440, 1080);
          case camera.PreconfigRatio.PRECONFIG_RATIO_16_9:
            return this.getConfig(1920, 1080);
          default:
            break;
        }
        break
      case camera.PreconfigType.PRECONFIG_4K:
        switch (preconfigRatio) {
          case camera.PreconfigRatio.PRECONFIG_RATIO_1_1:
            return this.getConfig(2160, 2160);
          case camera.PreconfigRatio.PRECONFIG_RATIO_4_3:
            return this.getConfig(2880, 2160);
          case camera.PreconfigRatio.PRECONFIG_RATIO_16_9:
            return this.getConfig(3840, 2160);
          default:
            break;
        }
        break
      case camera.PreconfigType.PRECONFIG_HIGH_QUALITY:
        switch (preconfigRatio) {
          case camera.PreconfigRatio.PRECONFIG_RATIO_1_1:
            return this.getConfig(2160, 2160, true);
          case camera.PreconfigRatio.PRECONFIG_RATIO_4_3:
            return this.getConfig(2880, 2160, true);
          case camera.PreconfigRatio.PRECONFIG_RATIO_16_9:
            return this.getConfig(3840, 2160, true);
          default:
            break;
        }
        break
      default:
        break;
    }
    return this.getConfig(1920, 1080);
  }

  getConfig(width: number, height: number, isHdr: boolean = false): media.AVRecorderConfig {
    let config = this.getDefaultConfig();
    config.profile.videoFrameWidth = width;
    config.profile.videoFrameHeight = height;
    if (isHdr) {
      config.profile.isHdr = true;
      config.profile.videoCodec = media.CodecMimeType.VIDEO_HEVC;
    }
    return config;
  }

  getDefaultConfig(): media.AVRecorderConfig {
    let config: media.AVRecorderConfig = {
      audioSourceType: media.AudioSourceType.AUDIO_SOURCE_TYPE_MIC,
      videoSourceType: media.VideoSourceType.VIDEO_SOURCE_TYPE_SURFACE_YUV,
      profile: {
        audioBitrate: 48000,
        audioChannels: 2,
        audioCodec: media.CodecMimeType.AUDIO_AAC,
        audioSampleRate: 48000,
        fileFormat: media.ContainerFormatType.CFT_MPEG_4,
        videoBitrate: 8000000,
        videoCodec: media.CodecMimeType.VIDEO_AVC,
        videoFrameWidth: 1920,
        videoFrameHeight: 1080,
        videoFrameRate: 30
      },
      url: '',
      rotation: 0
    };
    return config;
  }
}

export default new CameraService();