/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "camera_manager.h"
#include <cstdint>
#include <sstream>

#define LOG_TAG "DEMO:"
#define LOG_DOMAIN 0x3200
// preconfig mode
#define NO_PRECONFIG_MODE 1
#define TYPE_PRECONFIG_MODE 2
#define TYPE_RATIO_PRECONFIG_MODE 3
#define RESOURCE_TYPE_IMAGE MediaLibrary_ResourceType::MEDIA_LIBRARY_IMAGE_RESOURCE
#define RESOURCE_TYPE_VIDEO MediaLibrary_ResourceType::MEDIA_LIBRARY_VIDEO_RESOURCE
#define IMAGE_TYPE_JPEG MediaLibrary_ImageFileType::MEDIA_LIBRARY_IMAGE_JPEG

MediaLibrary_ErrorCode result = MEDIA_LIBRARY_OK;
OH_MediaAsset *g_mediaAsset = nullptr;
OH_MediaAssetManager *g_mediaAssetManager = nullptr;
OH_MediaAssetChangeRequest *g_changeRequest = nullptr;
OH_ImageSourceNative *g_imageSourceNative = nullptr;
OH_MovingPhoto *g_movingPhoto = nullptr;
MediaLibrary_MediaType g_mediaType;
MediaLibrary_MediaSubType g_mediaSubType;
MediaLibrary_RequestOptions g_requestOptions;
MediaLibrary_RequestId g_requestId;
MediaLibrary_DeliveryMode g_deliveryMode = MEDIA_LIBRARY_HIGH_QUALITY_MODE;
// MediaLibrary_DeliveryMode g_deliveryMode = MEDIA_LIBRARY_BALANCED_MODE;
const char *g_uri = nullptr;
const char *g_displayName = nullptr;
const char *g_title = nullptr;
const uint8_t *g_imageBuffer = nullptr;
const uint8_t *g_videoBuffer = nullptr;
char *g_mediaQualityCb = nullptr;
char *g_imageUri = nullptr;
char *g_videoUri = nullptr;
uint32_t g_imageSize = 0;
uint32_t g_videoSize = 0;
uint32_t g_mediaAssetSize;
uint32_t g_dateAdded;
uint32_t g_dateModified;
uint32_t g_dateTaken;
uint32_t g_dateAddedMs;
uint32_t g_dateModifiedMs;
uint32_t g_duration;
uint32_t g_width;
uint32_t g_height;
uint32_t g_orientation;
uint32_t g_favorite;
int32_t g_fd;
bool g_isMovingPhoto = false;

static void *requestImageCallback = nullptr;
static void *requestImageQualityCallback = nullptr;

namespace OHOS_CAMERA_SAMPLE {
NDKCamera *NDKCamera::ndkCamera_ = nullptr;
std::mutex NDKCamera::mtx_;
const std::unordered_map<uint32_t, Camera_SceneMode> g_int32ToCameraSceneMode = {
    {1, Camera_SceneMode::NORMAL_PHOTO},
    {2, Camera_SceneMode::NORMAL_VIDEO},
    {12, Camera_SceneMode::SECURE_PHOTO}
};
const std::unordered_map<uint32_t, Camera_PreconfigType> g_int32ToCameraPreconfigType = {
    {0, Camera_PreconfigType::PRECONFIG_720P},
    {1, Camera_PreconfigType::PRECONFIG_1080P},
    {2, Camera_PreconfigType::PRECONFIG_4K},
    {3, Camera_PreconfigType::PRECONFIG_HIGH_QUALITY}
};
const std::unordered_map<uint32_t, Camera_PreconfigRatio> g_int32ToCameraPreconfigRatio = {
    {0, Camera_PreconfigRatio::PRECONFIG_RATIO_1_1},
    {1, Camera_PreconfigRatio::PRECONFIG_RATIO_4_3},
    {2, Camera_PreconfigRatio::PRECONFIG_RATIO_16_9}
};

NDKCamera::NDKCamera(char *str, uint32_t focusMode, uint32_t cameraDeviceIndex,
    uint32_t sceneMode, uint32_t preconfigMode, uint32_t preconfigType, uint32_t preconfigRatio,
    uint32_t photoOutputType, bool isMovingPhoto, bool isSavingPhoto)
    : previewSurfaceId_(str), cameras_(nullptr), focusMode_(focusMode), cameraDeviceIndex_(cameraDeviceIndex),
      cameraOutputCapability_(nullptr), cameraInput_(nullptr), captureSession_(nullptr), size_(0),
      isCameraMuted_(nullptr), profile_(nullptr), photoSurfaceId_(nullptr), previewOutput_(nullptr),
      photoOutput_(nullptr), metaDataObjectType_(nullptr), metadataOutput_(nullptr),
      isExposureModeSupported_(false), isFocusModeSupported_(false), exposureMode_(EXPOSURE_MODE_LOCKED),
      minExposureBias_(0), maxExposureBias_(0), step_(0), ret_(CAMERA_OK),
      sceneMode_(NORMAL_PHOTO), sceneModes_(nullptr), preconfigType_(PRECONFIG_720P),
      preconfigRatio_(PRECONFIG_RATIO_1_1), activePreviewProfile_(nullptr), activePhotoProfile_(nullptr),
      activeVideoProfile_(nullptr), sceneModeSize_(0), preconfigMode_(preconfigMode), isSuccess_(false),
      preconfigged_(false), photoOutputType_(photoOutputType), isMovingPhoto_(isMovingPhoto) {
    valid_ = false;
    g_isMovingPhoto = isMovingPhoto;
    ReleaseCamera();
    Camera_ErrorCode ret = OH_Camera_GetCameraManager(&cameraManager_);
    if (cameraManager_ == nullptr || ret != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "Get CameraManager failed.");
    }

    ret = OH_CameraManager_CreateCaptureSession(cameraManager_, &captureSession_);
    if (captureSession_ == nullptr || ret != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "Create captureSession failed.");
    }

    auto itr1 = g_int32ToCameraSceneMode.find(sceneMode);
    if (itr1 != g_int32ToCameraSceneMode.end()) {
        sceneMode_ = itr1->second;
    }
    auto itr2 = g_int32ToCameraPreconfigType.find(preconfigType);
    if (itr2 != g_int32ToCameraPreconfigType.end()) {
        preconfigType_ = itr2->second;
    }
    auto itr3 = g_int32ToCameraPreconfigRatio.find(preconfigRatio);
    if (itr3 != g_int32ToCameraPreconfigRatio.end()) {
        preconfigRatio_ = itr3->second;
    }
    // expect CAMERA_OK
    ret = OH_CaptureSession_SetSessionMode(captureSession_, sceneMode_);
    DRAWING_LOGD(" [RM003 log] OH_CaptureSession_SetSessionMode sceneMode_: %{public}d!", sceneMode_);
    DRAWING_LOGD(" [RM003 log] OH_CaptureSession_SetSessionMode return with ret code: %{public}d!", ret);

    CaptureSessionRegisterCallback();
    GetSupportedCameras();
    GetSupportedOutputCapability();
    CreatePreviewOutput();
    CreateCameraInput();
    CameraInputOpen();
    CameraManagerRegisterCallback();
    SessionFlowFn();
    valid_ = true;
}

NDKCamera::~NDKCamera()
{
    valid_ = false;
    OH_LOG_ERROR(LOG_APP, "~NDKCamera");
    Camera_ErrorCode ret = CAMERA_OK;

    if (cameraManager_) {
        OH_LOG_ERROR(LOG_APP, "Release OH_CameraManager_DeleteSupportedCameraOutputCapability. enter");
        ret = OH_CameraManager_DeleteSupportedCameraOutputCapability(cameraManager_, cameraOutputCapability_);
        if (ret != CAMERA_OK) {
            OH_LOG_ERROR(LOG_APP, "Delete CameraOutputCapability failed.");
        } else {
            OH_LOG_ERROR(LOG_APP, "Release OH_CameraManager_DeleteSupportedCameraOutputCapability. ok");
        }

        OH_LOG_ERROR(LOG_APP, "Release OH_CameraManager_DeleteSupportedCameras. enter");
        ret = OH_CameraManager_DeleteSupportedCameras(cameraManager_, cameras_, size_);
        if (ret != CAMERA_OK) {
            OH_LOG_ERROR(LOG_APP, "Delete Cameras failed.");
        } else {
            OH_LOG_ERROR(LOG_APP, "Release OH_CameraManager_DeleteSupportedCameras. ok");
        }

        ret = OH_Camera_DeleteCameraManager(cameraManager_);
        if (ret != CAMERA_OK) {
            OH_LOG_ERROR(LOG_APP, "Delete CameraManager failed.");
        } else {
            OH_LOG_ERROR(LOG_APP, "Release OH_Camera_DeleteCameraMananger. ok");
        }
        cameraManager_ = nullptr;
    }

    OH_LOG_ERROR(LOG_APP, "~NDKCamera exit");
}

Camera_ErrorCode NDKCamera::RequestImageCallback(void *cb) {
    if (cb == nullptr) {
        OH_LOG_ERROR(LOG_APP, " RequestImageCallback invalid error");
        return CAMERA_INVALID_ARGUMENT;
    }
    requestImageCallback = cb;
    
    return CAMERA_OK;
}

Camera_ErrorCode NDKCamera::RequestImageQualityCallback(void *cb) {
    if (cb == nullptr) {
        OH_LOG_ERROR(LOG_APP, " RequestImageCallback invalid error");
        return CAMERA_INVALID_ARGUMENT;
    }
    requestImageQualityCallback = cb;
    
    return CAMERA_OK;
}

Camera_ErrorCode NDKCamera::ReleaseCamera(void)
{
    OH_LOG_ERROR(LOG_APP, " enter ReleaseCamera");
    if (previewOutput_) {
        PreviewOutputStop();
        OH_CaptureSession_RemovePreviewOutput(captureSession_, previewOutput_);
        PreviewOutputRelease();
    }
    if (photoOutput_) {
        PhotoOutputRelease();
    }
    if (captureSession_) {
        SessionRealese();
    }
    if (cameraInput_) {
        CameraInputClose();
    }
    if (g_mediaAsset) {
        MediaAssetRelease();
    }
    if (g_changeRequest) {
        ChangeRequestRelease();
    }
    if (g_mediaAssetManager) {
        MediaAssetManagerRelease();
    }
    if (g_movingPhoto) {
        MovingPhotoRelease();
    }
    OH_LOG_ERROR(LOG_APP, " exit ReleaseCamera");
    return ret_;
}

Camera_ErrorCode NDKCamera::ReleaseSession(void)
{
    OH_LOG_ERROR(LOG_APP, " enter ReleaseSession");
    PreviewOutputStop();
    PhotoOutputRelease();
    SessionRealese();
    OH_LOG_ERROR(LOG_APP, " exit ReleaseSession");
    return ret_;
}

Camera_ErrorCode NDKCamera::SessionRealese(void)
{
    OH_LOG_ERROR(LOG_APP, " enter SessionRealese");
    Camera_ErrorCode ret = OH_CaptureSession_Release(captureSession_);
    captureSession_ = nullptr;
    OH_LOG_ERROR(LOG_APP, " exit SessionRealese");
    return ret;
}

Camera_ErrorCode NDKCamera::HasFlashFn(uint32_t mode)
{
    Camera_FlashMode flashMode = static_cast<Camera_FlashMode>(mode);
    // Check for flashing lights
    bool hasFlash = false;
    Camera_ErrorCode ret = OH_CaptureSession_HasFlash(captureSession_, &hasFlash);
    if (captureSession_ == nullptr || ret != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_HasFlash failed.");
    }
    if (hasFlash) {
        OH_LOG_INFO(LOG_APP, "hasFlash success-----");
    } else {
        OH_LOG_ERROR(LOG_APP, "hasFlash fail-----");
    }

    // Check if the flash mode is supported
    bool isSupported = false;
    ret = OH_CaptureSession_IsFlashModeSupported(captureSession_, flashMode, &isSupported);
    if (ret != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_IsFlashModeSupported failed.");
    }
    if (isSupported) {
        OH_LOG_INFO(LOG_APP, "isFlashModeSupported success-----");
    } else {
        OH_LOG_ERROR(LOG_APP, "isFlashModeSupported fail-----");
    }

    // Set flash mode
    ret = OH_CaptureSession_SetFlashMode(captureSession_, flashMode);
    if (ret == CAMERA_OK) {
        OH_LOG_INFO(LOG_APP, "OH_CaptureSession_SetFlashMode success.");
    } else {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_SetFlashMode failed. %{public}d ", ret);
    }

    // Obtain the flash mode of the current device
    ret = OH_CaptureSession_GetFlashMode(captureSession_, &flashMode);
    if (ret == CAMERA_OK) {
        OH_LOG_INFO(LOG_APP, "OH_CaptureSession_GetFlashMode success. flashMode：%{public}d ", flashMode);
    } else {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_GetFlashMode failed. %d ", ret);
    }
    return ret;
}

Camera_ErrorCode NDKCamera::IsVideoStabilizationModeSupportedFn(uint32_t mode)
{
    Camera_VideoStabilizationMode videoMode = static_cast<Camera_VideoStabilizationMode>(mode);
    // Check if the specified video anti shake mode is supported
    bool isSupported = false;
    Camera_ErrorCode ret =
        OH_CaptureSession_IsVideoStabilizationModeSupported(captureSession_, videoMode, &isSupported);
    if (ret != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_IsVideoStabilizationModeSupported failed.");
    }
    if (isSupported) {
        OH_LOG_INFO(LOG_APP, "OH_CaptureSession_IsVideoStabilizationModeSupported success-----");
    } else {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_IsVideoStabilizationModeSupported fail-----");
    }

    // Set video stabilization
    ret = OH_CaptureSession_SetVideoStabilizationMode(captureSession_, videoMode);
    if (ret == CAMERA_OK) {
        OH_LOG_INFO(LOG_APP, "OH_CaptureSession_SetVideoStabilizationMode success.");
    } else {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_SetVideoStabilizationMode failed. %{public}d ", ret);
    }

    ret = OH_CaptureSession_GetVideoStabilizationMode(captureSession_, &videoMode);
    if (ret == CAMERA_OK) {
        OH_LOG_INFO(LOG_APP, "OH_CaptureSession_GetVideoStabilizationMode success. videoMode：%{public}f ",
                    videoMode);
    } else {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_GetVideoStabilizationMode failed. %{public}d ", ret);
    }
    return ret;
}

Camera_ErrorCode NDKCamera::setZoomRatioFn(uint32_t zoomRatio)
{
    float zoom = float(zoomRatio);
    // Obtain supported zoom range
    float minZoom;
    float maxZoom;
    Camera_ErrorCode ret = OH_CaptureSession_GetZoomRatioRange(captureSession_, &minZoom, &maxZoom);
    if (captureSession_ == nullptr || ret != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_GetZoomRatioRange failed.");
    } else {
        OH_LOG_INFO(LOG_APP, "OH_CaptureSession_GetZoomRatioRange success. minZoom: %{public}f, maxZoom:%{public}f",
                    minZoom, maxZoom);
    }

    // Set Zoom
    ret = OH_CaptureSession_SetZoomRatio(captureSession_, zoom);
    if (ret == CAMERA_OK) {
        OH_LOG_INFO(LOG_APP, "OH_CaptureSession_SetZoomRatio success.");
    } else {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_SetZoomRatio failed. %{public}d ", ret);
    }

    // Obtain the zoom value of the current device
    ret = OH_CaptureSession_GetZoomRatio(captureSession_, &zoom);
    if (ret == CAMERA_OK) {
        OH_LOG_INFO(LOG_APP, "OH_CaptureSession_GetZoomRatio success. zoom：%{public}f ", zoom);
    } else {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_GetZoomRatio failed. %{public}d ", ret);
    }
    return ret;
}

Camera_ErrorCode NDKCamera::SetRequestOption(uint32_t requestOption)
{
    if(requestOption == 0) {
        g_deliveryMode = MEDIA_LIBRARY_FAST_MODE;
    } else if (requestOption == 1) {
        g_deliveryMode = MEDIA_LIBRARY_HIGH_QUALITY_MODE;
    } else if (requestOption == 2) {
        g_deliveryMode = MEDIA_LIBRARY_BALANCED_MODE;
    } else {
        return CAMERA_INVALID_ARGUMENT;
    }
    return CAMERA_OK;
}

Camera_ErrorCode NDKCamera::SessionBegin(void)
{
    Camera_ErrorCode ret = OH_CaptureSession_BeginConfig(captureSession_);
    if (ret == CAMERA_OK) {
        OH_LOG_INFO(LOG_APP, "OH_CaptureSession_BeginConfig success.");
    } else {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_BeginConfig failed. %d ", ret);
    }
    return ret;
}

Camera_ErrorCode NDKCamera::SessionCommitConfig(void)
{
    Camera_ErrorCode ret = OH_CaptureSession_CommitConfig(captureSession_);
    if (ret == CAMERA_OK) {
        OH_LOG_INFO(LOG_APP, "OH_CaptureSession_CommitConfig success.");
    } else {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_CommitConfig failed. %d ", ret);
    }
    return ret;
}

Camera_ErrorCode NDKCamera::SessionStart(void)
{
    Camera_ErrorCode ret = OH_CaptureSession_Start(captureSession_);
    if (ret == CAMERA_OK) {
        OH_LOG_INFO(LOG_APP, "OH_CaptureSession_Start success.");
    } else {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_Start failed. %d ", ret);
    }
    return ret;
}

Camera_ErrorCode NDKCamera::SessionStop(void)
{
    Camera_ErrorCode ret = OH_CaptureSession_Stop(captureSession_);
    if (ret == CAMERA_OK) {
        OH_LOG_INFO(LOG_APP, "OH_CaptureSession_Stop success.");
    } else {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_Stop failed. %d ", ret);
    }
    return ret;
}

Camera_ErrorCode NDKCamera::SessionFlowFn(void)
{
    OH_LOG_INFO(LOG_APP, "Start SessionFlowFn IN.");
    // Start configuring session
    OH_LOG_INFO(LOG_APP, "session beginConfig.");
    Camera_ErrorCode ret = OH_CaptureSession_BeginConfig(captureSession_);

    isSuccess_ = false;
    CanAddInput(&isSuccess_);
    if (!isSuccess_) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::SessionFlowFn can not add input!");
        return Camera_ErrorCode::CAMERA_OPERATION_NOT_ALLOWED;
    }

    // Add CameraInput to the session
    OH_LOG_INFO(LOG_APP, "session addInput.");
    ret = OH_CaptureSession_AddInput(captureSession_, cameraInput_);

    isSuccess_ = false;
    CanAddPreviewOutput(&isSuccess_);
    if (!isSuccess_) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::SessionFlowFn can not add preview output!");
        return Camera_ErrorCode::CAMERA_OPERATION_NOT_ALLOWED;
    }

    // Add previewOutput to the session
    OH_LOG_INFO(LOG_APP, "session add Preview Output.");
    ret = OH_CaptureSession_AddPreviewOutput(captureSession_, previewOutput_);
    
    DRAWING_LOGD(" [RM003 log] NDKCamera::SessionFlowFn hit at point 1 !");
    CreatePhotoOutputWithoutSurfaceId();
    DRAWING_LOGD(" [RM003 log] NDKCamera::SessionFlowFn hit at point 2 !");
    isSuccess_ = false;
    CanAddPhotoOutput(&isSuccess_);
    if (!isSuccess_) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::SessionFlowFn hit at point 3 !");
//         return Camera_ErrorCode::CAMERA_OPERATION_NOT_ALLOWED;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::SessionFlowFn hit at point 4 !");
    ret = OH_CaptureSession_AddPhotoOutput(captureSession_, photoOutput_);
    DRAWING_LOGD(" [RM003 log] NDKCamera::SessionFlowFn hit at point 5 !");
    
    DRAWING_LOGD(" [RM003 log] NDKCamera::CreatePhotoOutput register photo asset available callback!");
    PhotoOutputRegisterPhotoAssetAvailableCallback();
    // Submit configuration information
    OH_LOG_INFO(LOG_APP, "session commitConfig");
    ret = OH_CaptureSession_CommitConfig(captureSession_);
    
    isSuccess_ = false;
    OH_PhotoOutput_IsMovingPhotoSupported(photoOutput_, &isSuccess_);
    if (!isSuccess_) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::StartVideo OH_PhotoOutput_IsMovingPhotoSupported return false!");
    } else {
        ret = OH_PhotoOutput_EnableMovingPhoto(photoOutput_, isMovingPhoto_);
        DRAWING_LOGD(" [RM003 log] NDKCamera::StartVideo OH_PhotoOutput_EnableMovingPhoto return with ret code: %{public}d isMovingPhoto_ = %{public}d!", ret, isMovingPhoto_);
    }

    // Start Session Work
    OH_LOG_INFO(LOG_APP, "session start");
    ret = OH_CaptureSession_Start(captureSession_);
    OH_LOG_INFO(LOG_APP, "session success");

    // Start focusing
    OH_LOG_INFO(LOG_APP, "IsFocusMode start");
    ret = IsFocusMode(focusMode_);
    OH_LOG_INFO(LOG_APP, "IsFocusMode success");
    return ret;
}

Camera_ErrorCode NDKCamera::CreateCameraInput(void)
{
    OH_LOG_ERROR(LOG_APP, "enter CreateCameraInput.");
    ret_ = OH_CameraManager_CreateCameraInput(cameraManager_, &cameras_[cameraDeviceIndex_], &cameraInput_);
    if (cameraInput_ == nullptr || ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "CreateCameraInput failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    OH_LOG_ERROR(LOG_APP, "exit CreateCameraInput.");
    CameraInputRegisterCallback();
    return ret_;
}

Camera_ErrorCode NDKCamera::CameraInputOpen(void)
{
    OH_LOG_ERROR(LOG_APP, "enter CameraInputOpen.");
    ret_ = OH_CameraInput_Open(cameraInput_);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "CameraInput_Open failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    OH_LOG_ERROR(LOG_APP, "exit CameraInputOpen.");
    return ret_;
}

Camera_ErrorCode NDKCamera::CameraInputClose(void)
{
    OH_LOG_ERROR(LOG_APP, "enter CameraInput_Close.");
    ret_ = OH_CameraInput_Close(cameraInput_);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "CameraInput_Close failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    OH_LOG_ERROR(LOG_APP, "exit CameraInput_Close.");
    return ret_;
}

Camera_ErrorCode NDKCamera::CameraInputRelease(void)
{
    OH_LOG_ERROR(LOG_APP, "enter CameraInputRelease.");
    ret_ = OH_CameraInput_Release(cameraInput_);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "CameraInput_Release failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    OH_LOG_ERROR(LOG_APP, "exit CameraInputRelease.");
    return ret_;
}

Camera_ErrorCode NDKCamera::GetSupportedCameras(void)
{
    ret_ = OH_CameraManager_GetSupportedCameras(cameraManager_, &cameras_, &size_);
    if (cameras_ == nullptr || &size_ == nullptr || ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "Get supported cameras failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    return ret_;
}

Camera_ErrorCode NDKCamera::GetSupportedOutputCapability(void)
{
    ret_ = OH_CameraManager_GetSupportedCameraOutputCapability(cameraManager_, &cameras_[cameraDeviceIndex_],
                                                               &cameraOutputCapability_);
    if (cameraOutputCapability_ == nullptr || ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "GetSupportedCameraOutputCapability failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    return ret_;
}

Camera_ErrorCode NDKCamera::CreatePreviewOutput(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::CreatePreviewOutput start!");
    isSuccess_ = false;
    if (preconfigMode_ == TYPE_RATIO_PRECONFIG_MODE) {
        ret_ = CanPreconfigWithRatio(&isSuccess_);
        if (ret_ != CAMERA_OK) {
            DRAWING_LOGD(" [RM003 log] NDKCamera::CreatePreviewOutput CanPreconfigWithRatio failed!");
            return CAMERA_INVALID_ARGUMENT;
        }
    } else if (preconfigMode_ == TYPE_PRECONFIG_MODE) {
        ret_ = CanPreconfig(&isSuccess_);
        if (ret_ != CAMERA_OK) {
            DRAWING_LOGD(" [RM003 log] NDKCamera::CreatePreviewOutput CanPreconfig failed!");
            return CAMERA_INVALID_ARGUMENT;
        }
    }

    if (preconfigMode_ == NO_PRECONFIG_MODE || !isSuccess_) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::CreatePreviewOutput into normal branch!");
        profile_ = cameraOutputCapability_->previewProfiles[0];
        if (profile_ == nullptr) {
            OH_LOG_ERROR(LOG_APP, "Get previewProfiles failed.");
            return CAMERA_INVALID_ARGUMENT;
        }
        cameraProfile_ = cameraOutputCapability_->previewProfiles[0];
        cameraProfile_->format = CAMERA_FORMAT_YUV_420_SP;
        cameraProfile_->size.width = 1920;
        cameraProfile_->size.height = 1080;
        ret_ = OH_CameraManager_CreatePreviewOutput(cameraManager_, cameraProfile_, previewSurfaceId_, &previewOutput_);
        if (previewSurfaceId_ == nullptr || previewOutput_ == nullptr || ret_ != CAMERA_OK) {
            OH_LOG_ERROR(LOG_APP, "CreatePreviewOutput failed.");
            return CAMERA_INVALID_ARGUMENT;
        }
        return ret_;
        PreviewOutputRegisterCallback();
    } else {
        DRAWING_LOGD(" [RM003 log] NDKCamera::CreatePreviewOutput into preconfig branch!");
        if (preconfigMode_ == TYPE_RATIO_PRECONFIG_MODE) {
            ret_ = PreconfigWithRatio();
            if (ret_ != CAMERA_OK) {
                DRAWING_LOGD(" [RM003 log] NDKCamera::CreatePreviewOutput PreconfigWithRatio failed!");
                return CAMERA_INVALID_ARGUMENT;
            }
        } else {
            ret_ = Preconfig();
            if (ret_ != CAMERA_OK) {
                DRAWING_LOGD(" [RM003 log] NDKCamera::CreatePreviewOutput Preconfig failed!");
                return CAMERA_INVALID_ARGUMENT;
            }
        }
        preconfigged_ = true;
        ret_ = OH_CameraManager_CreatePreviewOutputUsedInPreconfig(cameraManager_, previewSurfaceId_, &previewOutput_);
        if (previewOutput_ == nullptr || ret_ != CAMERA_OK) {
            DRAWING_LOGD(" [RM003 log] OH_CameraManager_CreatePreviewOutputUsedInPreconfig failed!");
            OH_LOG_ERROR(LOG_APP, "CreatePreviewOutput failed.");
            return CAMERA_INVALID_ARGUMENT;
        }
        DRAWING_LOGD(" [RM003 log] OH_CameraManager_CreatePreviewOutputUsedInPreconfig return with ret code: %{public}d!", ret_);
        return ret_;
        PreviewOutputRegisterCallback();
    }
}

Camera_ErrorCode NDKCamera::CreatePhotoOutput(char *photoSurfaceId)
{
    if (preconfigged_) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::CreatePhotoOutput into preconfigged branch!");
        ret_ = OH_CameraManager_CreatePhotoOutputUsedInPreconfig(cameraManager_, photoSurfaceId, &photoOutput_);
        if (photoOutput_ == nullptr || ret_ != CAMERA_OK) {
            DRAWING_LOGD(" [RM003 log] OH_CameraManager_CreatePhotoOutputUsedInPreconfig failed!");
            OH_LOG_ERROR(LOG_APP, "CreatePhotoOutput failed.");
            return CAMERA_INVALID_ARGUMENT;
        }
        DRAWING_LOGD(" [RM003 log] OH_CameraManager_CreatePhotoOutputUsedInPreconfig return with ret code: %{public}d!", ret_);
        PhotoOutputRegisterCallback();
        return ret_;
    } else {
        DRAWING_LOGD(" [RM003 log] NDKCamera::CreatePhotoOutput into normal branch!");
        profile_ = cameraOutputCapability_->photoProfiles[0];
        if (profile_ == nullptr) {
            OH_LOG_ERROR(LOG_APP, "Get photoProfiles failed.");
            return CAMERA_INVALID_ARGUMENT;
        }

        if (photoSurfaceId == nullptr) {
            OH_LOG_ERROR(LOG_APP, "CreatePhotoOutput failed.");
            return CAMERA_INVALID_ARGUMENT;
        }

        ret_ = OH_CameraManager_CreatePhotoOutput(cameraManager_, profile_, photoSurfaceId, &photoOutput_);
        PhotoOutputRegisterCallback();
        return ret_;
    }
}

Camera_ErrorCode NDKCamera::CreatePhotoOutputWithoutSurfaceId()
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::CreatePhotoOutput into createWithoutSurfaceID branch!");
    cameraProfile_ = cameraOutputCapability_->photoProfiles[0];
    cameraProfile_->size.width = 1920;
    cameraProfile_->size.height = 1080;
//     cameraProfile_->size.width = 3840;
//     cameraProfile_->size.height = 2160;
    cameraProfile_->format = CAMERA_FORMAT_JPEG;
    ret_ = OH_CameraManager_CreatePhotoOutputWithoutSurface(cameraManager_, cameraProfile_, &photoOutput_);
    if (photoOutput_ == nullptr || ret_ != CAMERA_OK) {
        DRAWING_LOGD(" [RM003 log] OH_CameraManager_CreatePhotoOutputWithoutSurface failed!");
        OH_LOG_ERROR(LOG_APP, "CreatePhotoOutput failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::CreatePhotoOutput register photo asset available callback!");
    MediaAssetManagerCreate();
    PhotoOutputRegisterPhotoAssetAvailableCallback();
    return ret_;
}

Camera_ErrorCode NDKCamera::CreateVideoOutput(char *videoId)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::CreateVideoOutput start!");
    if (preconfigged_) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::CreateVideoOutput into preconfigged_ branch!");
        ret_ = OH_CameraManager_CreateVideoOutputUsedInPreconfig(cameraManager_, videoId, &videoOutput_);
        if (videoOutput_ == nullptr || ret_ != CAMERA_OK) {
            DRAWING_LOGD(" [RM003 log] OH_CameraManager_CreateVideoOutputUsedInPreconfig failed!");
            OH_LOG_ERROR(LOG_APP, "CreateVideoOutput failed.");
            return CAMERA_INVALID_ARGUMENT;
        }
        DRAWING_LOGD(" [RM003 log] OH_CameraManager_CreateVideoOutputUsedInPreconfig return with ret code: %{public}d!", ret_);
        return ret_;
    } else {
        DRAWING_LOGD(" [RM003 log] NDKCamera::CreateVideoOutput into normal branch!");
        videoProfile_ = cameraOutputCapability_->videoProfiles[0];

        if (videoProfile_ == nullptr) {
            OH_LOG_ERROR(LOG_APP, "Get videoProfiles failed.");
            return CAMERA_INVALID_ARGUMENT;
        }
        ret_ = OH_CameraManager_CreateVideoOutput(cameraManager_, videoProfile_, videoId, &videoOutput_);
        if (videoId == nullptr || videoOutput_ == nullptr || ret_ != CAMERA_OK) {
            OH_LOG_ERROR(LOG_APP, "CreateVideoOutput failed.");
            return CAMERA_INVALID_ARGUMENT;
        }
    
        return ret_;
    }
}

Camera_ErrorCode NDKCamera::AddVideoOutput(void)
{
    Camera_ErrorCode ret = OH_CaptureSession_AddVideoOutput(captureSession_, videoOutput_);
    if (ret == CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_AddVideoOutput success.");
    } else {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_AddVideoOutput failed. %d ", ret);
    }
    return ret;
}

Camera_ErrorCode NDKCamera::AddPhotoOutput()
{
    Camera_ErrorCode ret = OH_CaptureSession_AddPhotoOutput(captureSession_, photoOutput_);
    if (ret == CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_AddPhotoOutput success.");
    } else {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_AddPhotoOutput failed. %d ", ret);
    }
    return ret;
}

Camera_ErrorCode NDKCamera::CreateMetadataOutput(void)
{
    metaDataObjectType_ = cameraOutputCapability_->supportedMetadataObjectTypes[0];
    if (metaDataObjectType_ == nullptr) {
        OH_LOG_ERROR(LOG_APP, "Get metaDataObjectType failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    ret_ = OH_CameraManager_CreateMetadataOutput(cameraManager_, metaDataObjectType_, &metadataOutput_);
    if (metadataOutput_ == nullptr || ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "CreateMetadataOutput failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    MetadataOutputRegisterCallback();
    return ret_;
}

Camera_ErrorCode NDKCamera::IsCameraMuted(void)
{
    ret_ = OH_CameraManager_IsCameraMuted(cameraManager_, isCameraMuted_);
    if (isCameraMuted_ == nullptr || ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "IsCameraMuted failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    return ret_;
}

Camera_ErrorCode NDKCamera::PreviewOutputStop(void)
{
    OH_LOG_ERROR(LOG_APP, "enter PreviewOutputStop.");
    ret_ = OH_PreviewOutput_Stop(previewOutput_);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "PreviewOutputStop failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    return ret_;
}

Camera_ErrorCode NDKCamera::PreviewOutputRelease(void)
{
    OH_LOG_ERROR(LOG_APP, "enter PreviewOutputRelease.");
    ret_ = OH_PreviewOutput_Release(previewOutput_);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "PreviewOutputRelease failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    return ret_;
}

Camera_ErrorCode NDKCamera::PhotoOutputRelease(void)
{
    OH_LOG_ERROR(LOG_APP, "enter PhotoOutputRelease.");
    //ret_ = OH_PhotoOutput_Release(photoOutput_);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "PhotoOutputRelease failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    return ret_;
}

Camera_ErrorCode NDKCamera::StartVideo(char *videoId, char *photoId)
{
    OH_LOG_INFO(LOG_APP, "StartVideo begin.");
    Camera_ErrorCode ret = SessionStop();
    if (ret == CAMERA_OK) {
        OH_LOG_INFO(LOG_APP, "SessionStop success.");
    } else {
        OH_LOG_ERROR(LOG_APP, "SessionStop failed. %d ", ret);
    }
    ret = SessionBegin();
    if (ret == CAMERA_OK) {
        OH_LOG_INFO(LOG_APP, "SessionBegin success.");
    } else {
        OH_LOG_ERROR(LOG_APP, "SessionBegin failed. %d ", ret);
    }
    OH_CaptureSession_RemovePhotoOutput(captureSession_, photoOutput_);
    CreatePhotoOutput(photoId);
    // expect CAMERA_SERVICE_FATAL_ERROR
    ret = OH_CaptureSession_SetSessionMode(captureSession_, Camera_SceneMode::NORMAL_PHOTO);
    DRAWING_LOGD(" [RM003 log] OH_CaptureSession_SetSessionMode NORMAL_PHOTO return with ret code: %{public}d!", ret);
    isSuccess_ = false;
    CanAddPhotoOutput(&isSuccess_);
    if (!isSuccess_) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::StartVideo can not add photo output!");
        return Camera_ErrorCode::CAMERA_OPERATION_NOT_ALLOWED;
    }
    AddPhotoOutput();
    CreateVideoOutput(videoId);
    // expect CAMERA_SERVICE_FATAL_ERROR
    ret = OH_CaptureSession_SetSessionMode(captureSession_, Camera_SceneMode::NORMAL_VIDEO);
    DRAWING_LOGD(" [RM003 log] OH_CaptureSession_SetSessionMode NORMAL_VIDEO return with ret code: %{public}d!", ret);
    isSuccess_ = false;
    CanAddVideoOutput(&isSuccess_);
    if (!isSuccess_) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::StartVideo can not add video output!");
        return Camera_ErrorCode::CAMERA_OPERATION_NOT_ALLOWED;
    }
    AddVideoOutput();
    SessionCommitConfig();
    SessionStart();
    VideoOutputRegisterCallback();
    return ret;
}

Camera_ErrorCode NDKCamera::VideoOutputStart(void)
{
    OH_LOG_INFO(LOG_APP, "VideoOutputStart begin.");
    Camera_ErrorCode ret = OH_VideoOutput_Start(videoOutput_);
    if (ret == CAMERA_OK) {
        OH_LOG_INFO(LOG_APP, "OH_VideoOutput_Start success.");
    } else {
        OH_LOG_ERROR(LOG_APP, "OH_VideoOutput_Start failed. %d ", ret);
    }
    return ret;
}

Camera_ErrorCode NDKCamera::StartPhoto(char *mSurfaceId)
{
    Camera_ErrorCode ret = CAMERA_OK;
    if (takePictureTimes == 0) {
        ret = SessionStop();
        if (ret == CAMERA_OK) {
            OH_LOG_INFO(LOG_APP, "SessionStop success.");
        } else {
            OH_LOG_ERROR(LOG_APP, "SessionStop failed. %d ", ret);
        }
        ret = SessionBegin();
        if (ret == CAMERA_OK) {
            OH_LOG_INFO(LOG_APP, "SessionBegin success.");
        } else {
            OH_LOG_ERROR(LOG_APP, "SessionBegin failed. %d ", ret);
        }
        OH_LOG_INFO(LOG_APP, "startPhoto begin.");
        ret = CreatePhotoOutput(mSurfaceId);
        OH_LOG_INFO(LOG_APP, "startPhoto CreatePhotoOutput ret = %{public}d.", ret);
        // expect CAMERA_SERVICE_FATAL_ERROR
        ret = OH_CaptureSession_SetSessionMode(captureSession_, Camera_SceneMode::NORMAL_PHOTO);
        DRAWING_LOGD(" [RM003 log] OH_CaptureSession_SetSessionMode NORMAL_PHOTO return with ret code: %{public}d!", ret);

        isSuccess_ = false;
        OH_PhotoOutput_IsMovingPhotoSupported(photoOutput_, &isSuccess_);
        if (!isSuccess_) {
            DRAWING_LOGD(" [RM003 log] NDKCamera::StartVideo OH_PhotoOutput_IsMovingPhotoSupported return false!");
        } else {
            ret = OH_PhotoOutput_EnableMovingPhoto(photoOutput_, isMovingPhoto_);
            DRAWING_LOGD(" [RM003 log] NDKCamera::StartVideo OH_PhotoOutput_EnableMovingPhoto return with ret code: %{public}d isMovingPhoto_ = %{public}d!", ret, isMovingPhoto_);
        }

        isSuccess_ = false;
        CanAddPhotoOutput(&isSuccess_);
        if (!isSuccess_) {
            DRAWING_LOGD(" [RM003 log] NDKCamera::StartPhoto can not add photo output!");
            return Camera_ErrorCode::CAMERA_OPERATION_NOT_ALLOWED;
        }
        ret = OH_CaptureSession_AddPhotoOutput(captureSession_, photoOutput_);
        OH_LOG_INFO(LOG_APP, "startPhoto AddPhotoOutput ret = %{public}d.", ret);
        ret = SessionCommitConfig();

        OH_LOG_INFO(LOG_APP, "startPhoto SessionCommitConfig ret = %{public}d.", ret);
        ret = SessionStart();
        OH_LOG_INFO(LOG_APP, "startPhoto SessionStart ret = %{public}d.", ret);
    }
    ret = TakePicture();
    OH_LOG_INFO(LOG_APP, "startPhoto OH_PhotoOutput_Capture ret = %{public}d.", ret);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "startPhoto failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    takePictureTimes++;
    return ret_;
}

Camera_ErrorCode NDKCamera::StartPhotoWithOutSurfaceId()
{
    Camera_ErrorCode ret = CAMERA_OK;
    ret = TakePicture();
    OH_LOG_INFO(LOG_APP, "startPhoto OH_PhotoOutput_Capture ret = %{public}d.", ret);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "startPhoto failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
//     isSuccess_ = false;
//     OH_PhotoOutput_IsMovingPhotoSupported(photoOutput_, &isSuccess_);
//     if (!isSuccess_) {
//         DRAWING_LOGD(" [RM003 log] NDKCamera::StartVideo OH_PhotoOutput_IsMovingPhotoSupported return false!");
//     } else {
//         ret = OH_PhotoOutput_EnableMovingPhoto(photoOutput_, isMovingPhoto_);
//         DRAWING_LOGD(" [RM003 log] NDKCamera::StartVideo OH_PhotoOutput_EnableMovingPhoto return with ret code: %{public}d isMovingPhoto_ = %{public}d!", ret, isMovingPhoto_);
//     }
    return ret_;
}

// exposure mode
Camera_ErrorCode NDKCamera::IsExposureModeSupportedFn(uint32_t mode)
{
    OH_LOG_INFO(LOG_APP, "IsExposureModeSupportedFn start.");
    exposureMode_ = static_cast<Camera_ExposureMode>(mode);
    ret_ = OH_CaptureSession_IsExposureModeSupported(captureSession_, exposureMode_, &isExposureModeSupported_);
    if (&isExposureModeSupported_ == nullptr || ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "IsExposureModeSupported failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    ret_ = OH_CaptureSession_SetExposureMode(captureSession_, exposureMode_);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "SetExposureMode failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    ret_ = OH_CaptureSession_GetExposureMode(captureSession_, &exposureMode_);
    if (&exposureMode_ == nullptr || ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "GetExposureMode failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    OH_LOG_INFO(LOG_APP, "IsExposureModeSupportedFn end.");
    return ret_;
}

Camera_ErrorCode NDKCamera::IsMeteringPoint(int x, int y)
{
    OH_LOG_INFO(LOG_APP, "IsMeteringPoint start.");
    ret_ = OH_CaptureSession_GetExposureMode(captureSession_, &exposureMode_);
    if (&exposureMode_ == nullptr || ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "GetExposureMode failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    Camera_Point exposurePoint;
    exposurePoint.x = x;
    exposurePoint.y = y;
    ret_ = OH_CaptureSession_SetMeteringPoint(captureSession_, exposurePoint);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "SetMeteringPoint failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    ret_ = OH_CaptureSession_GetMeteringPoint(captureSession_, &exposurePoint);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "GetMeteringPoint failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    OH_LOG_INFO(LOG_APP, "IsMeteringPoint end.");
    return ret_;
}

Camera_ErrorCode NDKCamera::IsExposureBiasRange(int exposureBias)
{
    OH_LOG_INFO(LOG_APP, "IsExposureBiasRange end.");
    float exposureBiasValue = (float)exposureBias;
    ret_ = OH_CaptureSession_GetExposureBiasRange(captureSession_, &minExposureBias_, &maxExposureBias_, &step_);
    if (&minExposureBias_ == nullptr || &maxExposureBias_ == nullptr || &step_ == nullptr || ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "GetExposureBiasRange failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    ret_ = OH_CaptureSession_SetExposureBias(captureSession_, exposureBiasValue);
    OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_SetExposureBias end.");
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "SetExposureBias failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    ret_ = OH_CaptureSession_GetExposureBias(captureSession_, &exposureBiasValue);
    if (&exposureBiasValue == nullptr || ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "GetExposureBias failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    OH_LOG_INFO(LOG_APP, "IsExposureBiasRange end.");
    return ret_;
}

// focus mode
Camera_ErrorCode NDKCamera::IsFocusModeSupported(uint32_t mode)
{
    Camera_FocusMode focusMode = static_cast<Camera_FocusMode>(mode);
    ret_ = OH_CaptureSession_IsFocusModeSupported(captureSession_, focusMode, &isFocusModeSupported_);
    if (&isFocusModeSupported_ == nullptr || ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "IsFocusModeSupported failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    return ret_;
}

Camera_ErrorCode NDKCamera::IsFocusMode(uint32_t mode)
{
    OH_LOG_INFO(LOG_APP, "IsFocusMode start.");
    Camera_FocusMode focusMode = static_cast<Camera_FocusMode>(mode);
    ret_ = OH_CaptureSession_IsFocusModeSupported(captureSession_, focusMode, &isFocusModeSupported_);
    if (&isFocusModeSupported_ == nullptr || ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "IsFocusModeSupported failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    ret_ = OH_CaptureSession_SetFocusMode(captureSession_, focusMode);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "SetFocusMode failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    ret_ = OH_CaptureSession_GetFocusMode(captureSession_, &focusMode);
    if (&focusMode == nullptr || ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "GetFocusMode failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    OH_LOG_INFO(LOG_APP, "IsFocusMode end.");
    return ret_;
}

Camera_ErrorCode NDKCamera::IsFocusPoint(float x, float y)
{
    OH_LOG_INFO(LOG_APP, "IsFocusPoint start.");
    Camera_Point focusPoint;
    focusPoint.x = x;
    focusPoint.y = y;
    ret_ = OH_CaptureSession_SetFocusPoint(captureSession_, focusPoint);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "SetFocusPoint failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    ret_ = OH_CaptureSession_GetFocusPoint(captureSession_, &focusPoint);
    if (&focusPoint == nullptr || ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "GetFocusPoint failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    OH_LOG_INFO(LOG_APP, "IsFocusPoint end.");
    return ret_;
}

int32_t NDKCamera::GetVideoFrameWidth(void)
{
    videoProfile_ = cameraOutputCapability_->videoProfiles[0];
    if (videoProfile_ == nullptr) {
        OH_LOG_ERROR(LOG_APP, "Get videoProfiles failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    return videoProfile_->size.width;
}

int32_t NDKCamera::GetVideoFrameHeight(void)
{
    videoProfile_ = cameraOutputCapability_->videoProfiles[0];
    if (videoProfile_ == nullptr) {
        OH_LOG_ERROR(LOG_APP, "Get videoProfiles failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    return videoProfile_->size.height;
}

int32_t NDKCamera::GetVideoFrameRate(void)
{
    videoProfile_ = cameraOutputCapability_->videoProfiles[0];
    if (videoProfile_ == nullptr) {
        OH_LOG_ERROR(LOG_APP, "Get videoProfiles failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    return videoProfile_->range.min;
}

Camera_ErrorCode NDKCamera::VideoOutputStop(void)
{
    OH_LOG_ERROR(LOG_APP, "enter VideoOutputStop.");
    ret_ = OH_VideoOutput_Stop(videoOutput_);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "VideoOutputStop failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    return ret_;
}

Camera_ErrorCode NDKCamera::VideoOutputRelease(void)
{
    OH_LOG_ERROR(LOG_APP, "enter VideoOutputRelease.");
    ret_ = OH_VideoOutput_Release(videoOutput_);
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "VideoOutputRelease failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    return ret_;
}

Camera_ErrorCode NDKCamera::TakePicture(void)
{
    Camera_ErrorCode ret = CAMERA_OK;
    ret = OH_PhotoOutput_Capture(photoOutput_);
    OH_LOG_ERROR(LOG_APP, "takePicture OH_PhotoOutput_Capture ret = %{public}d.", ret);
    if (ret != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "startPhoto failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    return ret;
}

Camera_ErrorCode NDKCamera::TakePictureWithPhotoSettings(Camera_PhotoCaptureSetting photoSetting)
{
    Camera_ErrorCode ret = CAMERA_OK;
    ret = OH_PhotoOutput_Capture_WithCaptureSetting(photoOutput_, photoSetting);

    OH_LOG_INFO(LOG_APP,
                "TakePictureWithPhotoSettings get quality %{public}d, rotation %{public}d, mirror %{public}d, "
                "latitude, %{public}d, longitude %{public}d, altitude %{public}d",
                photoSetting.quality, photoSetting.rotation, photoSetting.mirror, photoSetting.location->latitude,
                photoSetting.location->longitude, photoSetting.location->altitude);

    OH_LOG_ERROR(LOG_APP, "takePicture TakePictureWithPhotoSettings ret = %{public}d.", ret);
    if (ret != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "startPhoto failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    return ret;
}

// CameraManager Callback
void CameraManagerStatusCallback(Camera_Manager *cameraManager, Camera_StatusInfo *status)
{
    OH_LOG_INFO(LOG_APP, "CameraManagerStatusCallback");
}

CameraManager_Callbacks *NDKCamera::GetCameraManagerListener(void)
{
    static CameraManager_Callbacks cameraManagerListener = {.onCameraStatus = CameraManagerStatusCallback};
    return &cameraManagerListener;
}

Camera_ErrorCode NDKCamera::CameraManagerRegisterCallback(void)
{
    ret_ = OH_CameraManager_RegisterCallback(cameraManager_, GetCameraManagerListener());
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_CameraManager_RegisterCallback failed.");
    }
    return ret_;
}

// CameraInput Callback
void OnCameraInputError(const Camera_Input *cameraInput, Camera_ErrorCode errorCode)
{
    OH_LOG_INFO(LOG_APP, "OnCameraInput errorCode = %{public}d", errorCode);
}

CameraInput_Callbacks *NDKCamera::GetCameraInputListener(void)
{
    static CameraInput_Callbacks cameraInputCallbacks = {.onError = OnCameraInputError};
    return &cameraInputCallbacks;
}

Camera_ErrorCode NDKCamera::CameraInputRegisterCallback(void)
{
    ret_ = OH_CameraInput_RegisterCallback(cameraInput_, GetCameraInputListener());
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_CameraInput_RegisterCallback failed.");
    }
    return ret_;
}

// PreviewOutput Callback
void PreviewOutputOnFrameStart(Camera_PreviewOutput *previewOutput)
{
    OH_LOG_INFO(LOG_APP, "PreviewOutputOnFrameStart");
}

void PreviewOutputOnFrameEnd(Camera_PreviewOutput *previewOutput, int32_t frameCount)
{
    OH_LOG_INFO(LOG_APP, "PreviewOutput frameCount = %{public}d", frameCount);
}

void PreviewOutputOnError(Camera_PreviewOutput *previewOutput, Camera_ErrorCode errorCode)
{
    OH_LOG_INFO(LOG_APP, "PreviewOutput errorCode = %{public}d", errorCode);
}

PreviewOutput_Callbacks *NDKCamera::GetPreviewOutputListener(void)
{
    static PreviewOutput_Callbacks previewOutputListener = {
        .onFrameStart = PreviewOutputOnFrameStart,
        .onFrameEnd = PreviewOutputOnFrameEnd,
        .onError = PreviewOutputOnError
    };
    return &previewOutputListener;
}

Camera_ErrorCode NDKCamera::PreviewOutputRegisterCallback(void)
{
    ret_ = OH_PreviewOutput_RegisterCallback(previewOutput_, GetPreviewOutputListener());
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_PreviewOutput_RegisterCallback failed.");
    }
    return ret_;
}

// PhotoOutput Callback
void PhotoOutputOnFrameStart(Camera_PhotoOutput *photoOutput)
{
    OH_LOG_INFO(LOG_APP, "PhotoOutputOnFrameStart");
}

void PhotoOutputOnFrameShutter(Camera_PhotoOutput *photoOutput, Camera_FrameShutterInfo *info)
{
    OH_LOG_INFO(LOG_APP, "PhotoOutputOnFrameShutter");
}

void PhotoOutputOnFrameEnd(Camera_PhotoOutput *photoOutput, int32_t frameCount)
{
    OH_LOG_INFO(LOG_APP, "PhotoOutput frameCount = %{public}d", frameCount);
}

void PhotoOutputOnError(Camera_PhotoOutput *photoOutput, Camera_ErrorCode errorCode)
{
    OH_LOG_INFO(LOG_APP, "PhotoOutput errorCode = %{public}d", errorCode);
}

PhotoOutput_Callbacks *NDKCamera::GetPhotoOutputListener(void)
{
    static PhotoOutput_Callbacks photoOutputListener = {
        .onFrameStart = PhotoOutputOnFrameStart,
        .onFrameShutter = PhotoOutputOnFrameShutter,
        .onFrameEnd = PhotoOutputOnFrameEnd,
        .onError = PhotoOutputOnError
    };
    return &photoOutputListener;
}

Camera_ErrorCode NDKCamera::PhotoOutputRegisterCallback(void)
{
    ret_ = OH_PhotoOutput_RegisterCallback(photoOutput_, GetPhotoOutputListener());
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_PhotoOutput_RegisterCallback failed.");
    }
    return ret_;
}

// VideoOutput Callback
void VideoOutputOnFrameStart(Camera_VideoOutput *videoOutput)
{
    OH_LOG_INFO(LOG_APP, "VideoOutputOnFrameStart");
}

void VideoOutputOnFrameEnd(Camera_VideoOutput *videoOutput, int32_t frameCount)
{
    OH_LOG_INFO(LOG_APP, "VideoOutput frameCount = %{public}d", frameCount);
}

void VideoOutputOnError(Camera_VideoOutput *videoOutput, Camera_ErrorCode errorCode)
{
    OH_LOG_INFO(LOG_APP, "VideoOutput errorCode = %{public}d", errorCode);
}

VideoOutput_Callbacks *NDKCamera::GetVideoOutputListener(void)
{
    static VideoOutput_Callbacks videoOutputListener = {
        .onFrameStart = VideoOutputOnFrameStart,
        .onFrameEnd = VideoOutputOnFrameEnd,
        .onError = VideoOutputOnError
    };
    return &videoOutputListener;
}

Camera_ErrorCode NDKCamera::VideoOutputRegisterCallback(void)
{
    ret_ = OH_VideoOutput_RegisterCallback(videoOutput_, GetVideoOutputListener());
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_VideoOutput_RegisterCallback failed.");
    }
    return ret_;
}

// Metadata Callback
void OnMetadataObjectAvailable(Camera_MetadataOutput *metadataOutput, Camera_MetadataObject *metadataObject,
                               uint32_t size)
{
    OH_LOG_INFO(LOG_APP, "size = %{public}d", size);
}

void OnMetadataOutputError(Camera_MetadataOutput *metadataOutput, Camera_ErrorCode errorCode)
{
    OH_LOG_INFO(LOG_APP, "OnMetadataOutput errorCode = %{public}d", errorCode);
}

MetadataOutput_Callbacks *NDKCamera::GetMetadataOutputListener(void)
{
    static MetadataOutput_Callbacks metadataOutputListener = {
        .onMetadataObjectAvailable = OnMetadataObjectAvailable,
        .onError = OnMetadataOutputError
    };
    return &metadataOutputListener;
}

Camera_ErrorCode NDKCamera::MetadataOutputRegisterCallback(void)
{
    ret_ = OH_MetadataOutput_RegisterCallback(metadataOutput_, GetMetadataOutputListener());
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_MetadataOutput_RegisterCallback failed.");
    }
    return ret_;
}

// Session Callback
void CaptureSessionOnFocusStateChange(Camera_CaptureSession *session, Camera_FocusState focusState)
{
    DRAWING_LOGD(" [RM003 log] CaptureSession_Callbacks CaptureSessionOnFocusStateChange");
    OH_LOG_INFO(LOG_APP, "CaptureSessionOnFocusStateChange");
}

void CaptureSessionOnError(Camera_CaptureSession *session, Camera_ErrorCode errorCode)
{
    DRAWING_LOGD(" [RM003 log] CaptureSession_Callbacks CaptureSessionOnError");
    OH_LOG_INFO(LOG_APP, "CaptureSession errorCode = %{public}d", errorCode);
}

CaptureSession_Callbacks *NDKCamera::GetCaptureSessionRegister(void)
{
    static CaptureSession_Callbacks captureSessionCallbacks = {
        .onFocusStateChange = CaptureSessionOnFocusStateChange,
        .onError = CaptureSessionOnError
    };
    return &captureSessionCallbacks;
}

Camera_ErrorCode NDKCamera::CaptureSessionRegisterCallback(void)
{
    ret_ = OH_CaptureSession_RegisterCallback(captureSession_, GetCaptureSessionRegister());
    if (ret_ != CAMERA_OK) {
        OH_LOG_ERROR(LOG_APP, "OH_CaptureSession_RegisterCallback failed.");
    }
    return ret_;
}

Camera_ErrorCode NDKCamera::CanAddInput(bool *isSuccess)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::CanAddInput start!");
    ret_ = OH_CaptureSession_CanAddInput(captureSession_, cameraInput_, isSuccess);
    if (ret_ != CAMERA_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::CanAddInput failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::CanAddInput isSuccess: %{public}d!", *isSuccess);
    DRAWING_LOGD(" [RM003 log] NDKCamera::CanAddInput return with ret code: %{public}d!", ret_);
    return ret_;
}
Camera_ErrorCode NDKCamera::CanAddPreviewOutput(bool *isSuccess)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::CanAddPreviewOutput start!");
    ret_ = OH_CaptureSession_CanAddPreviewOutput(captureSession_, previewOutput_, isSuccess);
    if (ret_ != CAMERA_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::CanAddPreviewOutput failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::CanAddPreviewOutput isSuccess: %{public}d!", *isSuccess);
    DRAWING_LOGD(" [RM003 log] NDKCamera::CanAddPreviewOutput return with ret code: %{public}d!", ret_);
    return ret_;
}
Camera_ErrorCode NDKCamera::CanAddPhotoOutput(bool *isSuccess)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::CanAddPhotoOutput start!");
    ret_ = OH_CaptureSession_CanAddPhotoOutput(captureSession_, photoOutput_, isSuccess);
    if (ret_ != CAMERA_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::CanAddPhotoOutput failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::CanAddPhotoOutput isSuccess: %{public}d!", *isSuccess);
    DRAWING_LOGD(" [RM003 log] NDKCamera::CanAddPhotoOutput return with ret code: %{public}d!", ret_);
    return ret_;
}
Camera_ErrorCode NDKCamera::CanAddVideoOutput(bool *isSuccess)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::CanAddVideoOutput start!");
    ret_ = OH_CaptureSession_CanAddVideoOutput(captureSession_, videoOutput_, isSuccess);
    if (ret_ != CAMERA_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::CanAddVideoOutput failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::CanAddVideoOutput isSuccess: %{public}d!", *isSuccess);
    DRAWING_LOGD(" [RM003 log] NDKCamera::CanAddVideoOutput return with ret code: %{public}d!", ret_);
    return ret_;
}
Camera_ErrorCode NDKCamera::CanPreconfig(bool *isSuccess)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::CanPreconfig start!");
    ret_ = OH_CaptureSession_CanPreconfig(captureSession_, preconfigType_, isSuccess);
    if (ret_ != CAMERA_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::CanPreconfig failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::CanPreconfig isSuccess: %{public}d!", *isSuccess);
    DRAWING_LOGD(" [RM003 log] NDKCamera::CanPreconfig return with ret code: %{public}d!", ret_);
    return ret_;
}
Camera_ErrorCode NDKCamera::Preconfig(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::Preconfig start!");
    ret_ = OH_CaptureSession_Preconfig(captureSession_, preconfigType_);
    if (ret_ != CAMERA_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::Preconfig failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::Preconfig return with ret code: %{public}d!", ret_);
    return ret_;
}
Camera_ErrorCode NDKCamera::CanPreconfigWithRatio(bool *isSuccess)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::CanPreconfigWithRatio start!");
    ret_ = OH_CaptureSession_CanPreconfigWithRatio(captureSession_, preconfigType_, preconfigRatio_, isSuccess);
    if (ret_ != CAMERA_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::CanPreconfigWithRatio failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::CanPreconfigWithRatio isSuccess: %{public}d!", *isSuccess);
    DRAWING_LOGD(" [RM003 log] NDKCamera::CanPreconfigWithRatio return with ret code: %{public}d!", ret_);
    return ret_;
}
Camera_ErrorCode NDKCamera::PreconfigWithRatio(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::PreconfigWithRatio start!");
    ret_ = OH_CaptureSession_PreconfigWithRatio(captureSession_, preconfigType_, preconfigRatio_);
    if (ret_ != CAMERA_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::PreconfigWithRatio failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::PreconfigWithRatio return with ret code: %{public}d!", ret_);
    return ret_;
}
void onPhotoAssetAvailable(Camera_PhotoOutput *photoOutput, OH_MediaAsset *mediaAsset)
{
    DRAWING_LOGD(" [RM003 log] onPhotoAssetAvailable start!");
    if (photoOutput == nullptr) {
        DRAWING_LOGD(" [RM003 log] onPhotoAssetAvailable return because photoOutput is nullptr !");
        return;
    }
    if (mediaAsset == nullptr) {
        DRAWING_LOGD(" [RM003 log] onPhotoAssetAvailable return because mediaAsset is nullptr !");
        return;
    }
    if (g_mediaAsset != nullptr) {
        NDKCamera::MediaAssetRelease();
    }
    g_mediaAsset = mediaAsset;
    NDKCamera::MediaAssetGetUri();
    NDKCamera::MediaAssetGetMediaType();
    NDKCamera::MediaAssetGetMediaSubType();
    NDKCamera::MediaAssetGetDisplayName();
    NDKCamera::MediaAssetGetSize();
    NDKCamera::MediaAssetGetDateAdded();
    NDKCamera::MediaAssetGetDateModified();
    NDKCamera::MediaAssetGetDateTaken();
    NDKCamera::MediaAssetGetDateAddedMs();
    NDKCamera::MediaAssetGetDateModifiedMs();
    NDKCamera::MediaAssetGetDuration();
    NDKCamera::MediaAssetGetWidth();
    NDKCamera::MediaAssetGetHeight();
    NDKCamera::MediaAssetGetOrientation();
    NDKCamera::MediaAssetIsFavorite();
    NDKCamera::MediaAssetGetTitle();

    if (g_changeRequest) {
        DRAWING_LOGD(" [RM003 log] Release Change Request!");
        NDKCamera::ChangeRequestRelease();
    }
    NDKCamera::MediaAssetChangeRequestCreate();
    if (g_isMovingPhoto) {
        NDKCamera::ChangeRequestSaveCameraPhoto();
        NDKCamera::ManagerRequestMovingPhoto();
    } else {
        NDKCamera::MediaAssetManagerRequestImage();
    }
    DRAWING_LOGD(" [RM003 log] onPhotoAssetAvailable finish!");
    return;
}
Camera_ErrorCode NDKCamera::PhotoOutputRegisterPhotoAssetAvailableCallback(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::PhotoOutputRegisterPhotoAssetAvailableCallback start!");
    ret_ = OH_PhotoOutput_RegisterPhotoAssetAvailableCallback(photoOutput_, onPhotoAssetAvailable);
    if (ret_ != CAMERA_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::PhotoOutputRegisterPhotoAssetAvailableCallback failed.");
        return CAMERA_INVALID_ARGUMENT;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::PhotoOutputRegisterPhotoAssetAvailableCallback return with ret code: %{public}d", ret_);
    return ret_;
}
void NDKCamera::MediaAssetGetUri(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetUri start!");
    result = OH_MediaAsset_GetUri(g_mediaAsset, &g_uri);
    if (g_uri == nullptr || result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetUri failed.");
        return;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetUri uri: %{public}s", g_uri);
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetUri end!");
    return;
}
void NDKCamera::MediaAssetGetMediaType(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetMediaType start!");
    result = OH_MediaAsset_GetMediaType(g_mediaAsset, &g_mediaType);
    if (result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetMediaType failed.");
        return;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetMediaType mediaType: %{public}d", g_mediaType);
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetMediaType end!");
    return;
}
void NDKCamera::MediaAssetGetMediaSubType(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetMediaSubType start!");
    result = OH_MediaAsset_GetMediaSubType(g_mediaAsset, &g_mediaSubType);
    if (result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetMediaSubType failed.");
        return;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetMediaSubType mediaSubType: %{public}d", g_mediaSubType);
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetMediaSubType end!");
    return;
}
void NDKCamera::MediaAssetGetDisplayName(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetDisplayName start!");
    result = OH_MediaAsset_GetDisplayName(g_mediaAsset, &g_displayName);
    if (g_displayName == nullptr || result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetDisplayName failed.");
        return;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetDisplayName displayName: %{public}s", g_displayName);
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetDisplayName end!");
    return;
}
void NDKCamera::MediaAssetGetSize(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetSize start!");
    result = OH_MediaAsset_GetSize(g_mediaAsset, &g_mediaAssetSize);
    if (result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetSize failed.");
        return;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetSize mediaAssetSize: %{public}d", g_mediaAssetSize);
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetSize end!");
    return;
}
void NDKCamera::MediaAssetGetDateAdded(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetDateAdded start!");
    result = OH_MediaAsset_GetDateAdded(g_mediaAsset, &g_dateAdded);
    if (result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetDateAdded failed.");
        return;
    }
    DRAWING_LOGD("[RM003 log] NDKCamera::MediaAssetGetDateAdded dateAdded: %{public}d", g_dateAdded);
//     std::chrono::seconds timestamp(g_dateAdded); // 替换成你自己的时间戳
//     std::time_t time = timestamp.count();
//     std::tm* local_time = std::localtime(&time);
//     std::string osString = [&local_time]()->std::string{
//         std::ostringstream oss;
//         oss << std::put_time(local_time, "%Y-%m-%d %H:%M:%S");
//         return oss.str();
//     }();
//     DRAWING_LOGD("[RM003 log] NDKCamera::MediaAssetGetDateAdded dateAdded localtime: %{public}s", osString.c_str());
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetDateAdded end!");
    return;
}
void NDKCamera::MediaAssetGetDateModified(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetDateModified start!");
    result = OH_MediaAsset_GetDateModified(g_mediaAsset, &g_dateModified);
    if (result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetDateModified failed.");
        return;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetDateModified dateModified: %{public}d", g_dateModified);
//     std::chrono::seconds timestamp(g_dateModified); // 替换成你自己的时间戳
//     std::time_t time = timestamp.count();
//     std::tm* local_time = std::localtime(&time);
//     std::string osString = [&local_time]()->std::string{
//         std::ostringstream oss;
//         oss << std::put_time(local_time, "%Y-%m-%d %H:%M:%S");
//         return oss.str();
//     }();
//     DRAWING_LOGD("[RM003 log] NDKCamera::MediaAssetGetDateModified dateModified localtime : %{public}s", osString.c_str());
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetDateModified end!");
    return;
}
void NDKCamera::MediaAssetGetDateTaken(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetDateTaken start!");
    result = OH_MediaAsset_GetDateTaken(g_mediaAsset, &g_dateTaken);
    if (result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetDateTaken failed.");
        return;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetDateTaken dateTaken: %{public}d", g_dateTaken);
//     std::chrono::seconds timestamp(g_dateTaken); // 替换成你自己的时间戳
//     std::time_t time = timestamp.count();
//     std::tm* local_time = std::localtime(&time);
//     std::string osString = [&local_time]()->std::string{
//         std::ostringstream oss;
//         oss << std::put_time(local_time, "%Y-%m-%d %H:%M:%S");
//         return oss.str();
//     }();
//     DRAWING_LOGD("[RM003 log] NDKCamera::MediaAssetGetDateTaken dateTaken localtime: %{public}s", osString.c_str());
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetDateTaken end!");
    return;
}
void NDKCamera::MediaAssetGetDateAddedMs(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetDateAddedMs start!");
    result = OH_MediaAsset_GetDateAddedMs(g_mediaAsset, &g_dateAddedMs);
    if (result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetDateAddedMs failed.");
        return;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetDateAddedMs dateAddedMs: %{public}lu", g_dateAddedMs);
//     std::chrono::seconds timestamp(g_dateAddedMs); // 替换成你自己的时间戳
//     std::time_t time = timestamp.count();
//     std::tm* local_time = std::localtime(&time);
//     std::string osString = [&local_time]()->std::string{
//         std::ostringstream oss;
//         oss << std::put_time(local_time, "%Y-%m-%d %H:%M:%S");
//         return oss.str();
//     }();
//     DRAWING_LOGD("[RM003 log] NDKCamera::MediaAssetGetDateAdded dateAddedMs localtime: %{public}s", osString.c_str());
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetDateAddedMs end!");
    return;
}
void NDKCamera::MediaAssetGetDateModifiedMs(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetDateModifiedMs start!");
    result = OH_MediaAsset_GetDateModifiedMs(g_mediaAsset, &g_dateModifiedMs);
    if (result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetDateModifiedMs failed.");
        return;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetDateModifiedMs dateModifiedMs: %{public}lu", g_dateModifiedMs);
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetDateModifiedMs end!");
    return;
}
void NDKCamera::MediaAssetGetDuration(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetDuration start!");
    result = OH_MediaAsset_GetDuration(g_mediaAsset, &g_duration);
    if (result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetDuration failed.");
        return;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetDuration duration: %{public}d", g_duration);
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetDuration end!");
    return;
}
void NDKCamera::MediaAssetGetWidth(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetWidth start!");
    result = OH_MediaAsset_GetWidth(g_mediaAsset, &g_width);
    if (result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetWidth failed.");
        return;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetWidth width: %{public}d", g_width);
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetWidth end!");
    return;
}
void NDKCamera::MediaAssetGetHeight(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetHeight start!");
    result = OH_MediaAsset_GetHeight(g_mediaAsset, &g_height);
    if (result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetHeight failed.");
        return;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetHeight height: %{public}d", g_height);
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetHeight end!");
    return;
}
void NDKCamera::MediaAssetGetOrientation(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetOrientation start!");
    result = OH_MediaAsset_GetOrientation(g_mediaAsset, &g_orientation);
    if (result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetOrientation failed.");
        return;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetOrientation orientation: %{public}d", g_orientation);
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetOrientation end!");
    return;
}
void NDKCamera::MediaAssetIsFavorite(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetIsFavorite start!");
    result = OH_MediaAsset_IsFavorite(g_mediaAsset, &g_favorite);
    if (result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetIsFavorite failed.");
        return;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetIsFavorite favorite: %{public}d", g_favorite);
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetIsFavorite end!");
    return;
}
void NDKCamera::MediaAssetGetTitle(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetTitle start!");
    result = OH_MediaAsset_GetTitle(g_mediaAsset, &g_title);
    if (g_title == nullptr || result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetTitle failed.");
        return;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetTitle title: %{public}s", g_title);
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetGetTitle end!");
    return;
}
void NDKCamera::MediaAssetRelease(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetRelease start!");
    result = OH_MediaAsset_Release(g_mediaAsset);
    if (result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetRelease failed.");
        return;
    }
    g_mediaAsset = nullptr;
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetRelease end!");
    return;
}
void NDKCamera::MediaAssetChangeRequestCreate(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetChangeRequestCreate start!");
    g_changeRequest = OH_MediaAssetChangeRequest_Create(g_mediaAsset);
    if (g_changeRequest == nullptr) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetChangeRequestCreate failed.");
        return;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetChangeRequestCreate end!");
    return;
}
void NDKCamera::ChangeRequestAddResourceWithUri1(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestAddResourceWithUri1 start!");
    result = OH_MediaAssetChangeRequest_AddResourceWithUri(g_changeRequest, RESOURCE_TYPE_IMAGE, "file://com.samples.normalCamera/data/storage/el2/base/haps/test1.jpg");
    if (result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestAddResourceWithUri1 RESOURCE_TYPE_IMAGE failed.");
        return;
    }
    result = OH_MediaAccessHelper_ApplyChanges(g_changeRequest);
    if (result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestAddResourceWithUri1 OH_MediaAccessHelper_ApplyChanges failed.");
        return;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestAddResourceWithUri1 end!");
    return;
}
void NDKCamera::ChangeRequestAddResourceWithUri2(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestAddResourceWithUri2 start!");
    result = OH_MediaAssetChangeRequest_AddResourceWithUri(g_changeRequest, RESOURCE_TYPE_IMAGE, "file://com.samples.normalCamera/data/storage/el2/base/haps/test2.jpg");
    if (result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestAddResourceWithUri2 RESOURCE_TYPE_IMAGE failed.");
        return;
    }
    result = OH_MediaAccessHelper_ApplyChanges(g_changeRequest);
    if (result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestAddResourceWithUri2 OH_MediaAccessHelper_ApplyChanges failed.");
        return;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestAddResourceWithUri2 end!");
    return;
}
void NDKCamera::ChangeRequestAddResourceWithBuffer1(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestAddResourceWithBuffer1 start!");
    size_t bufferSize = 0;
    size_t readSize = 10000;
    char readBuffer[10000];
    int fd = open("/data/storage/el2/base/haps/test1.jpg", O_RDONLY);
    int fr = 0;
    while (1) {
        fr = read(fd, readBuffer, readSize);
        if (fr == -1) {
            DRAWING_LOGD("[RM003 log] NDKCamera::ChangeRequestAddResourceWithBuffer1 read readBuffer failed.");
            return;
        }
        bufferSize += fr;
        if (fr == 10000) {
            DRAWING_LOGD("[RM003 log] NDKCamera::ChangeRequestAddResourceWithBuffer1 read readBuffer not complete.");
        } else {
            DRAWING_LOGD("[RM003 log] NDKCamera::ChangeRequestAddResourceWithBuffer1 read readBuffer complete.");
            break;
        }
    }
    char buffer[bufferSize];
    close(fd);
    fd = open("/data/storage/el2/base/haps/test1.jpg", O_RDONLY);
    fr = read(fd, buffer, bufferSize);
    if (fr == -1 || fr != bufferSize) {
        DRAWING_LOGD("[RM003 log] NDKCamera::ChangeRequestAddResourceWithBuffer1 read buffer failed.");
        return;
    }
    close(fd);
    result = OH_MediaAssetChangeRequest_AddResourceWithBuffer(g_changeRequest, RESOURCE_TYPE_IMAGE, (uint8_t *)buffer, (uint32_t)bufferSize);
    if (result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestAddResourceWithBuffer1 failed.");
        return;
    }
    result = OH_MediaAccessHelper_ApplyChanges(g_changeRequest);
    if (result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestAddResourceWithBuffer1 OH_MediaAccessHelper_ApplyChanges failed.");
        return;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestAddResourceWithBuffer1 end!");
    return;
}
void NDKCamera::ChangeRequestAddResourceWithBuffer2(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestAddResourceWithBuffer2 start!");
    size_t bufferSize = 0;
    size_t readSize = 10000;
    char readBuffer[10000];
    int fd = open("/data/storage/el2/base/haps/test2.jpg", O_RDONLY);
    int fr = 0;
    while (1) {
        fr = read(fd, readBuffer, readSize);
        if (fr == -1) {
            DRAWING_LOGD("[RM003 log] NDKCamera::ChangeRequestAddResourceWithBuffer2 read readBuffer failed.");
            return;
        }
        bufferSize += fr;
        if (fr == 10000) {
            DRAWING_LOGD("[RM003 log] NDKCamera::ChangeRequestAddResourceWithBuffer2 read readBuffer not complete.");
        } else {
            DRAWING_LOGD("[RM003 log] NDKCamera::ChangeRequestAddResourceWithBuffer2 read readBuffer complete.");
            break;
        }
    }
    char buffer[bufferSize];
    close(fd);
    fd = open("/data/storage/el2/base/haps/test2.jpg", O_RDONLY);
    fr = read(fd, buffer, bufferSize);
    if (fr == -1 || fr != bufferSize) {
        DRAWING_LOGD("[RM003 log] NDKCamera::ChangeRequestAddResourceWithBuffer2 read buffer failed.");
        return;
    }
    close(fd);
    result = OH_MediaAssetChangeRequest_AddResourceWithBuffer(g_changeRequest, RESOURCE_TYPE_IMAGE, (uint8_t *)buffer, (uint32_t)bufferSize);
    if (result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestAddResourceWithBuffer2 failed.");
        return;
    }
    result = OH_MediaAccessHelper_ApplyChanges(g_changeRequest);
    if (result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestAddResourceWithBuffer2 OH_MediaAccessHelper_ApplyChanges failed.");
        return;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestAddResourceWithBuffer2 end!");
    return;
}
void NDKCamera::ChangeRequestGetWriteCacheHandler1(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestGetWriteCacheHandler1 start!");
    result = OH_MediaAssetChangeRequest_GetWriteCacheHandler(g_changeRequest, &g_fd);
    if (result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestGetWriteCacheHandler1 failed.");
        DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestGetWriteCacheHandler2 failed %{public}d.", result);
        return;
    }
    size_t bufferSize = 0;
    size_t readSize = 10000;
    char readBuffer[10000];
    int fd = open("/data/storage/el2/base/haps/test1.jpg", O_RDONLY);
    int fr = 0;
    while (1) {
        fr = read(fd, readBuffer, readSize);
        if (fr == -1) {
            DRAWING_LOGD("[RM003 log] NDKCamera::ChangeRequestGetWriteCacheHandler1 read readBuffer failed.");
            return;
        }
        bufferSize += fr;
        if (fr == 10000) {
            DRAWING_LOGD("[RM003 log] NDKCamera::ChangeRequestGetWriteCacheHandler1 read readBuffer not complete.");
        } else {
            DRAWING_LOGD("[RM003 log] NDKCamera::ChangeRequestGetWriteCacheHandler1 read readBuffer complete.");
            break;
        }
    }
    char buffer[bufferSize];
    close(fd);
    fd = open("/data/storage/el2/base/haps/test1.jpg", O_RDONLY);
    fr = read(fd, buffer, bufferSize);
    if (fr == -1 || fr != bufferSize) {
        DRAWING_LOGD("[RM003 log] NDKCamera::ChangeRequestGetWriteCacheHandler1 read buffer failed.");
        return;
    }
    write(g_fd, buffer, fr);
    close(g_fd);
    close(fd);
    result = OH_MediaAccessHelper_ApplyChanges(g_changeRequest);
    if (result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestGetWriteCacheHandler1 OH_MediaAccessHelper_ApplyChanges failed.");
        return;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestGetWriteCacheHandler1 end!");
    return;
}
void NDKCamera::ChangeRequestGetWriteCacheHandler2(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestGetWriteCacheHandler2 start!");
    result = OH_MediaAssetChangeRequest_GetWriteCacheHandler(g_changeRequest, &g_fd);
    if (result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestGetWriteCacheHandler2 failed.");
        return;
    }
    size_t bufferSize = 0;
    size_t readSize = 10000;
    char readBuffer[10000];
    int fd = open("/data/storage/el2/base/haps/test2.jpg", O_RDONLY);
    int fr = 0;
    while (1) {
        fr = read(fd, readBuffer, readSize);
        if (fr == -1) {
            DRAWING_LOGD("[RM003 log] NDKCamera::ChangeRequestGetWriteCacheHandler2 read readBuffer failed.");
            return;
        }
        bufferSize += fr;
        if (fr == 10000) {
            DRAWING_LOGD("[RM003 log] NDKCamera::ChangeRequestGetWriteCacheHandler2 read readBuffer not complete.");
        } else {
            DRAWING_LOGD("[RM003 log] NDKCamera::ChangeRequestGetWriteCacheHandler2 read readBuffer complete.");
            break;
        }
    }
    char buffer[bufferSize];
    close(fd);
    fd = open("/data/storage/el2/base/haps/test2.jpg", O_RDONLY);
    fr = read(fd, buffer, bufferSize);
    if (fr == -1 || fr != bufferSize) {
        DRAWING_LOGD("[RM003 log] NDKCamera::ChangeRequestGetWriteCacheHandler2 read buffer failed.");
        return;
    }
    write(g_fd, buffer, fr);
    close(g_fd);
    close(fd);
    result = OH_MediaAccessHelper_ApplyChanges(g_changeRequest);
    if (result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestGetWriteCacheHandler2 OH_MediaAccessHelper_ApplyChanges failed.");
        return;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestGetWriteCacheHandler2 end!");
    return;
}
void NDKCamera::ChangeRequestSaveCameraPhoto(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestSaveCameraPhoto start!");
    result = OH_MediaAssetChangeRequest_SaveCameraPhoto(g_changeRequest, IMAGE_TYPE_JPEG);
    if (result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestSaveCameraPhoto failed.");
        return;
    }
    result = OH_MediaAccessHelper_ApplyChanges(g_changeRequest);
    if (result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestSaveCameraPhoto OH_MediaAccessHelper_ApplyChanges failed.");
        return;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestSaveCameraPhoto end!");
    return;
}
void NDKCamera::ChangeRequestDiscardCameraPhoto(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestDiscardCameraPhoto start!");
    result = OH_MediaAssetChangeRequest_DiscardCameraPhoto(g_changeRequest);
    if (result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestDiscardCameraPhoto failed.");
        return;
    }
    result = OH_MediaAccessHelper_ApplyChanges(g_changeRequest);
    if (result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestDiscardCameraPhoto OH_MediaAccessHelper_ApplyChanges failed.");
        return;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestDiscardCameraPhoto end!");
    return;
}
void NDKCamera::ChangeRequestRelease(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestRelease start!");
    result = OH_MediaAssetChangeRequest_Release(g_changeRequest);
    if (result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestRelease failed.");
        return;
    }
    g_changeRequest = nullptr;
    DRAWING_LOGD(" [RM003 log] NDKCamera::ChangeRequestRelease end!");
    return;
}
void NDKCamera::MediaAssetManagerCreate(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetManagerCreate start!");
    g_mediaAssetManager = OH_MediaAssetManager_Create();
    if (g_mediaAssetManager == nullptr) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetManagerCreate failed.");
        return;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetManagerCreate end!");
    return;
}
void onMovingPhotoDataPrepared(MediaLibrary_ErrorCode result, MediaLibrary_RequestId requestId,
    MediaLibrary_MediaQuality mediaQuality, MediaLibrary_MediaContentType type, OH_MovingPhoto* movingPhoto)
{
    DRAWING_LOGD(" [RM003 log] onMovingPhotoDataPrepared start result:%{public}d!", result);
    if (movingPhoto == nullptr) {
        DRAWING_LOGD(" [RM003 log] onMovingPhotoDataPrepared movingPhoto is nullptr!");
        return;
    }
    g_movingPhoto = movingPhoto;
    auto cb = (void (*)(char *))(requestImageCallback);
    auto qCb = (void (*)(char *))(requestImageQualityCallback);
    // Todo: use result and requestId to judge the callback is legal or not
    if (mediaQuality == MediaLibrary_MediaQuality::MEDIA_LIBRARY_QUALITY_FAST) {
        DRAWING_LOGD(" [RM003 log] onMovingPhotoDataPrepared mediaQuality is MEDIA_LIBRARY_QUALITY_FAST!");
        g_mediaQualityCb = "fast";
        qCb(g_mediaQualityCb);

    }
    if (mediaQuality == MediaLibrary_MediaQuality::MEDIA_LIBRARY_QUALITY_FULL) {
        DRAWING_LOGD(" [RM003 log] onMovingPhotoDataPrepared mediaQuality is MEDIA_LIBRARY_QUALITY_FULL!");
        g_mediaQualityCb = "high";
        qCb(g_mediaQualityCb);

    }

    NDKCamera::MovingPhotoGetUri();
    DRAWING_LOGD(" [RM003 log] onMovingPhotoDataPrepared uri: %{public}s", g_uri);
    cb((char *)g_uri);

    g_imageUri = "file://com.samples.normalCamera/data/storage/el2/base/haps/ImageFile.jpg";
    g_videoUri = "file://com.samples.normalCamera/data/storage/el2/base/haps/VideoFile.mp4";
    
    for (int i = 0; i < 5; i++) {
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
        bool ret = NDKCamera::MovingPhotoRequestContentWithUris();
        if (!ret) {
            continue;
        }
        ret = NDKCamera::MovingPhotoRequestContentWithUri();
        if (!ret) {
            continue;
        }
        ret = NDKCamera::MovingPhotoRequestContentWithBuffer();
        if (!ret) {
            continue;
        }
        break;
    }
    return;
}
void NDKCamera::ManagerRequestMovingPhoto(void)
{

    DRAWING_LOGD(" [RM003 log] NDKCamera::ManagerRequestMovingPhoto start!");
    g_requestOptions.deliveryMode = g_deliveryMode;
    DRAWING_LOGD(" [RM003 log] NDKCamera::ManagerRequestMovingPhoto deliveryMode: %{public}d", g_requestOptions.deliveryMode);
    result = OH_MediaAssetManager_RequestMovingPhoto(g_mediaAssetManager, g_mediaAsset, g_requestOptions, &g_requestId,
                                                     onMovingPhotoDataPrepared);
    if (result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::ManagerRequestMovingPhoto failed.");
        return;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::ManagerRequestMovingPhoto end!");
    return;
}
void onImageDataPrepared(MediaLibrary_ErrorCode result, MediaLibrary_RequestId requestId,
    MediaLibrary_MediaQuality mediaQuality, MediaLibrary_MediaContentType type, OH_ImageSourceNative *imageSourceNative)
{
    DRAWING_LOGD(" [RM003 log] onImageDataPrepared start!");
    if (imageSourceNative == nullptr) {
        DRAWING_LOGD(" [RM003 log] onImageDataPrepared imageSourceNative is nullptr!");
        return;
    }
    g_imageSourceNative = imageSourceNative;
    auto cb = (void (*)(char *))(requestImageCallback);
    auto qCb = (void (*)(char *))(requestImageQualityCallback);
    // Todo: use result and requestId to judge the callback is legal or not
    if (mediaQuality == MediaLibrary_MediaQuality::MEDIA_LIBRARY_QUALITY_FAST) {
        DRAWING_LOGD(" [RM003 log] onImageDataPrepared mediaQuality is MEDIA_LIBRARY_QUALITY_FAST!");
        g_mediaQualityCb = "fast";
        qCb(g_mediaQualityCb);
    }
    if (mediaQuality == MediaLibrary_MediaQuality::MEDIA_LIBRARY_QUALITY_FULL) {
        DRAWING_LOGD(" [RM003 log] onImageDataPrepared mediaQuality is MEDIA_LIBRARY_QUALITY_FULL!");
        g_mediaQualityCb = "high";
        qCb(g_mediaQualityCb);
    }
    DRAWING_LOGD(" [RM003 log] onImageDataPrepared uri: %{public}s", g_uri);
    cb((char *)g_uri);
    return;
}
void NDKCamera::MediaAssetManagerRequestImage(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetManagerRequestImage start!");
    g_requestOptions.deliveryMode = g_deliveryMode;
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetManagerRequestImage deliveryMode: %{public}d", g_requestOptions.deliveryMode);
    result = OH_MediaAssetManager_RequestImage(g_mediaAssetManager, g_mediaAsset, g_requestOptions, &g_requestId,
                                               onImageDataPrepared);
    if (result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetManagerRequestImage failed.");
        return;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetManagerRequestImage end!");
    return;
}
void NDKCamera::MediaAssetManagerRelease(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetManagerRelease start!");
    result = OH_MediaAssetManager_Release(g_mediaAssetManager);
    if (result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetManagerRelease failed.");
        return;
    }
    g_mediaAssetManager = nullptr;
    DRAWING_LOGD(" [RM003 log] NDKCamera::MediaAssetManagerRelease end!");
    return;
}
void NDKCamera::MovingPhotoGetUri(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::MovingPhotoGetUri start!");
    result = OH_MovingPhoto_GetUri(g_movingPhoto, &g_uri);
    if (g_uri == nullptr || result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::MovingPhotoGetUri failed.");
        return;
    }
    DRAWING_LOGD(" [RM003 log] NDKCamera::MovingPhotoGetUri uri: %{public}s", g_uri);
    DRAWING_LOGD(" [RM003 log] NDKCamera::MovingPhotoGetUri end!");
    return;
}


bool NDKCamera::MovingPhotoRequestContentWithUris(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::MovingPhotoRequestContentWithUris start!");
    if (g_mediaQualityCb == "fast") {
        DRAWING_LOGD(" [RM003 log] MovingPhotoRequestContentWithUris is MEDIA_LIBRARY_QUALITY_FAST!");
        char *imageUriL = "file://com.samples.normalCamera/data/storage/el2/base/haps/UrisImageFileL.jpg";
        char *videoUriL = "file://com.samples.normalCamera/data/storage/el2/base/haps/UrisVideoFileL.mp4";
        result = OH_MovingPhoto_RequestContentWithUris(g_movingPhoto, imageUriL,videoUriL);
        if (result != MEDIA_LIBRARY_OK) {
            DRAWING_LOGD(" [RM003 log] NDKCamera::MovingPhotoRequestContentWithUrisL result:%{public}d.", result);
            return false;
        }
        DRAWING_LOGD(" [RM003 log] NDKCamera::MovingPhotoRequestContentWithUris imageUriL: %{public}s", imageUriL);
        DRAWING_LOGD(" [RM003 log] NDKCamera::MovingPhotoRequestContentWithUris videoUriL: %{public}s", videoUriL);
    }
    if (g_mediaQualityCb == "high") {
        DRAWING_LOGD(" [RM003 log] MovingPhotoRequestContentWithUris is MEDIA_LIBRARY_QUALITY_FULL!");
        char *imageUriH = "file://com.samples.normalCamera/data/storage/el2/base/haps/UrisImageFileH.jpg";
        char *videoUriH = "file://com.samples.normalCamera/data/storage/el2/base/haps/UrisVideoFileH.mp4";
        result = OH_MovingPhoto_RequestContentWithUris(g_movingPhoto, imageUriH, videoUriH);
        if (result != MEDIA_LIBRARY_OK) {
            DRAWING_LOGD(" [RM003 log] NDKCamera::MovingPhotoRequestContentWithUrisH result:%{public}d.", result);
            return false;
        }
        DRAWING_LOGD(" [RM003 log] NDKCamera::MovingPhotoRequestContentWithUris imageUriH: %{public}s", imageUriH);
        DRAWING_LOGD(" [RM003 log] NDKCamera::MovingPhotoRequestContentWithUris videoUriH: %{public}s", videoUriH);
    }
    
    DRAWING_LOGD(" [RM003 log] NDKCamera::MovingPhotoRequestContentWithUris end!");
    return true;
}
 
bool NDKCamera::MovingPhotoRequestContentWithUri(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::MovingPhotoRequestContentWithUri start!");
    
    if (g_mediaQualityCb == "fast") {
        DRAWING_LOGD(" [RM003 log] MovingPhotoRequestContentWithUris is MEDIA_LIBRARY_QUALITY_FAST!");
        char *imageUriL = "file://com.samples.normalCamera/data/storage/el2/base/haps/UriImageFileL.jpg";
        char *videoUriL = "file://com.samples.normalCamera/data/storage/el2/base/haps/UriVideoFileL.mp4";
        result = OH_MovingPhoto_RequestContentWithUri(g_movingPhoto, RESOURCE_TYPE_IMAGE, imageUriL);
        if (result != MEDIA_LIBRARY_OK) {
            DRAWING_LOGD(" [RM003 log] NDKCamera::MovingPhotoRequestContentWithUri RESOURCE_TYPE_IMAGE result:%{public}d.", result);
            return false;
        }
        DRAWING_LOGD(" [RM003 log] NDKCamera::MovingPhotoRequestContentWithUri imageUriL: %{public}s", imageUriL);
        result = OH_MovingPhoto_RequestContentWithUri(g_movingPhoto, RESOURCE_TYPE_VIDEO, videoUriL);
        if (result != MEDIA_LIBRARY_OK) {
            DRAWING_LOGD(" [RM003 log] NDKCamera::MovingPhotoRequestContentWithUri RESOURCE_TYPE_VIDEO result:%{public}d.", result);
            return false;
        }
        DRAWING_LOGD(" [RM003 log] NDKCamera::MovingPhotoRequestContentWithUri videoUriL: %{public}s", videoUriL);
    }
    if (g_mediaQualityCb == "high") {
        DRAWING_LOGD(" [RM003 log] MovingPhotoRequestContentWithUris is MEDIA_LIBRARY_QUALITY_FULL!");
        char *imageUriH = "file://com.samples.normalCamera/data/storage/el2/base/haps/UriImageFileH.jpg";
        char *videoUriH = "file://com.samples.normalCamera/data/storage/el2/base/haps/UriVideoFileH.mp4";
        result = OH_MovingPhoto_RequestContentWithUri(g_movingPhoto, RESOURCE_TYPE_IMAGE, imageUriH);
        if (result != MEDIA_LIBRARY_OK) {
            DRAWING_LOGD(" [RM003 log] NDKCamera::MovingPhotoRequestContentWithUri RESOURCE_TYPE_IMAGE result:%{public}d.", result);
            return false;
        }
        DRAWING_LOGD(" [RM003 log] NDKCamera::MovingPhotoRequestContentWithUri imageUriH: %{public}s", imageUriH);
        result = OH_MovingPhoto_RequestContentWithUri(g_movingPhoto, RESOURCE_TYPE_VIDEO, videoUriH);
        if (result != MEDIA_LIBRARY_OK) {
            DRAWING_LOGD(" [RM003 log] NDKCamera::MovingPhotoRequestContentWithUri RESOURCE_TYPE_VIDEO result:%{public}d.", result);
            return false;
        }
        DRAWING_LOGD(" [RM003 log] NDKCamera::MovingPhotoRequestContentWithUri videoUriH: %{public}s", videoUriH);
    }
    
    DRAWING_LOGD(" [RM003 log] NDKCamera::MovingPhotoRequestContentWithUri end!");
    return true;
}

bool NDKCamera::MovingPhotoRequestContentWithBuffer(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::MovingPhotoRequestContentWithBuffer start!");
    DRAWING_LOGD("[RM003 log] NDKCamera::MovingPhotoRequestContentWithBuffer, g_movingPhoto = %{public}p, RESOURCE_TYPE_IMAGE = %{public}d",g_movingPhoto, RESOURCE_TYPE_IMAGE);
    
    if (g_mediaQualityCb == "fast") {
        result = OH_MovingPhoto_RequestContentWithBuffer(g_movingPhoto, RESOURCE_TYPE_IMAGE, &g_imageBuffer, &g_imageSize);
        if (g_imageBuffer == nullptr || result != MEDIA_LIBRARY_OK) {
            DRAWING_LOGD(" [RM003 log] NDKCamera::MovingPhotoRequestContentWithBuffer RESOURCE_TYPE_IMAGE result = %{public}d", result);
            return false;
        }
        std::string fileImageName = "/data/storage/el2/base/haps/BufferImageFileL.jpg";
        std::ofstream outFile(fileImageName, std::ofstream::out);
        if (!outFile.is_open()) {
            return false;
        }
        outFile.write(reinterpret_cast<const char*>(g_imageBuffer), g_imageSize);
        outFile.close();
        DRAWING_LOGD(" [RM003 log] NDKCamera::MovingPhotoRequestContentWithBuffer imageSize: %{public}d", g_imageSize);
        result = OH_MovingPhoto_RequestContentWithBuffer(g_movingPhoto, RESOURCE_TYPE_VIDEO, &g_videoBuffer, &g_videoSize);
        if (g_videoBuffer == nullptr || result != MEDIA_LIBRARY_OK) {
            DRAWING_LOGD(" [RM003 log] NDKCamera::MovingPhotoRequestContentWithBuffer RESOURCE_TYPE_VIDEO result:%{public}d.", result);
            return false;
        }
        std::string fileVideoName = "/data/storage/el2/base/haps/BufferImageFileL.mp4";
        std::ofstream outVideoFile(fileVideoName, std::ofstream::out);
        if (!outVideoFile.is_open()) {
            return false;
        }
        outVideoFile.write(reinterpret_cast<const char*>(g_videoBuffer), g_videoSize);
        outVideoFile.close();
        DRAWING_LOGD(" [RM003 log] NDKCamera::MovingPhotoRequestContentWithBuffer videoSize: %{public}d", g_videoSize);
    }
    if (g_mediaQualityCb == "high") {
        result = OH_MovingPhoto_RequestContentWithBuffer(g_movingPhoto, RESOURCE_TYPE_IMAGE, &g_imageBuffer, &g_imageSize);
        if (g_imageBuffer == nullptr || result != MEDIA_LIBRARY_OK) {
            DRAWING_LOGD(" [RM003 log] NDKCamera::MovingPhotoRequestContentWithBuffer RESOURCE_TYPE_IMAGE result = %{public}d", result);
            return false;
        }
        std::string fileImageName = "/data/storage/el2/base/haps/BufferImageFileH.jpg";
        std::ofstream outFile(fileImageName, std::ofstream::out);
        if (!outFile.is_open()) {
            return false;
        }
        outFile.write(reinterpret_cast<const char*>(g_imageBuffer), g_imageSize);
        outFile.close();
        DRAWING_LOGD(" [RM003 log] NDKCamera::MovingPhotoRequestContentWithBuffer imageSize: %{public}d", g_imageSize);
        result = OH_MovingPhoto_RequestContentWithBuffer(g_movingPhoto, RESOURCE_TYPE_VIDEO, &g_videoBuffer, &g_videoSize);
        if (g_videoBuffer == nullptr || result != MEDIA_LIBRARY_OK) {
            DRAWING_LOGD(" [RM003 log] NDKCamera::MovingPhotoRequestContentWithBuffer RESOURCE_TYPE_VIDEO result = %{public}d", result);
            return false;
        }
        std::string fileVideoName = "/data/storage/el2/base/haps/BufferImageFileH.mp4";
        std::ofstream outVideoFile(fileVideoName, std::ofstream::out);
        if (!outVideoFile.is_open()) {
            return false;
        }
        outVideoFile.write(reinterpret_cast<const char*>(g_videoBuffer), g_videoSize);
        outVideoFile.close();
        DRAWING_LOGD(" [RM003 log] NDKCamera::MovingPhotoRequestContentWithBuffer videoSize: %{public}d", g_videoSize);
    }
    
    
    DRAWING_LOGD(" [RM003 log] NDKCamera::MovingPhotoRequestContentWithBuffer end!");
    return true;
}

void NDKCamera::MovingPhotoRelease(void)
{
    DRAWING_LOGD(" [RM003 log] NDKCamera::MovingPhotoRelease start!");
    result = OH_MovingPhoto_Release(g_movingPhoto);
    if (result != MEDIA_LIBRARY_OK) {
        DRAWING_LOGD(" [RM003 log] NDKCamera::MovingPhotoRelease failed.");
        return;
    }
    g_movingPhoto = nullptr;
    DRAWING_LOGD(" [RM003 log] NDKCamera::MovingPhotoRelease end!");
    return;
}
} // namespace OHOS_CAMERA_SAMPLE