#include <cstdint>
#include <cstring>
#include <string.h>
#include "napi/native_api.h"
#include "hilog/log.h"
#include "camera_manager.h"
// #include <multimedia/image_framework/image_source_mdk.h>
// #include <multimedia/image_framework/image_pixel_map_mdk.h>

#define LOG_TAG "TESTZJ"
#define LOG_DOMAIN 0x3210

using namespace OHOS_CAMERA_NDK_SAMPLE;

static NDKCamera *ndkCamera_ = nullptr;
size_t g_size = 0;

const int32_t ARGS_TWO = 2;
const int32_t ARGS_THREE = 3;
const int32_t ARGS_FOUR = 4;
const int32_t ARGS_FIVE = 5;
const int32_t ARGS_SIX = 6;
const int32_t ARGS_SEVEN = 7;
const int32_t ARGS_EIGHT = 8;

struct Capture_Setting {
    int32_t quality;
    int32_t rotation;
    int32_t location;
    bool mirror;
    int32_t latitude;
    int32_t longitude;
    int32_t altitude;
};

static napi_value InitCamera(napi_env env, napi_callback_info info) {
    OH_LOG_ERROR(LOG_APP, "InitCamera Start");
    size_t requireArgc = 9;
    size_t argc = 9;
    napi_value args[9] = {nullptr};
    napi_value result;
    size_t typeLen = 0;
    char *surfaceId = nullptr;

    napi_get_cb_info(env, info, &argc, args, nullptr, nullptr);

    napi_get_value_string_utf8(env, args[0], nullptr, 0, &typeLen);
    surfaceId = new char[typeLen + 1];
    napi_get_value_string_utf8(env, args[0], surfaceId, typeLen + 1, &typeLen);

    napi_valuetype valuetype1;
    napi_typeof(env, args[1], &valuetype1);

    int32_t focusMode;
    napi_get_value_int32(env, args[1], &focusMode);

    uint32_t cameraDeviceIndex;
    napi_get_value_uint32(env, args[ARGS_TWO], &cameraDeviceIndex);

    uint32_t sceneMode;
    napi_get_value_uint32(env, args[ARGS_THREE], &sceneMode);

    uint32_t preconfigMode;
    napi_get_value_uint32(env, args[ARGS_FOUR], &preconfigMode);

    uint32_t preconfigType;
    napi_get_value_uint32(env, args[ARGS_FIVE], &preconfigType);

    uint32_t preconfigRatio;
    napi_get_value_uint32(env, args[ARGS_SIX], &preconfigRatio);

    uint32_t photoOutputType;
    napi_get_value_uint32(env, args[ARGS_SEVEN], &photoOutputType);

    bool isMovingPhoto;
    napi_get_value_bool(env, args[ARGS_EIGHT], &isMovingPhoto);

    OH_LOG_ERROR(LOG_APP, "InitCamera focusMode : %{public}d", focusMode);
    OH_LOG_ERROR(LOG_APP, "InitCamera surfaceId : %{public}s", surfaceId);
    OH_LOG_ERROR(LOG_APP, "InitCamera cameraDeviceIndex : %{public}d", cameraDeviceIndex);
    OH_LOG_ERROR(LOG_APP, "InitCamera sceneMode : %{public}d", sceneMode);
    OH_LOG_ERROR(LOG_APP, "InitCamera preconfigMode : %{public}d", preconfigMode);
    OH_LOG_ERROR(LOG_APP, "InitCamera preconfigType : %{public}d", preconfigType);
    OH_LOG_ERROR(LOG_APP, "InitCamera preconfigRatio : %{public}d", preconfigRatio);
    OH_LOG_ERROR(LOG_APP, "InitCamera photoOutputType : %{public}d", photoOutputType);
    OH_LOG_ERROR(LOG_APP, "InitCamera isMovingPhoto : %{public}d", isMovingPhoto);

    if (ndkCamera_) {
        OH_LOG_ERROR(LOG_APP, "ndkCamera_ is not null");
        delete ndkCamera_;
        ndkCamera_ = nullptr;
    }
    ndkCamera_ = new NDKCamera(surfaceId, focusMode, cameraDeviceIndex, sceneMode, preconfigMode, preconfigType,
                               preconfigRatio, photoOutputType, isMovingPhoto);
    OH_LOG_ERROR(LOG_APP, "InitCamera End");
    napi_create_int32(env, argc, &result);
    return result;
}

static napi_value GetCaptureParam(napi_env env, napi_value captureConfigValue, Capture_Setting *config) {
    napi_value value = nullptr;
    napi_get_named_property(env, captureConfigValue, "quality", &value);
    napi_get_value_int32(env, value, &config->quality);

    napi_get_named_property(env, captureConfigValue, "rotation", &value);
    napi_get_value_int32(env, value, &config->rotation);

    napi_get_named_property(env, captureConfigValue, "mirror", &value);
    napi_get_value_bool(env, value, &config->mirror);

    napi_get_named_property(env, captureConfigValue, "latitude", &value);
    napi_get_value_int32(env, value, &config->latitude);

    napi_get_named_property(env, captureConfigValue, "longitude", &value);
    napi_get_value_int32(env, value, &config->longitude);

    napi_get_named_property(env, captureConfigValue, "altitude", &value);
    napi_get_value_int32(env, value, &config->altitude);

    OH_LOG_INFO(LOG_APP,
                "get quality %{public}d, rotation %{public}d, mirror %{public}d, latitude "
                "%{public}d, longitude %{public}d, altitude %{public}d",
                config->quality, config->rotation, config->mirror, config->latitude, config->longitude,
                config->altitude);
    return 0;
}
static void SetConfig(Capture_Setting settings, Camera_PhotoCaptureSetting *photoSetting, Camera_Location *location) {
    if (photoSetting == nullptr || location == nullptr) {
        OH_LOG_INFO(LOG_APP, "photoSetting is null");
    }
    photoSetting->quality = static_cast<Camera_QualityLevel>(settings.quality);
    photoSetting->rotation = static_cast<Camera_ImageRotation>(settings.rotation);
    photoSetting->mirror = settings.mirror;
    location->altitude = settings.altitude;
    location->latitude = settings.latitude;
    location->longitude = settings.longitude;
    photoSetting->location = location;
}

static napi_value ReleaseCamera(napi_env env, napi_callback_info info)
{
    OH_LOG_ERROR(LOG_APP, "ReleaseCamera Start");
    size_t requireArgc = 2;
    size_t argc = 2;
    napi_value args[2] = {nullptr};
    napi_value result;
    size_t typeLen = 0;
    char* surfaceId = nullptr;

    napi_get_cb_info(env, info, &argc, args, nullptr, nullptr);

    ndkCamera_->ReleaseCamera();
    if (ndkCamera_) {
        OH_LOG_ERROR(LOG_APP, "ndkCamera_ is not null");
        delete ndkCamera_;
        ndkCamera_ = nullptr;
    }
    OH_LOG_ERROR(LOG_APP, "ReleaseCamera End");
    napi_create_int32(env, argc, &result);
    return result;
}


static napi_ref callbackRef = nullptr;
static napi_env g_env;

static napi_value RequestImageUrl(napi_env env, napi_callback_info info) {
    OH_LOG_ERROR(LOG_APP, "RequestImageUrl start uri");
    napi_value result;
    napi_get_undefined(env, &result);

    napi_value argValue[1] = {nullptr};
    size_t argCount = 1;
    napi_get_cb_info(env, info, &argCount, argValue, nullptr, nullptr);

    g_env = env;
    napi_create_reference(env, argValue[0], 1, &callbackRef);
    if (callbackRef) {
        OH_LOG_ERROR(LOG_APP, "RequestImageUrl callbackRef is full");
    } else {
        OH_LOG_ERROR(LOG_APP, "RequestImageUrl callbackRef is null");
    }
    return result;
}

static void RequestImageUrlCb(char *url) {
    OH_LOG_ERROR(LOG_APP, "uri = %{public}s", url);
    // g_size = size;
    napi_value resource = nullptr;
    napi_async_work work;
    napi_create_string_utf8(g_env, "RequestImageUrlCb", NAPI_AUTO_LENGTH, &resource);
    napi_create_async_work(
        g_env, nullptr, resource, [](napi_env env, void *url) {},
        [](napi_env env, napi_status status, void *url) {
            napi_value result = nullptr;
            napi_value retVal;
            napi_value callback = nullptr;
            napi_create_string_utf8(env, (const char *)url, NAPI_AUTO_LENGTH, &result);
            // napi_create_external(g_env, pixelMap, nullptr, nullptr, &result);
            // napi_create_arraybuffer(env, g_size, &buffer, &result);

            napi_get_reference_value(g_env, callbackRef, &callback);
            if (callback) {
                OH_LOG_ERROR(LOG_APP, "RequestImageUrlCb callback is full");
            } else {
                OH_LOG_ERROR(LOG_APP, "RequestImageUrlCb callback is null");
            }
            napi_call_function(g_env, nullptr, callback, 1, &result, &retVal);
            //         napi_delete_reference(g_env, callbackRef); //todo
        },
        reinterpret_cast<void *>(url), &work);
    napi_queue_async_work(g_env, work);
}


static napi_ref bufferCallbackRef = nullptr;
static napi_env g_env_buffer;

static napi_value RequestImageBuffer(napi_env env, napi_callback_info info) {
    OH_LOG_ERROR(LOG_APP, "RequestImageBuffer start uri");
    napi_value result;
    napi_get_undefined(env, &result);

    napi_value argValue[1] = {nullptr};
    size_t argCount = 1;
    napi_get_cb_info(env, info, &argCount, argValue, nullptr, nullptr);

    g_env_buffer = env;
    napi_create_reference(env, argValue[0], 1, &bufferCallbackRef);
    if (bufferCallbackRef) {
        OH_LOG_ERROR(LOG_APP, "RequestImageBuffer callbackRef is full");
    } else {
        OH_LOG_ERROR(LOG_APP, "RequestImageBuffer callbackRef is null");
    }
    return result;
}

static void RequestImageBufferCb(uint8_t *buffer, size_t size) {
    OH_LOG_ERROR(LOG_APP, "RequestImageBufferCb size:%{public}zu", size);
    g_size = size;
    napi_value resource = nullptr;
    napi_async_work work;

    for (int i = 0; i < 8; i++) {
        OH_LOG_ERROR(LOG_APP, "RequestImageBufferCb buffer read res: %{public}d", buffer[i]);
    }
    // 创建一个新的 unique_ptr copyBuffer，大小与 buffer 相同
    //     auto copyBuffer = std::make_unique<uint8_t[]>(size);
    uint8_t *copyBuffer = static_cast<uint8_t *>(malloc(size));
    if (copyBuffer == nullptr) {
        return;
    }
    // 使用 std::memcpy 复制 buffer 的内容到 copyBuffer
    std::memcpy(copyBuffer, buffer, size);

    for (int i = 0; i < 8; i++) {
        OH_LOG_ERROR(LOG_APP, "RequestImageBufferCb copyBuffer read res: %{public}d", copyBuffer[i]);
    }
    napi_create_string_utf8(g_env_buffer, "RequestImageBufferCb", NAPI_AUTO_LENGTH, &resource);
    napi_status status = napi_create_async_work(
        g_env_buffer, nullptr, resource, [](napi_env env, void *copyBuffer) {},
        [](napi_env env, napi_status status, void *copyBuffer) {
            napi_value result = nullptr;
            napi_value retVal;
            napi_value callback = nullptr;
            for (int i = 0; i < 8; i++) {
                OH_LOG_ERROR(LOG_APP, "RequestImageBufferCb aync comp read res: %{public}d",
                             static_cast<uint8_t *>(copyBuffer)[i]);
            }
            void *data = nullptr;
            napi_value arrayBuffer = nullptr;
            size_t bufferSize = g_size;
            napi_create_arraybuffer(env, bufferSize, &data, &arrayBuffer);
            std::memcpy(data, copyBuffer, bufferSize);
            napi_create_typedarray(env, napi_uint8_array, bufferSize, arrayBuffer, 0, &result);
            OH_LOG_ERROR(LOG_APP, "RequestImageBufferCb g_size: %{public}zu", g_size);
            napi_get_reference_value(env, bufferCallbackRef, &callback);
            if (callback) {
                OH_LOG_ERROR(LOG_APP, "RequestImageBufferCb callback is full");
            } else {
                OH_LOG_ERROR(LOG_APP, "RequestImageBufferCb callback is null");
            }
            napi_call_function(env, nullptr, callback, 1, &result, &retVal);
        },
        static_cast<void *>(copyBuffer), &work);

    if (status != napi_ok) {
        delete copyBuffer;
    }
    napi_queue_async_work_with_qos(g_env_buffer, work, napi_qos_user_initiated);
}

static napi_ref callbackRef2 = nullptr;
static napi_env g_env2;

static napi_value RequestImageQuality(napi_env env, napi_callback_info info) {
    OH_LOG_ERROR(LOG_APP, "RequestImageQuality");
    napi_value result;
    napi_get_undefined(env, &result);

    napi_value argValue[1] = {nullptr};
    size_t argCount = 1;
    napi_get_cb_info(env, info, &argCount, argValue, nullptr, nullptr);

    g_env2 = env;
    napi_create_reference(env, argValue[0], 1, &callbackRef2);
    if (callbackRef2) {
        OH_LOG_ERROR(LOG_APP, "RequestImageCb callbackRef is full");
    } else {
        OH_LOG_ERROR(LOG_APP, "RequestImageCb callbackRef is null");
    }
    return result;
}


static void RequestImageQualityCb(char *quality) {
    OH_LOG_ERROR(LOG_APP, "quality = %{public}s", quality);
    napi_value resource = nullptr;
    napi_async_work work;
    napi_create_string_utf8(g_env2, "RequestImageQualityCb", NAPI_AUTO_LENGTH, &resource);
    napi_create_async_work(
        g_env2, nullptr, resource,
        [](napi_env env, void *quality) {

        },
        [](napi_env env, napi_status status, void *quality) {
            napi_value result = nullptr;
            napi_value retVal;
            napi_value callback = nullptr;
            napi_create_string_utf8(env, (const char *)quality, NAPI_AUTO_LENGTH, &result);


            napi_get_reference_value(g_env, callbackRef2, &callback);
            if (callback) {
                OH_LOG_ERROR(LOG_APP, "RequestImageCb callback is full");
            } else {
                OH_LOG_ERROR(LOG_APP, "RequestImageCb callback is null");
            }
            napi_call_function(g_env2, nullptr, callback, 1, &result, &retVal);
            //         napi_delete_reference(g_env, callbackRef); //todo
        },
        reinterpret_cast<void *>(quality), &work);
    napi_queue_async_work(g_env2, work);
}

static napi_value TakePictureWithSettings(napi_env env, napi_callback_info info) {
    OH_LOG_INFO(LOG_APP, "TakePictureWithSettings Start");
    size_t requireArgc = 1;
    size_t argc = 1;
    napi_value args[1] = {nullptr};
    Camera_PhotoCaptureSetting photoSetting;
    Capture_Setting setting_inner;
    Camera_Location *location = new Camera_Location;

    napi_get_cb_info(env, info, &argc, args, nullptr, nullptr);
    GetCaptureParam(env, args[0], &setting_inner);
    SetConfig(setting_inner, &photoSetting, location);

    //     ndkCamera_->RequestImageUrlCallback((void*)RequestImageUrlCb);
    ndkCamera_->RequestImageBufferCallback((void *)RequestImageBufferCb);
    ndkCamera_->RequestImageQualityCallback((void *)RequestImageQualityCb);
    napi_value result;
    Camera_ErrorCode ret = ndkCamera_->TakePictureWithPhotoSettings(photoSetting);
    OH_LOG_ERROR(LOG_APP, "TakePictureWithSettings result is %{public}d", ret);
    napi_create_int32(env, ret, &result);
    return result;
}

static napi_value SaveCurImage(napi_env env, napi_callback_info info)
{
    OH_LOG_ERROR(LOG_APP, "SaveCurImage Start");
    size_t argc = 2;
    napi_value args[2] = {nullptr};
    napi_value result;

    napi_get_cb_info(env, info, &argc, args, nullptr, nullptr);

    ndkCamera_->SaveCurImage();
    OH_LOG_ERROR(LOG_APP, "SaveCurImage End");
    napi_create_int32(env, argc, &result);
    return result;
}

EXTERN_C_START
static napi_value Init(napi_env env, napi_value exports) {
    napi_property_descriptor desc[] = {
        {"initCamera", nullptr, InitCamera, nullptr, nullptr, nullptr, napi_default, nullptr},
        {"releaseCamera", nullptr, ReleaseCamera, nullptr, nullptr, nullptr, napi_default, nullptr },
        {"takePictureWithSettings", nullptr, TakePictureWithSettings, nullptr, nullptr, nullptr, napi_default, nullptr},
        {"requestImageUrl", nullptr, RequestImageUrl, nullptr, nullptr, nullptr, napi_default, nullptr},
        {"requestImageBuffer", nullptr, RequestImageBuffer, nullptr, nullptr, nullptr, napi_default, nullptr},
        {"requestImageQuality", nullptr, RequestImageQuality, nullptr, nullptr, nullptr, napi_default, nullptr},
        {"saveCurImage", nullptr, SaveCurImage, nullptr, nullptr, nullptr, napi_default, nullptr},
    };
    napi_define_properties(env, exports, sizeof(desc) / sizeof(desc[0]), desc);
    return exports;
}
EXTERN_C_END

static napi_module demoModule = {
    .nm_version = 1,
    .nm_flags = 0,
    .nm_filename = nullptr,
    .nm_register_func = Init,
    .nm_modname = "entry",
    .nm_priv = ((void *)0),
    .reserved = {0},
};

extern "C" __attribute__((constructor)) void RegisterEntryModule(void) { napi_module_register(&demoModule); }
