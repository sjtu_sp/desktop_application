
interface Capture_Setting {
  quality: number;
  rotation: number;
  mirror: boolean;
  latitude: number;
  longitude: number;
  altitude: number;
}

export const initCamera:(surfaceId: string, focusMode: number, cameraDeviceIndex: number, sceneMode: number,
  preconfigMode: number, preconfigType: number, preconfigRatio: number, photoOutputType: number,
  isMovingPhoto: boolean) => number;
export const releaseCamera: () => number;
export const takePictureWithSettings: (setting: Capture_Setting) => number;
export const requestImageUrl: (callback: AsyncCallback<string>) => void;
export const requestImageBuffer: (callback: AsyncCallback<Uint8Array>) => void;
export const requestImageQuality: (callback: AsyncCallback<string>) => void;
export const saveCurImage: () => number;