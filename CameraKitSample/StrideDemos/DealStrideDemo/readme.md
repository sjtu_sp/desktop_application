# 相机预览花屏解决方案

## 概述
开发者在使用相机服务时，如果仅用于预览流展示，通常使用XComponent组件实现，如果需要获取每帧图像做二次处理（例如获取每帧图像完成二维码识别或人脸识别场景），可以通过ImageReceiver中imageArrival事件监听预览流每帧数据，解析图像内容。在解析图像内容时，发现很多开发者未考虑stride与width不一致场景，未正确读取图像buffer导致相机预览花屏。  
当开发者获取预览流每帧图像buffer后，若发现图片内容出现花屏堆叠状，类似反例截图，出现“相机预览花屏”现象，此时需要排查解析每帧图像时，是否处理了stride与width不一致场景，导致解析了无效像素。为避免这样糟糕的用户体验，因此建议开发者参考本指导正确处理stride。

## 效果

| 正例一                                                      | 正例二                                                      |
|----------------------------------------------------------|----------------------------------------------------------|
| <img src="screenshots/devices/zhengli1.gif" width="300"> | <img src="screenshots/devices/zhengli2.gif" width="300"> |

## 具体实现
以一种高频的用户使用场景为例，应用需要定义一个1080*1080分辨率的预览流图像，此时的stride在相关平台的返回值为1088，此时需要对stride进行处理，处理无效像素后解析出正确的像素数据，避免出现预览流花屏。
1. 应用通过image.ImageReceiver注册imageArrival图像回调方法，获取每帧图像数据实例image.Image，应用通过定义一个width为1080*height为1080分辨率的预览流直接创建pixelMap，此时获取到的stride的值为1088，需将处理stride后的buffer传给Image送显，预览流正常展示。  
2. 处理stride有两种方式，详见方案一二。
### 方案一
开发者使用width，height，stride三个值，处理相机预览流数据。  
分两种情况：
- 当stride和width相等时，按宽读取buffer不影响结果；
- 当stride和width不等时，将相机返回的预览流数据即component.byteBuffer中的数据去除掉stride，
此时将component.byteBuffer中的数据去除掉stride，拷贝得到新的dstArr数据进行数据处理，将处理后的dstArr数组buffer，通过width和height直接创建pixelMap, 并存储到全局变量stridePixel中，传给Image送显。  
以下为关键示例代码：  
```typescript
function onImageArrival(receiver: image.ImageReceiver): void {
  receiver.on('imageArrival', () => {
    receiver.readNextImage((err: BusinessError, nextImage: image.Image) => {
      nextImage.getComponent(image.ComponentType.JPEG, async (err, component: image.Component) => { // 根据图像的组件类型从图像中获取组件缓存并使用callback返回结果
        let width = 1080; // width为应用创建预览流分辨率对应的宽
        let height = 1080; // height为应用创建预览流分辨率对应的高
        let stride = component.rowStride // 通过component.rowStride获取stride
        Logger.info(TAG, `receiver getComponent width:${width} height:${height} stride:${stride}`);
        // 正例：情况1. 当图片的width等于相机预览流返回的行跨距stride，此时无需处理stride，通过width和height直接创建pixelMap,并存储到全局变量stridePixel中，传给Image送显。
        if (stride === width) {
          let pixelMap = await image.createPixelMap(component.byteBuffer, {
            size: { height: height, width: width },
            srcPixelFormat: image.PixelMapFormat.NV21,
          })
          AppStorage.setOrCreate('stridePixel', pixelMap)
        } else {
          // 正例：情况2. 当图片的width不等于相机预览流返回的行跨距stride，
          // 此时将相机返回的预览流数据component.byteBuffer去除掉stride，拷贝得到新的dstArr数据，数据处理后传给其他不支持stride的接口处理。
          const dstBufferSize = width * height * 1.5 // 创建一个width * height * 1.5的dstBufferSize空间，此处为NV21数据格式。
          const dstArr = new Uint8Array(dstBufferSize) // 存放去掉stride后的buffer。
          // 读取每行数据，相机支持的profile宽高均为偶数，不涉及取整问题。
          for (let j = 0; j < height * 1.5; j++) { // 循环dstArr的每一行数据。
            // 拷贝component.byteBuffer的每行数据前width个字节到dstArr中(去除无效像素，刚好每行得到一个width*height的八字节数组空间)。
            const srcBuf = new Uint8Array(component.byteBuffer, j * stride, width) // 将component.byteBuffer返回的buffer，每行遍历，从首位开始，每行截取出width字节。
            dstArr.set(srcBuf, j * width) // 将width*height大小的数据存储到dstArr中。
          }
          let pixelMap = await image.createPixelMap(dstArr.buffer, { // 将处理后的dstArr数组buffer，通过width和height直接创建pixelMap,并存储到全局变量stridePixel中，传给Image送显。
            size: { height: height, width: width },
            srcPixelFormat: image.PixelMapFormat.NV21,
          })
          AppStorage.setOrCreate('stridePixel', pixelMap)
        }
        nextImage.release();
      })
    });
  })
}
```
  
### 方案二
开发者使用width，height，stride三个值，处理相机预览流数据。  
分两种情况：  
- 当stride和width相等时，与正例一情况一致，此处不再赘述。
- 当stride和width不等时，如果应用想使用byteBuffer预览流数据创建pixelMap直接显示，可以根据stride*height字节的大小先创建pixelMap，然后调用PixelMap的cropSync方法裁剪掉多余的像素，从而正确处理stride，解决预览流花屏问题。  
以下为关键示例代码：
```typescript
function onImageArrival(receiver: image.ImageReceiver): void {
  receiver.on('imageArrival', () => {
    receiver.readNextImage((err: BusinessError, nextImage: image.Image) => {
      if (nextImage) {
        nextImage.getComponent(image.ComponentType.JPEG, async (err, component: image.Component) => {
          let width = 1080; // width为应用创建预览流分辨率对应的宽
          let height = 1080; // height为应用创建预览流分辨率对应的高
          let stride = component.rowStride // 通过component.rowStride获取stride
          Logger.info(TAG, `receiver getComponent width:${width} height:${height} stride:${stride}`);
          // stride和width相等，按宽读取buffer不影响结果
          if (stride === width) {
            let pixelMap = await image.createPixelMap(component.byteBuffer, {
              size: { height: height, width: width },
              srcPixelFormat: image.PixelMapFormat.NV21,
            })
            AppStorage.setOrCreate('stridePixel', pixelMap)
          } else {
            let pixelMap = await image.createPixelMap(component.byteBuffer, {
              // 正例：1、创建PixelMap时width传stride，
              size: { height: height, width: stride },
              srcPixelFormat: 8,
            })
            // 2、然后调用PixelMap的cropSync方法裁剪掉多余的像素。
            pixelMap.cropSync({
              size: { width: width, height: height },
              x: 0,
              y: 0
            }) // 根据输入的尺寸裁剪图片,从(0,0)开始，裁剪width*height字节的区域。
            let pixelBefore: PixelMap | undefined = AppStorage.get('stridePixel');
            await pixelBefore?.release();
            AppStorage.setOrCreate('stridePixel', pixelMap);
          }
          nextImage.release();
        })
      }
    });
  })
}
```
